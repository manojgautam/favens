<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 * API Use: This API is used to handle all the Bevaka functions to be performed in the mobile app.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bevaka extends MY_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-type:application/json");
        $this->load->model('app/bevakamodel', 'bevakaapp');
        $this->load->model('app/commonmodel', 'commonapp');
    }

    /* Description: Function to send notifications on checklist list.
     * Input params: access token
     * Output params: User's data when registered/logged in successfully.
     */

    public function listChecklistItems() {

        $auth = $this->input->post('auth');
        $access_token = $this->input->post('access_token');

        # An array of mandatory fields.
        $mandatory = array($auth, $access_token);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                $data = array(
                    array("title" => "Nike", "count" =>  "10st", "brand_id" => "7"),
                    array("title" => "Adidas", "count" =>  "10st", "brand_id" => "8"),
                    array("title" => "Woodland", "count" =>  "10st", "brand_id" => "9"),
                    array("title" => "Puma", "count" =>  "10st", "brand_id" => "10"),
                );

                echo json_encode(array('status' => 200, 'response' => $data));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

}
