<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 * API Use: This API is used to handle all the listing products functions to be performed in the mobile app.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends MY_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-type:application/json");
        $this->load->model('app/productsmodel', 'productsapp');
        $this->load->model('app/commonmodel', 'commonapp');
    }

    /* Description: Function to list all the products added by admin.
     * Input params: Auth Token
     * Output params: List of all products.
     */

    public function listAllProductsAddedByAdmin() {
        $auth = $this->input->post('auth');

        # An array of mandatory fields.
        $mandatory = array($auth);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                # get all products.
                $products = $this->productsapp->listAllProductsAddedByAdmin();

                # generate different sizes for iOS.
                $data = $this->differentLogoSizes($products, 'product_image', 'product');

                echo json_encode(array('status' => 200, 'response' => $data));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }
    
    
    /* Description: Function to list all the brands added by admin.
     * Input params: Auth Token
     * Output params: List of all brands.
     */

    public function listAllBrandsAddedByAdmin() {
        $auth = $this->input->post('auth');

        # An array of mandatory fields.
        $mandatory = array($auth);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                # get all brands.
                $data = $this->productsapp->listAllBrandsAddedByAdmin();

                echo json_encode(array('status' => 200, 'response' => $data));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

    /* Description: Function to list product details.
     * Input params: Product ID
     * Output params: Details of a product.
     */

    public function getProductDetailsFromID() {
        $auth = $this->input->post('auth');
        $product_id = $this->input->post('product_id');

        # An array of mandatory fields.
        $mandatory = array($auth, $product_id);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {

                # get product details.
                $product = $this->productsapp->getProductDetailsFromID($product_id); 
                $product[0]['product_images'][] = ($product[0]['product_image'] == 'product_default.png')? DEFAULT_IMAGE_URL . $product[0]['product_image'] : BASE_IMAGE_URL . $product[0]['product_image'];
                $otherimages = $this->productsapp->getProductOtherImagesFromID($product_id);
                unset($product[0]['product_image']);
                
                # insert all other images in an array
                foreach ($otherimages as $image) {
                    $product[0]['product_images'][] = BASE_IMAGE_URL . $image['p_image_name'];
                }
                echo json_encode(array('status' => 200, 'response' => $product));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }
    
    /**** Bevaka Screen Functions ****/
    
    /* Description: Function to save Bevaka Screen details
     * Input params: Auth Token, access token, brand_id, keyword, options.
     * Output params: Successful message.
     */

    public function saveBevakaValues() {
        $auth = $this->input->post('auth');
        $brand_id = $this->input->post('brand_id');
        $keyword = $this->input->post('keyword');
        $access_token = $this->input->post('access_token');
        $checkbox1 = $this->input->post('checkbox1');
        $checkbox2 = $this->input->post('checkbox2');
        $checkbox3 = $this->input->post('checkbox3');
        $checkbox1_value = $this->input->post('checkbox1_value');

        $user_data = $this->commonapp->authenticateAccessTokenAndGetColumns($access_token);

        # An array of mandatory fields.
        $mandatory = array($auth, $access_token, $keyword, $brand_id);
        
        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                $data = array(
                    "u_id" => $user_data[0]['user_id'],
                    "brand_id" => $brand_id,
                    "keyword" => $keyword,
                    "checkbox1" => (isset($checkbox1) && $checkbox1 == 1)?1:0,
                    "checkbox2" => (isset($checkbox2) && $checkbox2 == 1)?1:0,
                    "checkbox3" => (isset($checkbox3) && $checkbox3 == 1)?1:0,
                    "checkbox1_value" => (isset($checkbox1_value) && $checkbox1_value > 0)?$checkbox1_value:0,
                );

                $this->productsapp->saveBevakaValues($data);
                echo json_encode(array('status' => 200, 'response' => BEVAKA_SAVED));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

}
