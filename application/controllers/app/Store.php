<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 * API Use: This API is used to handle all the store operations in the mobile app.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Store extends MY_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-type:application/json");
        $this->load->model('app/storemodel', 'storeapp');
        $this->load->model('app/commonmodel', 'commonapp');
    }

    /* Description: Function to get store details
     * Input params: Auth Token and store_id.
     * Output params: Store details and Store items.
     */

    public function listStoreDetails() {
        $auth = $this->input->post('auth');
        $store_id = $this->input->post('store_id');
        $access_token = $this->input->post('access_token');

        if ($access_token != '' && $access_token != NULL) {
            $user_id = $this->commonapp->authenticateAccessToken($access_token);
        }

        # An array of mandatory fields.
        $mandatory = array($auth, $store_id);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                # get all keywords.
                $storedata = $this->storeapp->listStoreDetails($store_id);

                # generate different sizes for iOS.
                $data = $this->differentLogoSizes($storedata, 'product_image', 'product');

                # get category wise listing of products
                $final = $this->getCategoryWiseDetailsOfProducts($data);

                $is_favourite = 0;
                if (isset($user_id) && !empty($user_id)) {
                    $data = array(
                        "s_id" => $store_id,
                        "u_id" => $user_id
                    );

                    $is_favourite = ($this->storeapp->checkStoreAsFavourite($data) == true)?1:0;
                }

                echo json_encode(array('status' => 200, 'response' => $final, 'is_favourite' => $is_favourite));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

    /* Description: Function to get store details
     * Input params: Auth Token and store_id.
     * Output params: Store details and Store items.
     */

    public function setStoreAsFavourite() {
        $auth = $this->input->post('auth');
        $store_id = $this->input->post('store_id');
        $status = $this->input->post('status');
        $access_token = $this->input->post('access_token');

        $user_data = $this->commonapp->authenticateAccessTokenAndGetColumns($access_token);

        # An array of mandatory fields.
        $mandatory = array($auth, $store_id, $access_token);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                $data = array(
                    "s_id" => $store_id,
                    "u_id" => $user_data[0]['user_id']
                );

                $check = $this->storeapp->checkStoreAsFavourite($data);

                $message = $this->checkStoreStatus($data, $status, $check);

                echo json_encode(array('status' => 200, 'response' => $message));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

    /* Description: Function to get Fav stores
     * Input params: Auth Token and access_token.
     * Output params: Stores.
     */

    public function getFavouriteStoresFromAccessToken() {
        $auth = $this->input->post('auth');
        $access_token = $this->input->post('access_token');

        $user_data = $this->commonapp->authenticateAccessTokenAndGetColumns($access_token);

        # An array of mandatory fields.
        $mandatory = array($auth, $access_token);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {

                #get list of all fav stores.
                $stores = $this->storeapp->getFavouriteStoresOfAUser($user_data[0]['user_id']);

                echo json_encode(array('status' => 200, 'response' => $stores));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

    # function to check different store status.

    public function checkStoreStatus($data, $status, $check) {
        if (!$check && $status == 1) {
            $message = STORE_FAVOURITE;
            # set store as favourite.
            $this->storeapp->setStoreAsFavourite($data);
            return $message;
        }
        if ($status == 0) {
            $message = STORE_FAVOURITE_REMOVED;
            # set store as favourite.
            $this->storeapp->removeStoreAsFavourite($data);
            return $message;
        }
        if ($check && $status == 1) {
            return STORE_ALREADY_FAVOURITE;
        }
    }

    # function to create category wise listing of products

    public function getCategoryWiseDetailsOfProducts($data) {
        $count = count($data);
        $categories = array();
        $final = array();
        for ($i = 0; $i < $count; $i++) {
            if (in_array($data[$i]['product_category'], $categories)) {
                $category = $data[$i]['product_category'];
                $data[$i]['product_detail'] = strip_tags($data[$i]['product_detail']);
                unset($data[$i]['product_category']);
                $final[$category][] = $data[$i];
            } else {
                $categories[] = $data[$i]['product_category'];
                $category = $data[$i]['product_category'];
                $data[$i]['product_detail'] = strip_tags($data[$i]['product_detail']);
                unset($data[$i]['product_category']);
                $final[$category][] = $data[$i];
            }
        }
        return $final;
    }

}
