<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 * API Use: This API is used to handle all the login functions to be performed in the mobile app.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-type:application/json");
        $this->load->model('app/loginmodel', 'loginapp');
        $this->load->model('app/commonmodel', 'commonapp');
    }

    /* Description: Function to Register a user.
     * Input params: Email, Device token and device_type
     * Output params: User's data when registered/logged in successfully.
     */

    public function userRegister() {

        $email = $this->input->post('email');
        $device_token = $this->input->post('device_token');
        $device_type = $this->input->post('device_type');

        # An array of mandatory fields.
        $mandatory = array($device_token, $device_type);

        if ($this->checkPostData($mandatory)) {
            $logindata = array(
                "u_email" => (isset($email) && $email != '') ? $email : "" ,
                "u_device_token" => $device_token,
                "u_device_type" => $device_type,
                "u_access_token" => bin2hex(openssl_random_pseudo_bytes(32))
            );
            $check = $this->loginapp->registerWithDeviceTokenAndEmail($logindata);
            if ($check) {
                echo json_encode(array('status' => 200, 'response' => array("auth" => AUTH_TOKEN, "access_token" => $logindata['u_access_token'])));
            } else {
                echo json_encode(array('status' => 500, 'error' => SERVER_ERROR));
            }
            exit();
        }
    }

    /* Description: Function to get Logos from the database to display on main screen.
     * Input params: Nothing.
     * Output params: List of logos.
     */

    public function getLogosAddedByAdmin() {

        $auth = $this->input->post('auth');

        # An array of mandatory fields.
        $mandatory = array($auth);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                # get all logos.
                $images = $this->loginapp->getLogosAddedByAdmin();

                # generate different sizes for iOS.
                $data = $this->differentLogoSizes($images, 'logo_path_original', 'login');
                echo json_encode(array('status' => 200, 'response' => $data));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

}
