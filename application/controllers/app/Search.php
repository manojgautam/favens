<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 * API Use: This API is used to handle all the listing products functions to be performed in the mobile app.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search extends MY_Controller {

    public function __construct() {
        parent::__construct();
        header("Content-type:application/json");
        $this->load->model('app/searchmodel', 'searchapp');
        $this->load->model('app/commonmodel', 'commonapp');
    }

    /* Description: Function to get autocomplete list of keywords for search..
     * Input params: Auth Token and keyword.
     * Output params: List of all keywords.
     */

    public function listAllKeywordsFromSearchBox() {
        $auth = $this->input->post('auth');
        $keyword = $this->input->post('keyword');
        # An array of mandatory fields.
        $mandatory = array($auth, $keyword);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                # get all keywords.
                $keywords = $this->searchapp->listAllKeywordsFromSearchBox($keyword);

                echo json_encode(array('status' => 200, 'response' => $keywords));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

    /* Description: Function to get products list filtered on the basis of keyword
     * Input params: Auth Token and keyword.
     * Output params: List of all products.
     */

    public function listAllProductsFromKeyword() {
        $auth = $this->input->post('auth');
        $keyword = $this->input->post('keyword');
        $keyword_id = $this->input->post('keyword_id');
        $keyword_type = $this->input->post('keyword_type');

        # An array of mandatory fields.
        $mandatory = array($auth, $keyword, $keyword_id, $keyword_type);

        if ($this->checkPostData($mandatory)) {
            if ($auth == AUTH_TOKEN) {
                # get all keywords.
                $products = $this->searchapp->listAllProductsFromKeyword($keyword_id, $keyword_type, $keyword);

                # generate different sizes for iOS.
                $data = $this->differentLogoSizes($products, 'product_image', 'product');

                echo json_encode(array('status' => 200, 'response' => $data));
            } else {
                echo json_encode(array('status' => 500, 'error' => WRONG_AUTH));
            }
            exit();
        }
    }

}
