<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logos extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
		     	$this->load->model('admin/Dashboard_model', 'dashboardmodel');
      	
				if( !$this->checksession() ){
				 redirect(base_url().'admin/login');
			  }
			$this->load->model('admin/logos_model', 'logosmodel');
			$this->load->helper('string');
	}
	
	function index() {
		    
		}
		
	function logoslist(){
		    
		 $submit = $this->input->post('submit');
		if ($submit)
		{
		   /* image upload start */
		     $config['upload_path']='./uploads/';
			 $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
			 $config['file_name']=random_string('alnum', 40).".jpg";
			 $this->load->library('upload');
			 $this->upload->initialize($config);
			 
			if(!$this->upload->do_upload())
			{ 
			  $image='';
			 
			}
			else
			{ 
			  $imgdata=$this->upload->data();
			  $image=$imgdata['file_name'];  }
			  /* image upload end */
			  
		    $current_date_time = date("Y-m-d H:i:s");
			$register_array = array(
								'l_name' => $this->input->post('lname'),
								'l_logo' => $image,
							    'created_at' => $current_date_time
		);
		
		//print_r($register_array); die;
		$this->logosmodel->add_logos($register_array);
		//	$this->session->set_flashdata('success',"Record Added Successfully");
		//	redirect(base_url().'admin/logos/logoslist');
		}
		    
		    
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Logos";
            $data['llogos'] = "listlogos";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['logoslist'] = $this->logosmodel->get_all_logos();
	       	//print_r($data);die;
	    
		    $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/logoslist');	
		   	$this->load->view('common/footer');	
		}
		
	
		function deletelogos($l_id){
		    
	       	$data = $this->logosmodel->delete_logos($l_id);
	       	redirect(base_url().'admin/logos/logoslist');
		}
		
	
		
}
