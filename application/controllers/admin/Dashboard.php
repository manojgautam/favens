<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
      	$this->load->model('admin/Dashboard_model', 'dashboardmodel');
      	
		$this->load->model('admin/products_model', 'productsmodel');
		$this->load->model('admin/brands_model', 'brandsmodel');
		$this->load->model('admin/stores_model', 'storesmodel');
		$this->load->model('admin/User_model', 'usermodel');

      if( !$this->checksession() ){
         redirect(base_url().'admin/login');
      }
	}
		
	function index() {
	    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin dashboard";
	        $data['dashboard'] = "dashboard";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	     $data['productslist'] = $this->productsmodel->get_all_products();
	     $data['productcount'] = count($data['productslist']);
	     
	     $data['todayproductslist'] = $this->dashboardmodel->get_today_product();
	     $data['todayproductcount'] = count($data['todayproductslist']);
	     
	     
	      $data['brandlist'] = $this->brandsmodel->get_all_brands();
	      $data['brandcount'] = count($data['brandlist']);
	     
	     $data['todaybrandlist'] = $this->dashboardmodel->get_today_brand();
	      $data['todaybrandcount'] = count($data['todaybrandlist']);
	     
	     
	        $data['storeslist'] = $this->storesmodel->get_all_stores();
	        $data['storecount'] = count($data['storeslist']);
	     
	      $data['todaystoreslist'] = $this->dashboardmodel->get_today_store();
	        $data['todaystorecount'] = count($data['todaystoreslist']);
	     
	      $data['userlist'] = $this->usermodel->get_user_detail();
	        $data['usercount'] = count($data['userlist']);
	     
	      $data['todayuserlist'] = $this->dashboardmodel->get_today_user();
	        $data['todayusercount'] = count($data['todayuserlist']);
	     
	       $data['notificationdata'] = $this->notificationmodel->get_notification_data();
	     
			$this->load->view('common/header',$data);	
			$this->load->view('common/sidebar');
			$this->load->view('admin/dashboard');
			$this->load->view('common/footer');	

	}	

	

	
}
