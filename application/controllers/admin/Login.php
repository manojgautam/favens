<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Login_model', 'loginmodel');
      
	}
		
	function index() {
		
		
	   $submitted 			= $this->input->post('submitted');
		if ($submitted)
		{
			$email	= $this->input->post('email');
			$password	= $this->input->post('password');
			$login		= $this->loginmodel->check_login_details($email, $password);
           //print_r($login); die;
			//$redirect = site_url('admin/dashboard');
			
		
			if (count($login))
			{
			    //admin session start
			    $admindata= array( 'email'=> $email, 
			    'id' => $login[0]->id,
			    'name' => $login[0]->name,
			   'status' => $login[0]->status
			   );
			        
			        
			$this->session->set_userdata('adminsession', $admindata);
				redirect(base_url().'admin/dashboard');
			}
			else
			{
				//this adds the redirect back to flash data if they provide an incorrect credentials
		                $checkdeactivate= $this->loginmodel->check_deactivate_details($email, $password);
                               if(count($checkdeactivate)){$this->session->set_flashdata('error', 'User has been deactivated');}
                               else{
				$this->session->set_flashdata('error', 'Email/Password is not correct');}
				redirect(base_url().'admin/login');
			}
		}
		
		$data['title'] = "Favens | Admin Login";
		
			$this->load->view('common/header',$data);	
			$this->load->view('admin/login');	
			$this->load->view('common/footer');	

	}	

 function forgot(){
        $email=$this->input->post('email');
        $data=$this->loginmodel->Checkemailforgot($email);

        if(count($data))
        {$id=base64_encode($data[0]->id);
         $data_email['name']=$data[0]->name;
         $data_email['message']="<p>You  recently requested to reset your password for your Favens Account. Click the button below to reset it</p> ";
         $data_email['link']=base_url().'admin/login/forgetpassword/'.$id;
          $data_email['message1']="<p> if you dont request a password reset,Please ignore this email or reply to let us know.</p>";
           $data_email['link_text']="Click here to reset your password";
          $message=$this->load->view('admin/email',$data_email,true);
          $this->globalmailfunction($email,"Favens - Reset Password",$message);
         }
        else{
        echo "not match";
            }
 }

public function forgetpassword(){
$id=base64_decode($this->uri->segment(4));

$data['title'] = "Favens | Admin Login";
$data['id'] = $this->uri->segment(4);

  if($this->input->post('submitforgot')==true)
         {
            $this->form_validation->set_rules('password','Password','required');
            $this->form_validation->set_rules('confirm_password','Confirm Password','required|matches[password]');
            $this->form_validation->set_message('required','The passwords are not the same');
            $this->form_validation->set_error_delimiters('<span style="color: #f55753;font-size: 12px;">','</span>');
            if($this->form_validation->run()==true)
           {
          $data=array(
          'password'=>md5($this->input->post('password'))
          );
            $this->loginmodel->updateForgetpassword($id,$data);
           redirect(base_url());
         }
              else{
                        $this->load->view('common/header',$data);	
			$this->load->view('admin/forgot');	
			$this->load->view('common/footer');
                  }
}
else{
                        $this->load->view('common/header',$data);	
			$this->load->view('admin/forgot');	
			$this->load->view('common/footer');
}
		
			

}
	
	function logout(){ 
	    $this->session->unset_userdata('adminsession');
	    redirect(base_url().'admin/login');
	    
	}
}
