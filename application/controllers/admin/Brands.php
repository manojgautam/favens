<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brands extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
                    $this->load->model('admin/stores_model', 'storesmodel');
                    $this->load->helper('string');
      	
			if( !$this->checksession() ){
			redirect(base_url().'admin/login');
			}
		$this->load->model('admin/brands_model', 'brandsmodel');
		$this->load->helper('string');
	}
	
	function index() {	   
		$submit = $this->input->post('submit');
		if ($submit)
		{
		   /* image upload start */
			 $config['upload_path']='./uploads/';
			 $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
			 $config['file_name']=random_string('alnum', 40).".jpg";
			 $this->load->library('upload');
			 $this->upload->initialize($config);
			 
			if(!$this->upload->do_upload())
			{ 
			  $image='';
			 
			}
			else
			{ 
			  $imgdata=$this->upload->data();
			  $image=$imgdata['file_name'];  
			}
			 /* image upload end */
      	    $current_date_time = date("Y-m-d H:i:s");
			$register_array = array(
								'b_name' => $this->input->post('bname'),
								'b_details' => $this->input->post('bdetails'),
								'b_logo' => $image,
								'b_show_screen' => $this->input->post('show_screen'),
								'b_added_by' =>  $this->session->userdata('adminsession')['name'],
								'created_at' => $current_date_time
		     );
		
			//print_r($register_array); die;
			$brand_id =	$this->brandsmodel->add_brands($register_array);
	
			// start notification insert data
			$ndata= array(
					 'n_id' => $brand_id,
					'n_name' => $this->input->post('bname'),
					'n_added_by' => $this->session->userdata('adminsession')['name'],
					'n_type' => 2,
					'n_seen' => 1,
					'created_at' => $current_date_time
				);
		     $this->notificationmodel->notification_data($ndata);
		// end notification insert data
		
			$this->session->set_flashdata('success',"Record Added Successfully");
		}	
            $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Brands";
			$data['abrand'] = "addbrand";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	    		
			$data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
			$this->load->view('admin/brands');	
		   	$this->load->view('common/footer');	
		}
		
	function brandslist(){
		   
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Brands";
            $data['lbrand'] = "listbrand";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['brandlist'] = $this->brandsmodel->get_all_brands();
	       	//print_r($data);die;
	       	       	
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/brandslist');	
		   	$this->load->view('common/footer');	
		}
		
		function archivebrands($b_id){
		    
	       	$data = $this->brandsmodel->archive_brands($b_id);
	       	redirect(base_url().'admin/brands/brandslist');
		}
		
		function recoverbrands($b_id){
		    
	       	$data = $this->brandsmodel->recover_brands($b_id);
	       	redirect(base_url().'admin/brands/archivebrandslist');
		}
		
		function deletebrands($b_id){
		   
	       	$data = $this->brandsmodel->delete_brands($b_id);
	       	redirect(base_url().'admin/brands/archivebrandslist');
		}
		function archivebrandslist(){
		    
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Brands";
            $data['archbrand'] = "Archivebrand";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['archivebrandslist'] = $this->brandsmodel->get_archive_brands();
	       	//print_r($data);die;
	       	
	       	
$allidsarray=array();
if($this->input->post('submitdone')==TRUE){
if($this->input->post('deleteallhidden')=='0'){
$allarray=explode(',',$this->input->post('deleteallids'));
if($allarray[0]=='' || count($allarray)<count($this->input->post('checked_events'))){$allidsarray=$this->input->post('checked_events');}
else{$allidsarray=$allarray;}
}
else{
$allidsarray=$this->input->post('checked_events');
}
foreach($allidsarray as $id){
$this->brandsmodel->delete_brands($id);
}

redirect(base_url().'admin/brands/archivebrandslist');
}
	    	
	    		
		   $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   $this->load->view('common/header',$data);	
		   $this->load->view('common/sidebar');
		   $this->load->view('admin/archivebrands');	
		   $this->load->view('common/footer');	
		}
		
	function editbrands($b_id){
		     
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Brands";
            $data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['brandedit'] = $this->brandsmodel->get_edit_brands($b_id);
	    
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/editbrands');	
		   	$this->load->view('common/footer');	
		    
		}
		
	function updatebrand($b_id){
	    
	        $submit = $this->input->post('submit');
		    if ($submit)
	    	{
				$config['upload_path']='./uploads';
				$config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
				$config['file_name']=random_string('alnum', 50).".jpg";
				$this->load->library('upload',$config);
				$this->upload->initialize($config);
				if($this->upload->do_upload())
				{
					 $imgdata=$this->upload->data();
					 $image=$imgdata['file_name'];
				 }
                else
				{  
                   $image=$this->input->post('oldimage');
				}
				 $current_date_time = date("Y-m-d H:i:s");
                $data=array(
                     'b_name' => $this->input->post('bname'),
		   	    	  'b_details' => $this->input->post('bdetails'),
			    	  'b_logo' => $image,
			    	  'b_show_screen' => $this->input->post('show_screen'),
					  'updated_at' => $current_date_time
                );
	         $this->brandsmodel->update_brand($b_id, $data);
	         $this->session->set_flashdata('success',"Brand Updated Successfully");
             redirect(base_url().'admin/brands/brandslist');
	      	}
		
    	} 
		
	function updatestatus(){		 
		 $id = $this->input->post('id');
		 $status = $this->input->post('status');
		 $stat = $this->brandsmodel->update_status($id, $status);
		 echo $stat;
		 }

	function ajaxadd(){	
			if(is_uploaded_file($_FILES['userfile']['tmp_name'])) {
			$sourcePath = $_FILES['userfile']['tmp_name'];
			$imagname = random_string('alnum', 50).".jpg";
			$targetPath = "uploads/";
			$target_file = $targetPath.$imagname;
			move_uploaded_file($_FILES["userfile"]["tmp_name"], $target_file);
			}
			else
			{
			$imagname='';
			}
			 $current_date_time = date("Y-m-d H:i:s");
						$data = array(
								'b_name' => $this->input->post('bname'),
								'b_details' => $this->input->post('bdetails'),
								'b_logo' => $imagname,
								'b_added_by' =>  $this->session->userdata('adminsession')['name'],
								'created_at' => $current_date_time
		     );
			$this->brandsmodel->add_brands($data);
			$brandlist = $this->brandsmodel->get_all_brands();
			$storeslist = $this->storesmodel->get_all_stores();
			?>

			<select class="cs-select cs-skin-slide" data-init-plugin="cs-select" name="pbrand">
				<?php if($brandlist): foreach($brandlist as $row):?>
				<option value="<?php echo $row->b_id; ?>"><?php echo $row->b_name; ?></option>
				<?php endforeach; else:?>
				<p>No Brands Found</p>
				<?php endif; ?>
			</select>

			<?php echo 'sepratortext';?>
			<select class="cs-select cs-skin-slide" data-init-plugin="cs-select" id="prodselopt" name="pstore">
				<?php if($storeslist): foreach($storeslist as $row):?>
				<option value="<?php echo $row->s_id; ?>"><?php echo $row->s_name; ?></option>
				<?php endforeach; else:?>
				<p>No Stores Found</p>
				<?php endif; ?>
			</select>
			<script src="<?php echo base_url(); ?>assets/js/pages.min.js"></script>
			<?php
		}
		
		
}
