<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		     		$this->load->model('admin/brands_model', 'brandsmodel');
		     		$this->load->model('admin/stores_model', 'storesmodel');
		     		$this->load->model('admin/Notification_model', 'notificationmodel');
      	
		  if( !$this->checksession() ){
			 redirect(base_url().'admin/login');
		  }
			$this->load->model('admin/products_model', 'productsmodel');
		  $this->load->helper('string');
	}
	
	function index() {
		    
		$submit	= $this->input->post('submit');
		if ($submit)
		{ 
		    //echo "<pre>"; print_r($_POST); die;
			$this->form_validation->set_rules('size[]', 'Size', 'required');
			$this->form_validation->set_rules('color[]', 'Color', 'required');

		   if ($this->form_validation->run() == true) {
			   $groupMember_image = $_FILES['userfile2']['name'];
			   $banner_image = $_FILES['userfile1']['name'];
	 
				if($groupMember_image[0]!='')
				{
					$files = $_FILES;
					   $count = count($_FILES['userfile2']['name']);
						for($i=0; $i<$count; $i++)
						{
						$_FILES['userfile']['name']= $files['userfile2']['name'][$i];
						$_FILES['userfile']['type']= $files['userfile2']['type'][$i];
						$_FILES['userfile']['tmp_name']= $files['userfile2']['tmp_name'][$i];
						$_FILES['userfile']['error']= $files['userfile2']['error'][$i];
						$_FILES['userfile']['size']= $files['userfile2']['size'][$i];
						$config['upload_path'] = './uploads/';
						$config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
						$config['file_name']=random_string('alnum', 30).".jpg";
						$this->load->library('upload');
						$this->upload->initialize($config);
						$this->upload->do_upload('userfile');
						$theimgdata=$this->upload->data();
						$images[] = $theimgdata['file_name'];
						}
				}
                    
      

			     $config['upload_path']='./uploads/';
				 $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
				 $config['file_name']=random_string('alnum', 30).".jpg";
				 $this->load->library('upload');
				 $this->upload->initialize($config);
				 
				if(!$this->upload->do_upload('userfile1'))
				{ 
				  $image='product_default.png';
				 
				}
				else
				{ 
				  $imgdata=$this->upload->data();
				  $image=$imgdata['file_name'];  
				}

			    /* multi image upload end */      
				$size = implode(',', $this->input->post('size'));
				$color = implode(',', $this->input->post('color'));
				//print_r($pimage_array); die;
				$current_date_time = date("Y-m-d H:i:s");
				$register_array = array(
								'p_name' => $this->input->post('pname'),
								'p_currency' => $this->input->post('p_currency'),
                                                                'p_show_screen'=>$this->input->post('show_screen'),
								'p_original_price' => $this->input->post('poprice'),
								'p_new_price ' => $this->input->post('pnprice'),
								'p_detail' => $this->input->post('pdetail'),
								'p_store' => $this->input->post('pstore'),
								'p_brand' => $this->input->post('pbrand'),
								'p_size' => $size,
								'p_color' => $color,
								'p_added_by' => $this->session->userdata('adminsession')['name'],
								'p_image' => $image,
								'created_at' => $current_date_time
				);
		
				//print_r($register_array); die;
				$product_id =	$this->productsmodel->add_products($register_array);
				
				// start Add product offer 
				$p_date_range = $this->input->post('p_date_range');
				//print_r($p_date_range);die;
				$date_range = explode(' ',$p_date_range);
				
				
		
				//print_r($p_start_date);
				//print_r($p_end_date); die;
				if( $p_date_range != "" ){
				    
				    	$p_start_date = $date_range[0].' '.$date_range[1].' '.$date_range[2];
			$p_end_date = $date_range[4].' '.$date_range[5].' '.$date_range[6];
				    
				    
				    	$product_date_array = array(
								'p_start_date' => date_format(date_create($p_start_date),'Y-m-d H:i:s'),
								'p_end_date' => date_format(date_create($p_end_date),'Y-m-d H:i:s'),
							'product_id ' =>$product_id
				);
				    $this->productsmodel->add_products_date($product_date_array);
				}
				
				
				// End add product offer 
				
				// start notification insert data
				$ndata = array(	
						'n_id' => $product_id,
						'n_name' => $this->input->post('pname'),
						'n_added_by' => $this->session->userdata('adminsession')['name'],
						'n_type' => 1,
						'n_seen' => 1,
						'created_at' => $current_date_time
						
					);
				$this->notificationmodel->notification_data($ndata);
				// end notification insert data
		
				$current_date_time1 = date("Y-m-d H:i:s");
                             if(isset($images)){
				foreach($images as $val){
				$data1=array(
					'p_image_name' => $val,
					'p_id' => $product_id,
					'p_image_type' => 2,
					'created_at' => $current_date_time1);
				  
				   $this->productsmodel->add_products_image($data1);
					 
				 }
                              } 
		


					$allcat=$this->input->post('thecats');
					$storeid=$this->input->post('pstore');
					foreach($allcat as $val){
					$data3=array(
								'p_id' =>$product_id,
					's_id' =>$storeid,
					'cat_id' =>$val);
					$this->productsmodel->add_products_categories($data3);
					}
		  		$this->session->set_flashdata('success',"Product Added Successfully");
				redirect(base_url().'admin/products');
			}
		}
 		
            $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Add Product";
	    
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	    	$data['brandlist'] = $this->brandsmodel->get_all_brands();
	        $data['storeslist'] = $this->storesmodel->get_all_stores();
                $data['currency'] = $this->productsmodel->get_all_currency();
	        //print_r($data['storeslist'][0]->s_id); die;
	        if(isset($data['storeslist']))
	        $data['catlist'] = $this->productsmodel->get_all_categ($data['storeslist'][0]->s_id);
	        else{$data['catlist']=array();}
	                  
            $data['padd'] = 'addproduct';
		
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/products');	
		   	$this->load->view('common/footer');	
		}



    
	function getcategories(){
            
			$id=$this->input->post('id');
            $data['catlist'] = $this->productsmodel->get_all_categ($id);
?>       
			<select id="multi" class="full-width" name="thecats[]" multiple>
                <?php foreach( $data['catlist'] as $row):?>
                <option value="<?php echo $row->c_id; ?>"><?php echo $row->c_name; ?></option>
                <?php endforeach; ?>
            </select>
        <?php }
		
	function productslist($type=NULL,$id=NULL){
		    
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Product List";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	    	if($type!=NULL)
			   {	$data['productslist'] = $this->productsmodel->get_all_products($type,$id);}
			   else
			   {	$data['productslist'] = $this->productsmodel->get_all_products();}
					$data['plist'] = 'productlist';
				$i=0;
					foreach($data['productslist'] as $val){
						$data['productslist'][$i]->single= $this->productsmodel->get_pimages($val->p_id,1);
						$data['productslist'][$i]->multiple = $this->productsmodel->get_pimages($val->p_id,2); 
						$data['productslist'][$i]->productcategories= $this->productsmodel->get_all_productcategories($val->p_id);
					   $data['productslist'][$i]->activeproduct = $this->productsmodel->get_active_product_offer($val->p_id);
					   $data['productslist'][$i]->expiredproduct = $this->productsmodel->get_expire_product_offer($val->p_id);
					$i++;
						
					}
	       //echo "<pre>";	print_r($data['productslist']);die;
	         
               
	    
		
	       $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   $this->load->view('common/header',$data);	
		   $this->load->view('common/sidebar');
		   $this->load->view('admin/productslist');	
		   $this->load->view('common/footer');	
	}
		

    function editproduct($pid){
		    
        if($this->input->post('submitedit')==TRUE){

		  $this->form_validation->set_rules('size[]', 'Size', 'required');
			 $this->form_validation->set_rules('color[]', 'Color', 'required');

		if ($this->form_validation->run() == true) {
		  $groupMember_image = $_FILES['userfile2']['name'];
          $banner_image = $_FILES['userfile1']['name'];
 
          if($groupMember_image[0]!='')
          {
            $files = $_FILES;
               $count = count($_FILES['userfile2']['name']);
                for($i=0; $i<$count; $i++)
                {
                $_FILES['userfile']['name']= $files['userfile2']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile2']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile2']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile2']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile2']['size'][$i];
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
                $config['file_name']=random_string('alnum', 30).".jpg";
                $this->load->library('upload');
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $theimgdata=$this->upload->data();
                $images[] = $theimgdata['file_name'];
                }
          }
          else{$images=array();}
		  
		 $config['upload_path']='./uploads/';
		 $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
		 $config['file_name']=random_string('alnum', 30).".jpg";
		 $this->load->library('upload');
		 $this->upload->initialize($config);
		 
		if(!$this->upload->do_upload('userfile1'))
		{ 
		  $image=$this->input->post('oldimage');
		 
		}
		else
		{ 
		  $imgdata=$this->upload->data();
		  $image=$imgdata['file_name'];  }

		/* multi image upload end */      
		$size = implode(',', $this->input->post('size'));
		$color = implode(',', $this->input->post('color'));
		//print_r($pimage_array); die;
		    $current_date_time = date("Y-m-d H:i:s");
			$register_array = array(
								'p_name' => $this->input->post('pname'),
                                                                'p_currency' => $this->input->post('p_currency'),
                                                                'p_show_screen'=>$this->input->post('show_screen'),
								'p_original_price' => $this->input->post('poprice'),
								'p_new_price ' => $this->input->post('pnprice'),
								'p_detail' => $this->input->post('pdetail'),
								'p_store' => $this->input->post('pstore'),
								'p_brand' => $this->input->post('pbrand'),
								'p_size' => $size,
								'p_color' => $color,
								'p_added_by' => $this->session->userdata('adminsession')['name'],
								'p_image' => $image,
								'updated_at' => $current_date_time
								
		);
		
		//print_r($register_array); die;
		$this->productsmodel->update_products($register_array,$pid);
		$current_date_time1 = date("Y-m-d H:i:s");
		 foreach($images as $val){
        $data1=array(
          	'p_image_name' => $val,
			'p_id' => $pid,
			'p_image_type' => 2,
			'created_at' => $current_date_time1);
          
           $this->productsmodel->add_products_image($data1);
		     
	  	 }
	  	 	$p_date_range = $this->input->post('p_date_range');
				//print_r($p_date_range);die;
				$date_range = explode(' ',$p_date_range);
				
				
		if($p_date_range != ""){
		$check_product_date = $this->input->post('check_product_date');
	   if($check_product_date=="update"){
	$p_start_date = $date_range[0].' '.$date_range[1].' '.$date_range[2];
			$p_end_date = $date_range[4].' '.$date_range[5].' '.$date_range[6];
				$product_date_array = array(
								'p_start_date' => date_format(date_create($p_start_date),'Y-m-d H:i:s'),
								'p_end_date' => date_format(date_create($p_end_date),'Y-m-d H:i:s')
				);
				    $this->productsmodel->update_products_date($product_date_array, $pid);
			
	       
	       
	   }else{ 
	       
	       		$p_start_date = $date_range[0].' '.$date_range[1].' '.$date_range[2];
			$p_end_date = $date_range[4].' '.$date_range[5].' '.$date_range[6];
				$product_date_array = array(
								'p_start_date' => date_format(date_create($p_start_date),'Y-m-d H:i:s'),
								'p_end_date' => date_format(date_create($p_end_date),'Y-m-d H:i:s'),
								'product_id ' =>$pid
				);
				    $this->productsmodel->add_products_date($product_date_array);
			
	   }
		}
		  
			 
			$this->productsmodel->delete_products_categories($pid);
            if($this->input->post('thecats'))
            {$allcat=$this->input->post('thecats');}
            else{$allcat=explode(',',$this->input->post('hiddencats'));}
            $storeid=$this->input->post('pstore');
            foreach($allcat as $val){
            $data3=array(
                        'p_id' =>$pid,
			's_id' =>$storeid,
			'cat_id' =>$val);
            $this->productsmodel->add_products_categories($data3);
            }

	       	$this->session->set_flashdata('success',"Product updated Successfully");
                redirect(base_url().'admin/products/productslist');
	         	}

              }

		    
				$email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Favens | Admin Product List";
				$data['details'] = $this->dashboardmodel->get_admin_details($email);
                                $data['brandlist'] = $this->brandsmodel->get_all_brands();
				$data['storeslist'] = $this->storesmodel->get_all_stores();
				$data['productdetail'] = $this->productsmodel->get_single_product($pid);
                                $data['singimg'] = $this->productsmodel->get_pimages($pid,1);
                                $data['multimg'] = $this->productsmodel->get_pimages($pid,2);
                                $data['catlist'] = $this->productsmodel->get_all_categ($data['productdetail'][0]->p_store);
                                $pscat = $this->productsmodel->get_p_s_categories($pid);
                                $data['currency'] = $this->productsmodel->get_all_currency();
                                $data['expiredproduct'] = $this->productsmodel->get_expire_product_offer($pid);
             
                                $data['activeproduct'] = $this->productsmodel->get_active_product_offer($pid);
                        //echo "<pre>";print_r($data['activeproduct']);die;
                                $catarray=array();
				foreach($pscat as $val){
				$catarray[]=$val->cat_id;
				}
					  
			$data['p_s_categories']=$catarray;

                        //echo "<pre>";print_r($data['productdetail']);die;
                
	      
	                $data['notificationdata'] = $this->notificationmodel->get_notification_data();

		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		        $this->load->view('admin/editproduct');	
		   	$this->load->view('common/footer');	
		}

    function deleteimages(){
                   $imageid=$this->input->post('imgid');
                   $this->productsmodel->delete_pimages($imageid);

    }
		
	function archiveproductslist(){
		    
		    
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Archive Products";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['archiveproductslist'] = $this->productsmodel->get_archive_products();
                $data['parchive'] = 'archiveproducts';
                
                $i=0;
                foreach($data['archiveproductslist'] as $val){
                   	$data['archiveproductslist'][$i]->single= $this->productsmodel->get_pimages($val->p_id,1);
                	$data['archiveproductslist'][$i]->multiple = $this->productsmodel->get_pimages($val->p_id,2); 
                	$i++;
                }  
            
$allidsarray=array();
if($this->input->post('submitdone')==TRUE){
if($this->input->post('deleteallhidden')=='0'){
$allarray=explode(',',$this->input->post('deleteallids'));
if($allarray[0]=='' || count($allarray)<count($this->input->post('checked_events'))){$allidsarray=$this->input->post('checked_events');}
else{$allidsarray=$allarray;}
}
else{
$allidsarray=$this->input->post('checked_events');
}
foreach($allidsarray as $id){
$this->productsmodel->delete_products($id);

}
redirect(base_url().'admin/products/archiveproductslist');
}
	    	    
                
	       	//print_r($data);die;	    
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();		
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/archiveproducts');	
		   	$this->load->view('common/footer');	
	}
		
			
	function archiveproducts($p_id){
		    
	       	$data = $this->productsmodel->archive_products($p_id);
	       	redirect(base_url().'admin/products/productslist');
	}
		
	function recoverproducts($p_id){
		    
	       	$data = $this->productsmodel->recover_products($p_id);
	       	redirect(base_url().'admin/products/archiveproductslist');
	}
		
	function deleteproducts($p_id){
		    
	       	$data = $this->productsmodel->delete_products($p_id);
	       	redirect(base_url().'admin/products/archiveproductslist');
	}
				
	function updatestatus(){
		 
		 $id = $this->input->post('id');
		 $status = $this->input->post('status');
		 $stat = $this->productsmodel->update_status($id, $status);
		 echo $stat;
		 
	}
		
}
