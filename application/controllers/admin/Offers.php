<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offers extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
      	
      if( !$this->checksession() ){
         redirect(base_url().'admin/login');
      }
		$this->load->model('admin/offers_model', 'offersmodel');
      $this->load->helper('string');
	}
	function index() {
		    
		$submit	= $this->input->post('submit');
		if ($submit)
		{
                    
            $odate=date_format(date_create($this->input->post('date')),"Y-m-d");
		    $current_date_time = date("Y-m-d H:i:s");
			$register_array = array(
								'o_title' => $this->input->post('otitle'),
								'o_detail' => $this->input->post('odetail'),
								'o_date' => $odate,
								'o_added_by' =>  $this->session->userdata('adminsession')['name'],
								'created_at' => $current_date_time
		);
		
		//print_r($register_array); die;
		$this->offersmodel->add_offers($register_array);
		$this->session->set_flashdata('success',"Record Added Successfully");
		}
		
            $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Offers";
	        $data['oadd'] = "Offer Add";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	    
		    $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/offers');	
		   	$this->load->view('common/footer');	
	}
		
	function activeofferslist(){
		    		    
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Offers";
            $data['oactive'] = "Active Offers";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['activeofferslist'] = $this->offersmodel->get_all_offers();
	       	//print_r($data);die;
	    
		    $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/activeoffers');	
		   	$this->load->view('common/footer');	
	}
		
	function archiveofferslist(){
		    
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Offers";
            $data['oarchive'] = "Archive Offers";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['archiveofferslist'] = $this->offersmodel->get_archive_offers();
	       	//print_r($data);die;
	        
	        $allidsarray=array();
if($this->input->post('submitdone')==TRUE){
if($this->input->post('deleteallhidden')=='0'){
$allarray=explode(',',$this->input->post('deleteallids'));
if($allarray[0]=='' || count($allarray)<count($this->input->post('checked_events'))){$allidsarray=$this->input->post('checked_events');}
else{$allidsarray=$allarray;}
}
else{
$allidsarray=$this->input->post('checked_events');
}
foreach($allidsarray as $id){
$this->offersmodel->delete_offers($id);

}
redirect(base_url().'admin/offers/archiveofferslist');
} 

		    $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/archiveoffers');	
		   	$this->load->view('common/footer');	
	}


    function expireoffers(){
		    
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Offers";
            $data['oexpired'] = "Expired Offers";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['expireofferslist'] = $this->offersmodel->get_expire_offers();
	       	//print_r($data);die;
	    
		    $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/expireoffers');	
		   	$this->load->view('common/footer');	
	}
		
	function archiveoffers($o_id){
		    
			$data = $this->offersmodel->archive_offers($o_id);
			redirect(base_url().'admin/offers/archiveofferslist');
	}
		
	function recoveroffers($o_id){
		    
	       	$data = $this->offersmodel->recover_offers($o_id);
	       	redirect(base_url().'admin/offers/archiveofferslist');
	}
		
	function deleteoffers($o_id){
		    
	       	$data = $this->offersmodel->delete_offers($o_id);
	       	redirect(base_url().'admin/offers/archiveofferslist');
	}
		
	function updatestatus(){
		 
			 $id = $this->input->post('id');
			 $status = $this->input->post('status');
			 $stat = $this->offersmodel->update_status($id, $status);
			 echo $stat;
		 
	}
		
	function editoffer($o_id){
		     
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin offer";
            $data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['offeredit'] = $this->offersmodel->get_edit_offers($o_id);
	    
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		    $this->load->view('admin/editoffer');	
		   	$this->load->view('common/footer');	
		    
	}
		
	function updateoffer($o_id){
	    
	        $submit = $this->input->post('submit');
			if ($submit)
			{
			 $odate=date_format(date_create($this->input->post('date')),"Y-m-d");
			 $current_date_time = date("Y-m-d H:i:s");
		     $data=array(
					  'o_title' => $this->input->post('otitle'),
					  'o_detail' => $this->input->post('odetail'),
					  'o_date' => $odate,
					  'updated_at' => $current_date_time
        
                 );
		
				$this->offersmodel->update_offer($o_id, $data);
				$this->session->set_flashdata('success',"Store Updated Successfully");
				redirect(base_url().'admin/offers/activeofferslist');
			}
		
	} 
		
		
}
