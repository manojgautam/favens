<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Dashboard_model', 'dashboardmodel');
      	
      if( !$this->checksession() ){
         redirect(base_url().'admin/login');
      }
		$this->load->model('admin/Register_model', 'registermodel');
      $this->load->helper('string');
	}
		function index() {
		    
		       $submit			= $this->input->post('submit');
		if ($submit)
		{

   $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[admin.email]');
   $this->form_validation->set_rules('tpassword', 'Password', 'trim|min_length[4]');

  if ($this->form_validation->run() == true) {
		   /* image upload start */
   echo  $config['upload_path']='./uploads/';
     $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
     $config['file_name']=random_string('alnum', 40).".jpg";
     $this->load->library('upload');
     $this->upload->initialize($config);
     
    if(!$this->upload->do_upload())
    { 
      $image='';
   
      
    }
    else
    { 
      $imgdata=$this->upload->data();
      $image=$imgdata['file_name'];  }
      /* image upload end */
      
		    $current_date_time = date("Y-m-d H:i:s");
			$register_array = array(
								'name' => $this->input->post('username'),
								'email' => $this->input->post('email'),
								'password' => md5($this->input->post('tpassword')),
								'image' => $image,
								'created_at' => $current_date_time
		);
		
	
		$this->registermodel->add_admin($register_array);
		$this->session->set_flashdata('success',"Record Added Successfully");
		}
            }
		
            $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Registeration";
	        $data['aadmin'] = "addadmin";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	    
		
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		   $this->load->view('admin/register');	
		   	$this->load->view('common/footer');	
		}
		
		function adminlist(){
		    
		    
		     $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin List";
                $data['ladmin'] = "listadmin";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['list'] = $this->registermodel->get_all_admin();
	       	//print_r($data);die;
	    
		
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		   $this->load->view('admin/adminlist');	
		   	$this->load->view('common/footer');	
		}
		
        function deleteadmin($id){

		$this->registermodel->deleteadmin($id);
		redirect(base_url().'admin/register/adminlist');
		 
		}
		
		function updatestatus(){
		 
		 $id = $this->input->post('id');
		 $status = $this->input->post('status');
		 $stat = $this->registermodel->update_status($id, $status);
		 echo $stat;
		 
		}
		
}
