<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
         	$this->load->model('admin/Settings_model', 'settingsmodel');
                $this->load->helper('string');
      	
			if( !$this->checksession() ){
			 redirect(base_url().'admin/login');
			}
	}
		
	function index() {
			$email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Setting";
	        $data['setting'] = "setting";
	    	$data['details'] = $this->settingsmodel->get_setting_detail($email);
	    		
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
			$this->load->view('common/header',$data);	
			$this->load->view('common/sidebar');
			$this->load->view('admin/settings');
			$this->load->view('common/footer');	

	}
	function settings(){
	    
		    $submit = $this->input->post('submitaccount');
			if ($submit)
			{
		  
			  $config['upload_path']='./uploads';
			  $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
			  $config['file_name']=random_string('alnum', 50).".jpg";
			  $this->load->library('upload',$config);
				$this->upload->initialize($config);
				if($this->upload->do_upload())
				{
				  $imgdata=$this->upload->data();
				   $image=$imgdata['file_name'];
				 }
				else
				{  
				 $image=$this->input->post('oldimage');
			   }
            $current_date_time = date("Y-m-d H:i:s");
			$data=array(
			'name'=>$this->input->post('name'),
			'image'=>$image,
			'updated_at'=> $current_date_time
			);
		
			$this->settingsmodel->update_setting($data);
			$this->session->set_flashdata('success',"Account Updated Successfully");
            redirect(base_url().'admin/settings');
			}	  
		} 

		function changepassword(){
	    
	        $submit = $this->input->post('submitpass');
			if ($submit)
			{
		   $this->form_validation->set_rules('oldpass', 'Old Passowrd', 'required|callback_password_check');
		   $this->form_validation->set_rules('newpass', 'Password', 'trim|required|min_length[4]');
		   $this->form_validation->set_rules('confirmpass', 'Password Confirmation', 'trim|required|matches[newpass]');
			if ($this->form_validation->run() == true) {
				  
		    if($this->input->post('newpassword')){$pass=md5($this->input->post('newpassword'));}
		    else{$pass=$this->input->post('chkoldpass');}

			$data=array(
			'password'=>md5($this->input->post('newpass'))
			);

				$this->settingsmodel->update_setting($data);
				$this->session->set_flashdata('success',"Password Updated Successfully");
						redirect(base_url().'admin/settings');
				}
			}
			
			$email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Setting";
	    
	    	$data['details'] = $this->settingsmodel->get_setting_detail($email);
	    		
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
			$this->load->view('common/header',$data);	
			$this->load->view('common/sidebar');
			$this->load->view('admin/settings');
			$this->load->view('common/footer');	
           
	  	} 

	public function password_check($str)
        {     
			$email =   $this->session->userdata('adminsession')['email'];
			$chkpass=$this->settingsmodel->check_password(md5($str),$email);
			  
			if (!count($chkpass))
			{
					$this->form_validation->set_message('password_check', 'The {field} field does not match');
					return FALSE;
			}
			else
			{
					return TRUE;
			}
        }
		
}
