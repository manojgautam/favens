<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
      	$this->load->model('admin/Dashboard_model', 'dashboardmodel');
      	      	$this->load->model('admin/User_model', 'usermodel');
      	
		if( !$this->checksession() ){
			 redirect(base_url().'admin/login');
		  }
	}
		
	function index() {
			$email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Users dashboard";
	        $data['users'] = "users";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	    	$data['userdetail'] = $this->usermodel->get_user_detail();



$allidsarray=array();
if($this->input->post('submitdone')==TRUE){
if($this->input->post('deleteallhidden')=='0'){
$allarray=explode(',',$this->input->post('deleteallids'));
if($allarray[0]=='' || count($allarray)<count($this->input->post('checked_events'))){$allidsarray=$this->input->post('checked_events');}
else{$allidsarray=$allarray;}
}
else{
$allidsarray=$this->input->post('checked_events');
}
foreach($allidsarray as $id){
$this->usermodel->delete_user($id);
}
redirect(base_url().'admin/users');
}
	    	
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
			$this->load->view('common/header',$data);	
			$this->load->view('common/sidebar');
			$this->load->view('admin/users');
			$this->load->view('common/footer');	

	}	
	
    function updatestatususer(){		 
		 $id = $this->input->post('id');
		 $status = $this->input->post('status');
		 $stat = $this->usermodel->update_status($id, $status);
		 echo $stat;
		 }
		
	function deleteuser($uid){
		 $this->usermodel->delete_user($uid);
		 redirect(base_url().'admin/users');
		}
		
	function today(){
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Users dashboard";
	        $data['users'] = "users";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	    	$data['userdetail'] = $this->usermodel->get_todayuser_detail();
	    			
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
			$this->load->view('common/header',$data);	
			$this->load->view('common/sidebar');
			$this->load->view('admin/users');
			$this->load->view('common/footer');	
		}
			
}
