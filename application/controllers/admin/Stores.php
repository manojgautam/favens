<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stores extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		
                        $this->load->model('admin/brands_model', 'brandsmodel');
      	
			  if( !$this->checksession() ){
				 redirect(base_url().'admin/login');
			  }
				$this->load->model('admin/stores_model', 'storesmodel');
			  $this->load->helper('string');
	}
	function index() {
		    
		    $submit	= $this->input->post('submit');
			if ($submit)
			{
		     /* image upload start */
			 $config['upload_path']='./uploads/';
			 $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
			 $config['file_name']=random_string('alnum', 40).".jpg";
			 $this->load->library('upload');
			 $this->upload->initialize($config);
			 
			  if(!$this->upload->do_upload())
			   { 
			     $image='';			 
			    }
				else
				{ 
				$imgdata=$this->upload->data();
				$image=$imgdata['file_name'];  
				}
				/* image upload end */
			  
			$current_date_time = date("Y-m-d H:i:s");
			$register_array = array(
								's_name' => $this->input->post('sname'),
								's_location' => $this->input->post('slocation'),
								's_image' => $image,
								's_show_screen' => $this->input->post('show_screen'),
								's_added_by' =>  $this->session->userdata('adminsession')['name'],
								'created_at' => $current_date_time
						);
		
				//print_r($register_array); die;
			$store_id =	$this->storesmodel->add_stores($register_array);
				
				
			// start notification insert data
			$ndata= array(
		         'n_id' => $store_id,
		        'n_name' => $this->input->post('sname'),
		        'n_added_by' => $this->session->userdata('adminsession')['name'],
		        'n_type' => 3,
		        'n_seen' => 1,
		        'created_at' => $current_date_time
		    );
		    $this->notificationmodel->notification_data($ndata);
			// end notification insert data
		
			$this->session->set_flashdata('success',"Record Added Successfully");
			}
				
				$email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Favens | Admin Stores";
				$data['astore'] = "addstore";
				$data['details'] = $this->dashboardmodel->get_admin_details($email);
				
				$data['notificationdata'] = $this->notificationmodel->get_notification_data();
				$this->load->view('common/header',$data);	
				$this->load->view('common/sidebar');
				$this->load->view('admin/stores');	
				$this->load->view('common/footer');	
		}
		
	function storeslist(){
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Stores";
            $data['lstore'] = "liststore";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['storeslist'] = $this->storesmodel->get_all_stores();
	       	       	
	       	 $i=0;
                foreach($data['storeslist'] as $val){
                   	$data['storeslist'][$i]->storecategories= $this->storesmodel->get_all_storecategories($val->s_id);
                	$i++;
                }
	    
	       //echo "<pre>";   	print_r($data['storeslist']);die;
		
	       $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   $this->load->view('common/header',$data);	
		   $this->load->view('common/sidebar');
		   $this->load->view('admin/storeslist');	
		   $this->load->view('common/footer');	
		}
		
	function archivestoreslist(){
		    
		$email = $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Stores";
                $data['archstore'] = "archivestore";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['archivestoreslist'] = $this->storesmodel->get_archive_stores();
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();

$allidsarray=array();
if($this->input->post('submitdone')==TRUE){
if($this->input->post('deleteallhidden')=='0'){
$allarray=explode(',',$this->input->post('deleteallids'));
if($allarray[0]=='' || count($allarray)<count($this->input->post('checked_events'))){$allidsarray=$this->input->post('checked_events');}
else{$allidsarray=$allarray;}
}
else{
$allidsarray=$this->input->post('checked_events');
}
foreach($allidsarray as $id){
$this->storesmodel->delete_stores($id);
}
redirect(base_url().'admin/stores/archivestoreslist');
}

		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
		        $this->load->view('admin/archivestores');	
		   	$this->load->view('common/footer');	
		}
		
	function updatestatus(){
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			$stat = $this->storesmodel->update_status($id, $status);
			echo $stat;
		 }
		
	function archivestores($s_id){
		    $data = $this->storesmodel->archive_stores($s_id);
	       	redirect(base_url().'admin/stores/storeslist');
		}
		
	function recoverstores($s_id){		    
	       	$data = $this->storesmodel->recover_stores($s_id);
	       	redirect(base_url().'admin/stores/archivestoreslist');
		}
		
	function deletestores($s_id){		    
	       	$data = $this->storesmodel->delete_stores($s_id);
	       	redirect(base_url().'admin/stores/archivestoreslist');
		}
						
	function editstore($s_id){		     
		    $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Admin Store";
            $data['details'] = $this->dashboardmodel->get_admin_details($email);
	       	$data['storeedit'] = $this->storesmodel->get_edit_stores($s_id);
	    
	        $data['notificationdata'] = $this->notificationmodel->get_notification_data();
		   	$this->load->view('common/header',$data);	
		   	$this->load->view('common/sidebar');
			$this->load->view('admin/editstore');	
		   	$this->load->view('common/footer');	
		    
		}
		
	function updatestore($s_id){	    
			$submit = $this->input->post('submit');
			if ($submit)
			{
				 
				$config['upload_path']='./uploads';
				$config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
				$config['file_name']=random_string('alnum', 50).".jpg";
				$this->load->library('upload',$config);
				$this->upload->initialize($config);
				if($this->upload->do_upload())
				{
				  $imgdata=$this->upload->data();
				   $image=$imgdata['file_name'];
				 }
				else
				{  
				 $image=$this->input->post('oldimage');
			    }
				$current_date_time = date("Y-m-d H:i:s");
  			    $data=array(
						  's_name' => $this->input->post('sname'),
						  's_location' => $this->input->post('slocation'),
						  's_image' => $image,
						  's_show_screen' => $this->input->post('show_screen'),
						  'updated_at' => $current_date_time
				 );
				
				$this->storesmodel->update_store($s_id, $data);
				$this->session->set_flashdata('success',"Store Updated Successfully");
				redirect(base_url().'admin/stores/storeslist');
			}
		
    	} 
    	
    			
	function categories($type=NULL,$id=NULL){
		    		    
			  $submit = $this->input->post('submit');
				if ($submit)
				{
					$current_date_time = date("Y-m-d H:i:s");						 
				    $categories_array = array(
									'c_name' => $this->input->post('cname'),
									'c_added_by' =>  $this->session->userdata('adminsession')['name'],
									'created_at' => $current_date_time
					);
					
				$this->storesmodel->add_categories($categories_array);
				$this->session->set_flashdata('success',"Record Added Successfully");
				}
								
				$email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Favens | Admin Stores";
				$data['cstore'] = "categoriesstore";
				$data['categorlist'] = $this->storesmodel->get_all_categories();
				$data['details'] = $this->dashboardmodel->get_admin_details($email);
				if($type!=NULL)
			    {
					 $data['storeslist'] = $this->storesmodel->get_all_stores($type,$id);
				}
			    else
			    {
				 $data['storeslist'] = $this->storesmodel->get_all_stores();
				}
			

$allidsarray=array();
if($this->input->post('submitdone')==TRUE){
if($this->input->post('deleteallhidden')=='0'){
$allarray=explode(',',$this->input->post('deleteallids'));
if($allarray[0]=='' || count($allarray)<count($this->input->post('checked_events'))){$allidsarray=$this->input->post('checked_events');}
else{$allidsarray=$allarray;}
}
else{
$allidsarray=$this->input->post('checked_events');
}
foreach($allidsarray as $id){
$this->storesmodel->delete_categories($id);
}
redirect(base_url().'admin/stores/categories');
}
						
	            $data['notificationdata'] = $this->notificationmodel->get_notification_data();
				$this->load->view('common/header',$data);	
				$this->load->view('common/sidebar');
			    $this->load->view('admin/categories');	
				$this->load->view('common/footer');	
		}
		
	function managecategories(){
		    		    
			    $submit = $this->input->post('submit');
				if ($submit)
				{
					 $current_date_time = date("Y-m-d H:i:s");
				     $categories_array = array(
									'c_name' => $this->input->post('cname'),
									'c_store' => $this->input->post('storeid'),
									'c_added_by' =>  $this->session->userdata('adminsession')['name'],
									'created_at' => $current_date_time
					);
					
				$this->storesmodel->add_categories($categories_array);
				$this->session->set_flashdata('success',"Record Added Successfully");
				redirect(base_url().'admin/stores/storeslist');
				}
					
				$email =   $this->session->userdata('adminsession')['email'];
				$data['title'] = "Favens | Admin Stores";
				$data['cstore'] = "categoriesstore";				
				$data['details'] = $this->dashboardmodel->get_admin_details($email);
				
				
	            $data['notificationdata'] = $this->notificationmodel->get_notification_data();
				$this->load->view('common/header',$data);	
				$this->load->view('common/sidebar');
			    $this->load->view('admin/storeslist');	
				$this->load->view('common/footer');	
		}
		
		
	function categoriesdelete($c_id)
		{
		    $data = $this->storesmodel->delete_categories($c_id);
	       	redirect(base_url().'admin/stores/categories');
		    
		}
		
	function updatestatuscat(){
		 
			 $id = $this->input->post('id');
			 $status = $this->input->post('status');
			 $stat = $this->storesmodel->update_status_cat($id, $status);
			 echo $stat;
		 }
		
	function ajaxadd(){
		
			if(is_uploaded_file($_FILES['userfilestore']['tmp_name'])) {
			$sourcePath = $_FILES['userfilestore']['tmp_name'];
			$imagname = random_string('alnum', 50).".jpg";
			$targetPath = "uploads/";
			$target_file = $targetPath.$imagname;
			move_uploaded_file($_FILES["userfilestore"]["tmp_name"], $target_file);
			}
			else{
			$imagname='';
			}

			 $current_date_time = date("Y-m-d H:i:s");
						$data = array(
											's_name' => $this->input->post('sname'),
								's_location' => $this->input->post('slocation'),
								's_image' => $imagname,
								's_added_by' =>  $this->session->userdata('adminsession')['name'],
								'created_at' => $current_date_time
						);
			$this->storesmodel->add_stores($data);
			$brandlist = $this->brandsmodel->get_all_brands();
			$storeslist = $this->storesmodel->get_all_stores();
			?>
			<select class="cs-select cs-skin-slide" data-init-plugin="cs-select" name="pbrand">
				<?php if($brandlist): foreach($brandlist as $row):?>
					<option value="<?php echo $row->b_id; ?>"><?php echo $row->b_name; ?></option>
						<?php endforeach; else:?>
						<p>No Brands Found</p>
						<?php endif; ?>
			</select>
			<?php echo 'sepratortext';?>
			<select class="cs-select cs-skin-slide" data-init-plugin="cs-select" id="prodselopt" name="pstore">
					<?php if($storeslist): foreach($storeslist as $row):?>
					<option value="<?php echo $row->s_id; ?>"><?php echo $row->s_name; ?></option>
					<?php endforeach; else:?>
					<p>No Stores Found</p>
					<?php endif; ?>
								 
			</select>
			<script src="<?php echo base_url(); ?>assets/js/pages.min.js"></script>
			<?php
		}
	
	
}
