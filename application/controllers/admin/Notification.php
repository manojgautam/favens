<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

      if( !$this->checksession() ){
         redirect(base_url().'admin/login');
      }
	}
		
	function index() {
	    
		     $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Notification  List";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	   
	       $data['notificationdata'] = $this->notificationmodel->get_notification_data();
	     
			$this->load->view('common/header',$data);	
			$this->load->view('common/sidebar');
			$this->load->view('admin/notification');
			$this->load->view('common/footer');	

	}	

	function notificationdelete(){
	       $email =   $this->session->userdata('adminsession')['email'];
	    	$data['title'] = "Favens | Notification  List";
	    	$data['details'] = $this->dashboardmodel->get_admin_details($email);
	   
	       $data['notificationdata'] = $this->notificationmodel->delete_notifaction();
	     redirect(base_url().'admin/notification');
	}

	
}
