<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Dashboard_model', 'dashboardmodel');
        $this->load->model('admin/Notification_model', 'notificationmodel');

        date_default_timezone_set("Asia/Kolkata");
    }

    public function checksession() {
        if ($this->session->userdata('adminsession') != '') {
            return true;
        } else {
            return false;
        }
    }

    public function globalmailfunction($email, $subject, $message) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'karan.techindustan@gmail.com',
            'smtp_pass' => 'karan@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        $this->email->from('favens@gmail.com', 'Favens');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
    }

    public function checkPostData($postArray) {
        foreach ($postArray as $value) {
            if (empty($value) || strlen($value) == 0) {
                echo json_encode(array('status' => 500, 'error' => MANDATORY_ERROR));
                exit();
            }
        }
        return true;
    }

    # to generate images with different sizes.

    public function differentLogoSizes($images, $key, $function) {
        $count = count($images);
        for ($i = 0; $i < $count; $i++) {
            if ($function == 'login') {
                $images[$i]['logo_path_original'] = TIMTHUMB_URL . "?src=" . $images[$i][$key] . "&w=600&h=400&zc=1";
                $images[$i][$key . '_228_114'] = TIMTHUMB_URL . "?src=" . $images[$i][$key] . "&w=228&h=114&zc=2";
                $images[$i][$key . '_152_76'] = TIMTHUMB_URL . "?src=" . $images[$i][$key] . "&w=152&h=76&zc=2";
                $images[$i][$key . '_76_38'] = TIMTHUMB_URL . "?src=" . $images[$i][$key] . "&w=76&h=38&zc=2";
            }
            if ($function == 'product') {
                $imageURL = ($images[$i][$key] == 'product_default.png')? DEFAULT_IMAGE_URL . $images[$i][$key] : BASE_IMAGE_URL . $images[$i][$key];
                $images[$i]['product_image'] = $imageURL;
                $images[$i]['product_image_thumbnails'][$key . '_100_100'] = TIMTHUMB_URL . "?src=" . $imageURL . "&w=100&h=100";
                $images[$i]['product_image_thumbnails'][$key . '_200_200'] = TIMTHUMB_URL . "?src=" . $imageURL . "&w=200&h=200";
                $images[$i]['product_image_thumbnails'][$key . '_300_300'] = TIMTHUMB_URL . "?src=" . $imageURL . "&w=300&h=300";
            }
        }
        return $images;
    }

}
