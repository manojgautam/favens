<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    # function to authenticate device token from the server.

    public function authenticateDeviceToken($token) {
        $this->db->select('user_id, u_access_token');
        $this->db->from('users');
        $this->db->where(array('u_device_token' => $token));
        $query = $this->db->get();
        if (!$query->num_rows()) {
            echo json_encode(array('status' => 401, 'error' => TOKEN_ERROR));
            exit();
        } else {
            $result = $query->result_array();
            return $result;
        }
    }

    # function to authenticate access token from the server.

    public function authenticateAccessToken($token) {
        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where(array('u_access_token' => $token));
        $query = $this->db->get();
        if (!$query->num_rows()) {
            echo json_encode(array('status' => 401, 'error' => TOKEN_ERROR));
            exit();
        } else {
            $result = $query->result_array();
            return $result[0]['user_id'];
        }
    }

    # function to authenticate access token and get multiple columns from the server.

    public function authenticateAccessTokenAndGetColumns($token) {
        $this->db->select('user_id, u_email, is_active');
        $this->db->from('users');
        $this->db->where(array('u_access_token' => $token));
        $query = $this->db->get();
        if (!$query->num_rows()) {
            echo json_encode(array('status' => 401, 'error' => TOKEN_ERROR));
            exit();
        } else {
            $result = $query->result_array();
            return $result;
        }
    }

    # function to get device token and device type from access token

    public function getDeviceTokenAndDeviceTypeFromAccessToken($token) {
        $this->db->select('u_device_token, u_device_type');
        $this->db->from('users');
        $this->db->where(array('u_access_token' => $token));
        $query = $this->db->get();
        if (!$query->num_rows()) {
            echo json_encode(array('status' => 401, 'error' => TOKEN_ERROR));
            exit();
        } else {
            $result = $query->result_array();
            return $result;
        }
    }

    # function to check email duplicacy.

    public function authenticateEmailAndUsername($email) {
        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where('u_email', $email);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows()) {
            echo json_encode(array('status' => 400, 'error' => EMAIL_ERROR));
            exit();
        } else {
            return true;
        }
    }

    # function to check email existance.

    public function checkEmail($email) {
        $query = $this->db->get_where('users', array('u_email' => $email));
        if ($query->num_rows()) {
            return true;
        } else {
            echo json_encode(array('status' => 400, 'error' => EMAIL_NOT_ERROR));
            exit();
        }
    }

}
