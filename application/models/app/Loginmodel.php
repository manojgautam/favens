<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    # function to register a user with email.

    public function registerWithDeviceTokenAndEmail($data) {
        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where(array('u_device_token' => $data['u_device_token']));
        $this->db->limit(1);
        $query = $this->db->get();

        # check if user exists or not.
        if ($query->num_rows()) {
            # update device token
            $result = $query->row();
            $this->db->where('user_id', $result->user_id);
            $this->db->update('users', array(
                "u_email" => $data['u_email'],
                "u_device_type" => $data['u_device_type'],
                "u_access_token" => $data['u_access_token']
                    )
            );
            return ($this->db->affected_rows() != 1) ? false : true;
        } else {
            $this->db->insert('users', $data);
            return ($this->db->affected_rows() != 1) ? false : true;
        }
    }

    # function to check email existance in account details screen

    public function authenticateEmailForAccount($accessToken, $email) {
        $this->db->select("u_access_token");
        $this->db->from('users');
        $this->db->where(array('u_email' => $email));
        $query = $this->db->get();
        $result = $query->row();
        if (count($result) && $result->u_access_token != $accessToken) {
            echo json_encode(array('status' => 400, 'error' => EMAIL_ERROR));
            exit();
        } else {
            return true;
        }
    }

    # function to get all logos added by Admin

    public function getLogosAddedByAdmin() {
        $this->db->select("CONCAT('" . BASE_IMAGE_URL . "', '', l_logo) as logo_path_original, 10 as store_id, 'The Galleria' as store_name");
        $this->db->from('logos');
        $query = $this->db->get();
        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    # function to login with Access Token

    public function loginWithAccessToken($user_id) {
        $this->db->select('u_email, is_active');
        $this->db->from('users');
        $this->db->where(array('user_id' => $user_id));
        $this->db->limit(1);
        $query = $this->db->get();

        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    # function to update account details.

    public function updateAccountDetails($data, $user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->update('users', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

}
