<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    # function to list all the products added by admin

    public function listAllProductsAddedByAdmin() {
        $this->db->select(""
                . "p.p_id as product_id, "
                . "p.p_name as product_name, "
                . "CONCAT(p.p_currency, '', p.p_original_price) as product_original_price, "
                . "CONCAT(p.p_currency, '', p.p_new_price) as product_new_price, "
                . "p.p_detail as product_detail, "
                . "IF(b.b_name IS NULL, '', b.b_name)  as product_brand, "
                . "p_image as product_image, "
                . "p.p_size as product_sizes, "
                . "p.p_color as product_colors, "
                . "p.p_currency as product_currency, "
                . "IF(s.s_name IS NULL, '', s.s_name) as store_name, "
                . "IF(s.s_id IS NULL, '', s.s_id) as store_id");
        $this->db->from('products p');
        $this->db->join('stores s', 's.s_id = p.p_store', 'LEFT');
        $this->db->join('brands b', 'b.b_id = p.p_brand', 'LEFT');
        $this->db->where(array('p.p_status' => 1, 'p.p_archive' => 0));
        $query = $this->db->get();
        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }
    
    # function to list all brands
    
    public function listAllBrandsAddedByAdmin() {
        $this->db->select("b_id as brand_id, b_name as brand_name");
        $this->db->from('brands');
        $this->db->where(array('b_status' => 1, 'b_archive' => 0));
        $query = $this->db->get();
        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    # function to get product details

    public function getProductDetailsFromID($product_id) {
        $this->db->select(""
                . "p.p_id as product_id, "
                . "p.p_name as product_name, "
                . "CONCAT(p.p_currency, '', p.p_original_price) as product_original_price, "
                . "CONCAT(p.p_currency, '', p.p_new_price) as product_new_price, "
                . "p.p_detail as product_detail, "
                . "IF(b.b_name IS NULL, '', b.b_name)  as product_brand, "
                . "p_image as product_image, "
                . "p.p_size as product_sizes, "
                . "p.p_color as product_colors, "
                . "p.p_currency as product_currency, "
                . "IF(s.s_name IS NULL, '', s.s_name) as store_name, "
                . "IF(s.s_location IS NULL, '', s.s_location) as store_location, "
                . "IF(s.s_id IS NULL, '', s.s_id) as store_id");
        $this->db->from('products p');
        $this->db->join('stores s', 's.s_id = p.p_store', 'LEFT');
        $this->db->join('brands b', 'b.b_id = p.p_brand', 'LEFT');
        $this->db->where(array('p.p_id' => $product_id));
        $this->db->limit(1);
        $query = $this->db->get();
        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    # function to get product images

    public function getProductOtherImagesFromID($product_id) {
        $this->db->select("p_image_name");
        $this->db->from('product_images');
        $this->db->where(array('p_id' => $product_id));
        $query = $this->db->get();
        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }
    
    # function to save Bevaka Values
    
    public function saveBevakaValues($data) {
        $this->db->insert('user_bevaka', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

}
