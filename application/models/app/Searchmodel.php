<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    # function to list all the keywords.

    public function listAllKeywordsFromSearchBox($keyword) {
        $query = $this->db->query(
                "(SELECT s_id as keyword_id, s_name as keyword, 'store' as keyword_type "
                . "FROM stores_view "
                . "WHERE (s_name LIKE '%$keyword%' OR s_location LIKE '%$keyword%') LIMIT 3) "
                . "UNION ALL "
                . "(SELECT b_id as keyword_id, b_name as keyword, 'brand' as keyword_type "
                . "FROM brands_view "
                . "WHERE (b_name LIKE '%$keyword%' OR b_details LIKE '%$keyword%') LIMIT 3)"
                . "UNION ALL "
                . "(SELECT p_id as keyword_id, p_name as keyword, 'product' as keyword_type "
                . "FROM products_view "
                . "WHERE (p_name LIKE '%$keyword%' OR p_detail LIKE '%$keyword%') LIMIT 4) "
                
        );
        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    # function to list all the products from a keyword.

    public function listAllProductsFromKeyword($keyword_id, $keyword_type, $keyword = "") {

        #  get prouducts from product keyword
        $this->db->select(""
                . "p.p_id as product_id, "
                . "p.p_name as product_name, "
                . "p.p_original_price as product_original_price, "
                . "p.p_new_price as product_new_price, "
                . "p.p_detail as product_detail, "
                . "IF(b.b_name IS NULL, '', b.b_name)  as product_brand, "
                . "p_image as product_image, "
                . "p.p_size as product_sizes, "
                . "p.p_color as product_colors, "
                . "'$' as product_currency, "
                . "IF(s.s_name IS NULL, '', s.s_name) as store_name, "
                . "IF(s.s_id IS NULL, '', s.s_id) as store_id");
        $this->db->from('products p');
        $this->db->join('stores s', 's.s_id = p.p_store', 'LEFT');
        $this->db->join('brands b', 'b.b_id = p.p_brand', 'LEFT');
        
        # check for keyword type.
        if ($keyword_type == "product") {
            $this->db->where(array('p.p_id' => $keyword_id));
        } else if ($keyword_type == "store") {
            $this->db->where(array('p.p_store' => $keyword_id));
        } else if ($keyword_type == "brand") {
            $this->db->where(array('p.p_brand' => $keyword_id));
        } else {
            $this->db->where(array('p.p_status' => 1, 'p.p_archive' => 0));
        }
        $query = $this->db->get();

        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

}
