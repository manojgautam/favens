<?php

/*
 * Developed by: tecHindustan
 * Author: tecHindustan
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class StoreModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    # function to list all the products in a store.

    public function listStoreDetails($store_id) {
        $this->db->select(""
                . "p.p_id as product_id, "
                . "p.p_name as product_name, "
                . "p.p_original_price as product_original_price, "
                . "p.p_new_price as product_new_price, "
                . "p.p_detail as product_detail, "
                . "IF(b.b_name IS NULL, '', b.b_name)  as product_brand, "
                . "IF(c.c_name IS NULL, '', c.c_name)  as product_category, "
                . "p_image as product_image, "
                . "p.p_size as product_sizes, "
                . "p.p_color as product_colors, "
                . "'$' as product_currency, ");
        $this->db->from('products_view p');
        $this->db->join('p_s_categories ps', 'ps.p_id = p.p_id', 'LEFT');
        $this->db->join('brands b', 'b.b_id = p.p_brand', 'LEFT');
        $this->db->join('categories c', 'c.c_id = ps.cat_id', 'LEFT');
        $this->db->where(array('c.c_status' => 1, 'ps.s_id' => $store_id));
        $query = $this->db->get();

        # check if user exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

    # set store as favourite

    public function setStoreAsFavourite($data) {
        $this->db->insert('user_favourite_stores', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    
    # check if store is already in favorites
    
    public function checkStoreAsFavourite($data) {
        $this->db->select("id");
        $this->db->from('user_favourite_stores');
        $this->db->where($data);
        $query = $this->db->get();

        # check if user exists or not.
        if ($query->num_rows()) {
            return true;
        } else {
            return false;
        }
    }
    
    # remove store as favourite

    public function removeStoreAsFavourite($data) {
        $this->db->delete('user_favourite_stores', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    
    # get list of all fav stores of a user.
    
    public function getFavouriteStoresOfAUser($user_id) {
        $this->db->select(""
                . "s.s_id as store_id,"
                . "s.s_name as store_name");
        $this->db->from('user_favourite_stores f');
        $this->db->join('stores s', 's.s_id = f.s_id', 'LEFT');
        $this->db->where(array('f.u_id' => $user_id));
        $query = $this->db->get();

        # check if data exists or not.
        if ($query->num_rows()) {
            $result = $query->result_array();
            return $result;
        } else {
            return array();
        }
    }

}
