<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
		public function get_user_detail() {
		 // $email =   $this->session->userdata('adminsession')['email'];
        $this->db->select('*');
           $this->db->order_by('user_id','desc');
        $this->db->from('users');
        $Q = $this->db->get();
       return $Q->result();
    }
    
    public function update_status($id, $status){
        if($status==1){$newstatus=0;}
        else{$newstatus=1;}
        $data=array('is_active'=>$newstatus);
        $this->db->where('user_id', $id);
        $this->db->update('users', $data);
        return $newstatus;
    }

    public function delete_user($uid){
    $this->db->where('user_id', $uid);
        $this->db->delete('users');
  
  }
  
  	public function get_todayuser_detail() {
		 // $email =   $this->session->userdata('adminsession')['email'];
        $this->db->select('*');
        $this->db->from('users');
        $this->db->like('created_at', date('Y-m-d'));
        $Q = $this->db->get();
       return $Q->result();
    }
	

}
	