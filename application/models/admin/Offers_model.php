<?php
class Offers_model extends CI_Model {
    public function __construct() {
	  
    }
    
	public function add_offers($data){
  
            $return = $this->db->insert('offers', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}	
	
	public function get_all_offers() {
        $this->db->select('*');
        $this->db->order_by('o_id','desc');
        $this->db->from('offers');
        $this->db->where("o_archive",0);
        $this->db->where("o_date >=",date('Y-m-d'));
        $q=$this->db->get();
        return $q->result();
    }
	
	public function get_archive_offers() {
        $this->db->select('*');
        $this->db->order_by('o_id','desc');
        $this->db->from('offers');
        $this->db->where("o_archive",1);
		//$this->db->order_by("id", "DESC");
        $Q = $this->db->get();
        return $Q->result();
    }


    public function get_expire_offers() {
        $this->db->select('*');
        $this->db->order_by('o_id','desc');
        $this->db->from('offers');
        $this->db->where("o_date <",date('Y-m-d'));
        $this->db->where("o_archive",0);
        $Q = $this->db->get();
        return $Q->result();
    }
    
    public function archive_offers($o_id){
        
        $data=array('o_archive'=> 1);
        $this->db->where('o_id', $o_id);
        $this->db->update('offers', $data);
    }
    
    public function recover_offers($o_id){
        
        $data=array('o_archive'=> 0);
        $this->db->where('o_id', $o_id);
        $this->db->update('offers', $data);
    }
    
    	public function delete_offers($o_id){
		$this->db->where('o_id', $o_id);
		$this->db->delete('offers');
	}
	
	    public function update_status($id, $status){
        if($status==1){$newstatus=0;}
        else{$newstatus=1;}
        $data=array('o_status'=>$newstatus);
        $this->db->where('o_id', $id);
        $this->db->update('offers', $data);
        return $newstatus;
    }
    	
	public function get_edit_offers($o_id) {
     
        $this->db->select('*');
        $this->db->from('offers');
        $this->db->where("o_archive",0);
        $this->db->where('o_id', $o_id);
        $q=$this->db->get();
        $all= $q->result();
        return $all;
    }
    
    public function update_offer($o_id, $data){

          $this->db->where('o_id', $o_id);
         $this->db->update('offers', $data);
        
    }
	
	
	
}
?>
