<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
 public function notification_data($data){
        $this->db->insert('notification',$data);
        
    }
    
     public function get_notification_data(){
         $this->db->order_by('id','desc');
       $ndata =  $this->db->get('notification');
       
       
       $alldata=$ndata->result();
       
       foreach($alldata as $val){
          $val->created_at=$this->time_elapsed_string($val->created_at);
       }
       return $alldata;
     }
    
   public function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
 }
   
   public function delete_notifaction(){
       	$this->db->empty_table('notification');
	
   }
}
	