<?php
class Stores_model extends CI_Model {
    public function __construct() {
	  
    }
    
	public function add_stores($data){
  
            $return = $this->db->insert('stores', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}	
	

	public function get_all_stores() {
     
        $this->db->select('*');
        $this->db->order_by('s_id','desc');
        $this->db->from('stores');
        $this->db->where("s_archive",0);
        $q=$this->db->get();
        $all= $q->result();
        foreach($all as $val){
        $val->s_p_count=$this->getproductCount($val->s_id);
        }
        return $all;
    }
    
        public function getproductCount($sid){
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where("p_store",$sid);
	     $Q = $this->db->get();
         $res= $Q->result();

         $data=array('s_p_count'=>count($res));
        $this->db->where('s_id', $sid);
        $this->db->update('stores', $data);
        
        return count($res);
        
    }
	
	public function get_archive_stores() {
	    
        $this->db->select('*');
        $this->db->order_by('s_id','desc');
        $this->db->from('stores');
        $this->db->where("s_archive",1);
		//$this->db->order_by("id", "DESC");
        $Q = $this->db->get();
        $all= $Q->result();
        foreach($all as $val){
        $val->s_p_count=$this->getproductCount($val->s_id);
        }
        return $all;
    }
    
    public function archive_stores($s_id){
        
        $data=array('s_archive'=> 1);
        $this->db->where('s_id', $s_id);
        $this->db->update('stores', $data);
    }
    
    public function recover_stores($s_id){
        
        $data=array('s_archive'=> 0);
        $this->db->where('s_id', $s_id);
        $this->db->update('stores', $data);
    }
    
    	public function delete_stores($s_id){
               $image=$this->gestoreDetails($s_id);
               if(count($image)){
               if($image[0]->s_image!='')
               @unlink('uploads/'.$image[0]->s_image);}

		$this->db->where('s_id', $s_id);
		$this->db->delete('stores');
	}

public function gestoreDetails($sid){
$query = $this->db->get_where('stores', array('s_id' => $sid));
return $query->result();
}
	
	public function get_edit_stores($s_id) {
     
        $this->db->select('*');
        $this->db->from('stores');
        $this->db->where("s_archive",0);
        $this->db->where('s_id', $s_id);
        $q=$this->db->get();
        $all= $q->result();
        return $all;
    }
    
    public function update_store($s_id, $data){

          $this->db->where('s_id', $s_id);
         $this->db->update('stores', $data);
        
    }
	public function update_status($id, $status){
        if($status==1){$newstatus=0;}
        else{$newstatus=1;}
        $data=array('s_status'=>$newstatus);
        $this->db->where('s_id', $id);
        $this->db->update('stores', $data);
        return $newstatus;
    }
	
	   
	public function add_categories($data){
  
            $return = $this->db->insert('categories', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}
	
	public function get_all_categories($type=NULL,$id=NULL) {
     
        $this->db->select('*');
           $this->db->order_by('c_id','desc');
        $this->db->from('categories');
        $this->db->join('stores','categories.c_store = stores.s_id','left');
        
        if($type!=NULL && $type=="store")
        $this->db->where('stores.s_id',$id);
        
        $q=$this->db->get();
        $all= $q->result();
        return $all;
    }
    
    public function get_all_storecategories($s_id){
        
         $this->db->select('categories.c_name');
        $this->db->from('categories');
        
          $this->db->where('categories.c_store', $s_id);
          
         $q=$this->db->get();
        $all= $q->result();
        return $all;
        
    }
    
    public function delete_categories($c_id){
		$this->db->where('c_id', $c_id);
		$this->db->delete('categories');

                $this->db->where('cat_id', $c_id);
		$this->db->delete('p_s_categories');
	}
	
	public function update_status_cat($id, $status){
        if($status==1){$newstatus=0;}
        else{$newstatus=1;}
        $data=array('c_status'=>$newstatus);
        $this->db->where('c_id', $id);
        $this->db->update('categories', $data);
        return $newstatus;
    }
	
	
}
?>
