<?php
class Register_model extends CI_Model {
    public function __construct() {
	  
    }
    
	public function add_admin($data){
  
            $return = $this->db->insert('admin', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}	
	
	public function get_all_admin() {
        $this->db->select('*');
        $this->db->from('admin');
		$this->db->order_by("id", "DESC");
        $Q = $this->db->get();
        return $Q->result();
    }
    
    public function update_status($id, $status){
        if($status==1){$newstatus=0;}
        else{$newstatus=1;}
        $data=array('status'=>$newstatus);
        $this->db->where('id', $id);
        $this->db->update('admin', $data);
        return $newstatus;
    }

    public function deleteadmin($id){
        $this->db->where('id', $id);
        $this->db->delete('admin');
    }
	
}
?>
