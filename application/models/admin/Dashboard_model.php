<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	
	}
	
		public function get_admin_details($email) {
		 // $email =   $this->session->userdata('adminsession')['email'];
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email', $email);
			$this->db->limit(1);
        $Q = $this->db->get();
      // print_r($Q->result());
       return $Q->result();
    }
    
    public function get_today_product(){
         $this->db->select('*');
         $this->db->from('products');
         $this->db->like('created_at',date('Y-m-d'));
         $Q = $this->db->get();
       return $Q->result();
    }
     public function get_today_brand(){
         $this->db->select('*');
         $this->db->from('brands');
         $this->db->like('created_at',date('Y-m-d'));
         $Q = $this->db->get();
       return $Q->result();
    }
     public function get_today_store(){
         $this->db->select('*');
         $this->db->from('stores');
         $this->db->like('created_at',date('Y-m-d'));
         $Q = $this->db->get();
       return $Q->result();
    }
      public function get_today_user(){
         $this->db->select('*');
         $this->db->from('users');
         $this->db->like('created_at',date('Y-m-d'));
         $Q = $this->db->get();
       return $Q->result();
    }
    
   
   
}
	