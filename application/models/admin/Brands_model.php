<?php
class Brands_model extends CI_Model {
    public function __construct() {
	  
    }
    
	public function add_brands($data){
  
            $return = $this->db->insert('brands', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}	
	

	public function get_all_brands() {
     
        $this->db->select('*');
        $this->db->order_by('b_id','desc');
        $this->db->from('brands');
        $this->db->where("b_archive",0);
        $q=$this->db->get();
        $all= $q->result();
       foreach($all as $val){
        $val->b_p_count=$this->getproductCount($val->b_id);
        }
        return $all;
    }
    
    public function getproductCount($bid){
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where("p_brand",$bid);
	     $Q = $this->db->get();
         $res= $Q->result();

         $data=array('b_p_count'=>count($res));
        $this->db->where('b_id', $bid);
        $this->db->update('brands', $data);
        
        return count($res);
        
    }
	
	public function get_archive_brands() {
        $this->db->select('*');
        $this->db->order_by('b_id','desc');
        $this->db->from('brands');
        $this->db->where("b_archive",1);
		//$this->db->order_by("id", "DESC");
        $Q = $this->db->get();
        $all= $Q->result();
       foreach($all as $val){
        $val->b_p_count=$this->getproductCount($val->b_id);
        }
        return $all;
    }
    
    public function archive_brands($b_id){
        
        $data=array('b_archive'=> 1);
        $this->db->where('b_id', $b_id);
        $this->db->update('brands', $data);
    }
    
    public function recover_brands($b_id){
        
        $data=array('b_archive'=> 0);
        $this->db->where('b_id', $b_id);
        $this->db->update('brands', $data);
    }
    
    	public function delete_brands($b_id){
               $image=$this->getbrandDetails($b_id);
               if(count($image)){
               if($image[0]->b_logo!='')
               @unlink('uploads/'.$image[0]->b_logo);}

		$this->db->where('b_id', $b_id);
		$this->db->delete('brands');
	}

public function getbrandDetails($b_id){
$query = $this->db->get_where('brands', array('b_id' => $b_id));
return $query->result();
}
	
		public function get_edit_brands($b_id) {
     
        $this->db->select('*');
        $this->db->from('brands');
        $this->db->where("b_archive",0);
        $this->db->where('b_id', $b_id);
        $q=$this->db->get();
        $all= $q->result();
        return $all;
    }
    
        
    public function update_brand($b_id, $data){

          $this->db->where('b_id', $b_id);
         $this->db->update('brands', $data);
        
    }
    
     public function update_status($id, $status){
        if($status==1){$newstatus=0;}
        else{$newstatus=1;}
        $data=array('b_status'=>$newstatus);
        $this->db->where('b_id', $id);
        $this->db->update('brands', $data);
        return $newstatus;
    }
	
}
?>
