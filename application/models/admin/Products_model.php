<?php
class Products_model extends CI_Model {
    public function __construct() {
	  
    }
    
	public function add_products($data){
  
            $return = $this->db->insert('products', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}	
	
	public function add_products_image($data){
  
            $return = $this->db->insert('product_images', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}	

	public function update_products_image($data,$pid){
		 $this->db->where("p_id",$pid);
		 $this->db->where("p_image_type",1);
		 $this->db->update('product_images',$data);
	}

	public function update_products($data,$pid){
		 $this->db->where("p_id",$pid);
		 $this->db->update('products',$data);

	}

	public function get_all_products($type=NULL,$id=NULL) {
        $this->db->select('*');
		$this->db->order_by('products.p_id','desc');
        $this->db->from('products');
        $this->db->join('brands','products.p_brand = brands.b_id','left');
        $this->db->join('stores','products.p_store = stores.s_id','left');
        //$this->db->join('product_images','products.p_id = product_images.p_id');
       // $this->db->where('product_images.p_image_type',1);
        if($type!=NULL && $type=="brand")
        $this->db->where('brands.b_id',$id);
        if($type!=NULL && $type=="store")
        $this->db->where('stores.s_id',$id);
        if($type!=NULL && $type=="today")
        $this->db->like('products.created_at', date('Y-m-d'));
        if($type==NULL)
        $this->db->where("p_archive",0);
        $q=$this->db->get();
        return $q->result();
    }


	public function get_single_product($pid) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->join('brands','products.p_brand = brands.b_id','left');
        $this->db->join('stores','products.p_store = stores.s_id','left');
        //$this->db->join('product_images','products.p_id = product_images.p_id');
        $this->db->where("p_archive",0);
        $this->db->where("products.p_id",$pid);
        $q=$this->db->get();
        return $q->result();
    }



    public function get_pimages($pid,$type) {
        $this->db->select('*');
        $this->db->from('product_images');
        $this->db->where('p_id',$pid);
        $this->db->where("p_image_type",$type);
        $Q = $this->db->get();
        return $Q->result();
    }

    public function delete_pimages($imgid){

               $image=$this->getproductimageDetails($imgid);
               if(count($image)){
               if($image[0]->p_image_name!='')
               @unlink('uploads/'.$image[0]->p_image_name);}

               $this->db->where("image_id",$imgid);
	       $this->db->delete("product_images");

	}

public function getproductimageDetails($imgid){
$query = $this->db->get_where('product_images', array('image_id' => $imgid));
return $query->result();
}
	
	public function get_archive_products() {
        $this->db->select('*');
		$this->db->order_by('products.p_id','desc');
        $this->db->from('products');
        $this->db->join('brands','products.p_brand = brands.b_id','left');
        $this->db->join('stores','products.p_store = stores.s_id','left');
        //$this->db->join('product_images','products.p_id = product_images.p_id');
       // $this->db->where('product_images.p_image_type',1);
        $this->db->where("p_archive",1);
		//$this->db->order_by("id", "DESC");
        $Q = $this->db->get();
        return $Q->result();
    }
    
    public function archive_products($p_id){
        
        $data=array('p_archive'=> 1);
        $this->db->where('p_id', $p_id);
        $this->db->update('products', $data);
    }
    
    public function recover_products($p_id){
        
        $data=array('p_archive'=> 0);
        $this->db->where('p_id', $p_id);
        $this->db->update('products', $data);
  
    }
    
    public function delete_products($p_id){
               $images=$this->getproductimagesbyProductId($p_id);
               foreach($images as $image)
               {if($image->p_image_name!='')
               @unlink('uploads/'.$image->p_image_name);}

               $this->db->where("p_id",$p_id);
	       $q=$this->db->get("products");
               $res=$q->result();
               if(count($res)){
               if($res[0]->p_image!='')
               @unlink('uploads/'.$image->p_image);}

               $this->db->where('p_id',$p_id);
               $this->db->delete('p_s_categories');

		$this->db->where('p_id', $p_id);
		$this->db->delete('products');
	}

public function getproductimagesbyProductId($p_id){
$query = $this->db->get_where('product_images', array('p_id' => $p_id));
return $query->result();
}
	
	public function update_status($id, $status){
        if($status==1){$newstatus=0;}
        else{$newstatus=1;}
        $data=array('p_status'=>$newstatus);
        $this->db->where('p_id', $id);
        $this->db->update('products', $data);
        return $newstatus;
    }
    
    
    public function get_all_categ($s_id) {
          $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('c_store',$s_id);
        $this->db->or_where('c_store',0);
        $this->db->where("c_status",1);
        $q=$this->db->get();
        return $q->result();
    }
    
    public function get_all_productcategories($p_id){
        
         $this->db->select('categories.c_name');
        $this->db->from('products');
        $this->db->join('p_s_categories','p_s_categories.p_id = products.p_id');
         $this->db->join('categories','categories.c_id = p_s_categories.cat_id');
         
        
          $this->db->where('products.p_id', $p_id);
          
         $q=$this->db->get();
        $all= $q->result();
        return $all;
        
    }

    public function add_products_categories($data){
		$this->db->insert('p_s_categories',$data);
    }

    public function get_p_s_categories($pid){
        $this->db->select('*');
        $this->db->from('p_s_categories');
        $this->db->where('p_id',$pid);
        $q=$this->db->get();
        return $q->result();}

    public function delete_products_categories($pid){
		$this->db->where('p_id', $pid);
        $this->db->delete('p_s_categories');
  
    }
    
    public function add_products_date($data){
         $return = $this->db->insert('product_offer_limit', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }    
    }
    
    public function get_expire_product_offer($pid){
        $this->db->select('*');
        $this->db->from('product_offer_limit');
        $this->db->where('product_id',$pid);
         $this->db->where('p_end_date<',date('Y-m-d H:i:s'));
         $q=$this->db->get();
        return $q->result();
    }
	
	 public function get_active_product_offer($pid){
        $this->db->select('*');
        $this->db->from('product_offer_limit');
        $this->db->where('product_id',$pid);
         $this->db->where('p_end_date>=',date('Y-m-d H:i:s'));
         $q=$this->db->get();
        return $q->result();
    }
    public function update_products_date($data, $pid){
        	 $this->db->where("product_id",$pid);
		 $this->db->update('product_offer_limit',$data);
    }

    public function get_all_currency(){
     $q=$this->db->get('currency');
     return $q->result();
    }
	
	
	
}
?>
