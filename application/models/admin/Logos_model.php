<?php
class Logos_model extends CI_Model {
    public function __construct() {
	  
    }
    
	public function add_logos($data){
  
            $return = $this->db->insert('logos', $data);
            if ((bool) $return === TRUE) {
                return $this->db->insert_id();
            } else {
                return $return;
            }       
			
	}	
	

	public function get_all_logos() {
     
        $this->db->select('*');
        $this->db->order_by('l_id','desc');
        $this->db->from('logos');
        $q=$this->db->get();
        return $q->result();
    }
	
    
    public function delete_logos($l_id){

               $image=$this->getlogoDetails($l_id);
               if($image[0]->l_logo!='')
               @unlink('uploads/'.$image[0]->l_logo);

		$this->db->where('l_id', $l_id);
		$this->db->delete('logos');
	}

public function getlogoDetails($l_id){
$query = $this->db->get_where('logos', array('l_id' => $l_id));
return $query->result();
}
	

}
?>
