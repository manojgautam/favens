      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
			<div class="container-fluid container-fixed-lg">
				<div class="row">
				  <div class="col-lg-12 col-md-12">
                         <!-- START PANEL -->
                          <div class="panel panel-transparent">
                <!-- <div class="panel-heading">
                    <div class="panel-title">Add Offers
                    </div>
                    
                    <div class="clearfix"></div>
                </div> -->
				
                <?php if($this->session->flashdata('success')==true): ?>
				<div class="message_block">
                <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div></div>
        <?php endif;?>
		
                 <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" method="post" action="<?php echo base_url('admin/offers'); ?>">
					
					<div class="row">
                        <div class="col-sm-6">
                          <div class="form-group form-group-default">
                            <label>Offer Title</label>
                            <input type="text" class="form-control" name="otitle" placeholder="Enter Offer Title" required>
                            
                          </div>
                        </div>
                        <div class="col-sm-6 no-padding">
                         <div class="form-group form-group-default">
                            <label>Offer Date</label>
                            
                    <div id="datepicker-component" class="input-group date col-sm-8">
                     <input type="text"  name="date" class="form-control" placeholder="mm/dd/yyyy" required><span class="input-group-addon" style="background:transparent;border:none;"></span>
                        </div>
                          </div>
                        </div>
						 <div class="row">
						 <div class="col-sm-12">
                           <div class="form-group form-group-default">
                            <label>Offer Description</label>
                            <textarea class="form-control" name="odetail" placeholder="Description" required></textarea>
                          </div>
                        </div>
                        </div>

                      </div>
                     
                     
					  
					
                      <div class="clearfix"></div>
						 <div class="add_btn">
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Offer" >
					  </div>
                    </form>
                </div>
                </div>
                <!-- END PANEL -->
			</div>
			</div>
			</div>
             
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
