
<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
<!-- START PAGE CONTENT -->
<div class="content ">
 <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="p-t-10 panel-title">Users Details
                </div>
								
				<div class="pull-right">    
				<div class="col-xs-12">       
				<input type="text" id="search-table" class="form-control pull-right" placeholder="Search">               
				</div>              
				</div>
                <div class="clearfix"></div>
              </div>
              <form method='post' action="<?php echo base_url();?>admin/users" id="userform"><input type="submit" value='Delete' data-text="You will not be able to recover these records" class="del_event btn btn-danger submitallids"  style='display:none' name='bulk_delt'>
              <input type="hidden" name="deleteallhidden" id="deleteallhidden" value="0">
              <input type="hidden" name="deleteallids" id="deleteallids">
               <input type="hidden" name="submitdone" id="submitdone" value="submitdone">
              <div class="panel-body">   
			  <table class="table table-hover demo-table-search table-responsive-block tableclass" id="tableWithSearch">
			  
                  <thead>        
				  <tr> 
                                  <th>
<?php if(count($userdetail)){ ?>
  <div class="checkbox check-success">                 
        <input type="checkbox" name="deleteall[]" class="checkAll"  id="checkbox0"> 
                     <label for="checkbox0"></label>
                   </div>
<?php } ?>
</th>           
				  <th>Email</th>   
				  <th>Date of Registeration</th>  
				  <th>Last Login</th>
				  <th>Status</th>
				  <th>Action</th>
				  </tr>         
				  </thead>
                   <tbody>		
				   
                      <?php foreach($userdetail as $row):?>
                   <tr>    
                       <td>
<div class="checkbox check-success">                 
        <input type='checkbox' class='checkdata' value="<?php echo $row->user_id;?>" name='checked_events[]' id="checkbox<?php echo $row->user_id;?>"> 
                     <label for="checkbox<?php echo $row->user_id;?>"></label>
                   </div>
</td>                  
				   <td class="v-align-middle semi-bold"> 
				   <?php echo $row->u_email; ?>
				   </td>                     
				   <td class="v-align-middle"> 
				   <?php echo date_format(date_create($row->created_at),'d M  Y'); ?>
				   </td>                    
				   <td class="v-align-middle"> 
				   </td>    
				   <td class="v-align-middle"> 
				    <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
					 <a class="updatestatususer" theid="<?php echo $row->user_id ; ?>" thestatus="<?php echo $row->is_active ; ?>"  href="javascript:;" id="statuser<?php echo $row->user_id ;?>">
					     <div class="statuschangeuser<?php echo $row->user_id; ?>">
					 <?php if($row->is_active == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
					 </div>
					 </a>
				   </td>  
				   <td class="v-align-middle">
				   			   
				   <a data-url="<?php echo base_url('admin/users/deleteuser/'.$row->user_id);?>" href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" data-original-title="Delete Admin" class="btn btn-danger m-b-10 deleterecord"><i class="fa fa-trash-o"></i> </a>
				  
				 </td>     
				   </tr>	
				    <?php endforeach; ?>
				  </tbody>
                </table>		
				</div>	
                                </form>		
				</div>
              </div>			  			  
            </div>
            <!-- END PANEL -->
          </div>
         
</div>
<!-- START PAGE CONTENT -->
</div>
  <!-- START PAGE CONTENT WRAPPER -->         
