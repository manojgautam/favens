<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
<!-- START PAGE CONTENT -->
<div class="content ">
 <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
              <div class="panel-heading">
                <div class="p-t-10 panel-title">Admin Details
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                   
                    <a id="btnToggleSlideUpSize" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Admin</a>
                  </div>
                </div>								
				<div class="pull-right">    
				<div class="col-xs-12">       
				<input type="text" id="search-table" class="form-control pull-right" placeholder="Search">               
				</div>              
				</div>
                <div class="clearfix"></div>
              </div>
              <div class="panel-body">   
			  <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
			  
                  <thead>        
				  <tr>            
				  <th>Name</th>   
				  <th>Email</th>  
				  <th>Image</th>
				  <th>Status</th>				  
				  <th>Action</th> 
				  </tr>         
				  </thead>
                   <tbody>	
                    <?php $email =   $this->session->userdata('adminsession')['email']; ?>
				   <?php foreach($list as $row):?> 
                   <tr>                      
				   <td class="v-align-middle semi-bold"> 
				   <p><?php echo $row->name;?></p> 
				   </td>                     
				   <td class="v-align-middle">
				   <?php echo $row->email;?>    
				   </td>                    
				   <td class="v-align-middle"> 
				   <img src="<?php if($row->image == "") echo base_url('uploads/default.png'); else echo  base_url('uploads/'.$row->image);?>" height="50px" width="50px">                    
				   </td>           
				     <td class="v-align-middle">
				         <?php if($row->role == "1"){ ?>
				         <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
					 <a class="updatestatus" theid="<?php echo $row->id ; ?>" thestatus="<?php echo $row->status ; ?>"  href="javascript:;" id="stats<?php echo $row->id ;?>">
					     <div class="statuschange<?php echo $row->id; ?>">
					 <?php if($row->status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
					 </div>
					 </a>
					 <?php } else { } ?>
					
					 </td>
				   <td class="v-align-middle">
				                
				   <!-- Indicates a dangerous or potentially negative action -->  
                   <?php if($row->role == "1" && $row->email != $email) {?>				   
				   <a data-url="<?php echo base_url('admin/register/deleteadmin/'.$row->id);?>" href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" data-original-title="Delete Admin" class="btn btn-danger m-b-10 deleterecord"><i class="fa fa-trash-o"></i> </a>
				   <?php } else { } ?>            
				   </td>                
				   </tr>				
				   <?php endforeach; ?> 
				   </tbody>
                </table>		
				</div>			
				</div>
              </div>			  			    
            </div>
            <!-- END PANEL -->
          </div>
         
</div>
<!-- START PAGE CONTENT -->
</div>
  <!-- START PAGE CONTENT WRAPPER -->         
