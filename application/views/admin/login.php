        <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="<?php echo base_url('assets/img/demo/login.jpg')?>" data-src="<?php echo base_url('assets/img/demo/login.jpg')?>" data-src-retina="<?php echo base_url('assets/img/demo/login.jpg')?>" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
		   <img src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" alt="logo" data-src="<?php echo base_url('assets/img/favens_logo_white.png')?>" data-src-retina="<?php echo base_url('assets/img/logo_2x.png')?>" width="150" >
          <p class="small">
           copyright &copy; 2017 FAVENS.
          </p>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-100 sm-p-l-15 sm-p-r-15 sm-p-t-40">
		<img src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" alt="logo" data-src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" data-src-retina="<?php echo base_url('assets/img/logo_2x.png')?>" width="150" >
          <p class="p-t-35">Sign In as Favens account</p>
          
                     <?php if($this->session->flashdata('error')==true): ?>
                       <h5 style="color:red;"><?php echo $this->session->flashdata('error') ;?></h5>
                      <?php endif;?>
          
          <!-- START Login Form -->
          <form id="form-login" method="post" class="p-t-15" role="form" action="<?php echo base_url('admin/login')?>">
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>Email</label>
              <div class="controls">
                <input type="email" name="email" placeholder="Enter Your Email" class="form-control" required>
              </div>
            </div>
            <!-- END Form Control-->
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>Password</label>
              <div class="controls">
                <input type="password" class="form-control" name="password" placeholder="Enter Your Password" required>
              </div>
            </div>
            <!-- START Form Control-->
            <div class="row">
              <div class="col-md-6 no-padding">
                <div class="checkbox ">
                  <input type="checkbox" value="1" id="checkbox1">
                  <label for="checkbox1">Keep Me Signed in</label>
                </div>
              </div>
              <div class="col-md-6 text-right">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#forget" id="forgot-pass" class="text-info small">Forgot Password?</a>
              </div>
            </div>
            <!-- END Form Control-->
            
            <input type="submit" name="submitted" class="btn btn-primary btn-cons m-t-10" value="Sign in">
          </form>
          <!--END Login Form-->

        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    			<!-- Modal -->
<div id="forget" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forgot Password</h4>
      </div>
      <div class="modal-body">
 <form id="form-personal" role="form"  autocomplete="off" method="post" action="<?php echo base_url(); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" id="forgetemail" placeholder="Enter Your Mail" required>
                          </div>
<p id="err-forgot" style="color: #f55753;font-size: 12px;"></p>
 <p id="success_message" style="color:green;font-size:12px;"></p> 
                        </div>
                      </div>
                
       
                      <div class="clearfix"></div>
                     
                      <input type="submit" name="submit" class="btn btn-primary" id="forget_submit" value="Get Password" >
                    </form>
      </div>
         </div>

  </div>
</div>
            <!-- /.modal-dialog -->
