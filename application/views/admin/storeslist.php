<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Stores Details
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">

                            <a href="<?php echo base_url('admin/stores/'); ?>" id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Store</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>Store Name</th>
                                <th>Store Image</th>
                                <th>Products Count</th>
								<th>Store Status</th>
                                <th>Added By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($storeslist as $row):?>
                                <tr>
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->s_name;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle"> <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpimg-<?php echo $row->s_id;?>" >
                                        <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url('/uploads/'.$row->s_image);?>&h=50&w=50"></a> </td>
                                    <td class="v-align-middle semi-bold">
                                        
                                            <a href="<?php echo base_url('admin/products/productslist/store/'.$row->s_id); ?>"><span class="label label-success"> <?php echo $row->s_p_count;?></span></a>
                                        
                                    </td>
								  <td class="v-align-middle">
				                <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
				     	       <a class="updatestatusstore" theid="<?php echo $row->s_id ; ?>" thestatus="<?php echo $row->s_status ; ?>"  href="javascript:;" id="statsstore<?php echo $row->s_id ;?>">
					           <div class="statuschangestore<?php echo $row->s_id; ?>">
				         	  <?php if($row->s_status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
				            	 </div>
			      	        	 </a>
				               	 </td>
									 <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->s_added_by;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                        <span data-toggle="modal" data-target="#modalSlideUpct-<?php echo $row->s_id;?>">
                                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="Manage Category"   class="btn btn-info  m-b-10"><i class="fa fa-paste"></i> </a>
                                       </span>
                                       <span data-toggle="modal" data-target="#modalSlideUps-<?php echo $row->s_id;?>">
                                         <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="View Store"  class="btn btn-primary m-b-10"><i class="fa fa-eye"></i> </a>
                                       </span>
                                        <span class="action">
                                         <a href="<?php echo base_url('admin/stores/editstore/'.$row->s_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit Store" class="btn btn-info  m-b-10"><i class="fa fa-edit"></i> </a>
                                         </span>
                                        <!-- Indicates a dangerous or potentially negative action -->
                                       <span class="action">
                                         <a href="<?php echo base_url('admin/stores/archivestores/'.$row->s_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Archive Store" class="btn btn-danger  m-b-10"><i class="fa fa-warning"></i></a>
                                       </span>
                                    </td>
                                </tr>
                                
                                <!--- Store Modal Detail-->       
          <div class="modal fade slide-up disable-scroll" id="modalSlideUps-<?php echo $row->s_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Store <span class="semi-bold">Detail</span></h5>
                    
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">
					     
					 <div class="col-sm-6">
					  <div class="form-group form-group-default">
						<label>Store Image</label>
						<img src="<?php echo base_url('/uploads/'.$row->s_image);?>" height="200" width="200">
					  </div>
					</div>
					
					  <div class="col-sm-6">
					  <div class="form-group form-group-default">
						<label>Store Name</label>
						<p><?php echo $row->s_name;?></p>
					  </div>
					
					  <div class="form-group form-group-default">
						<label>Store Location</label>
						<p><?php echo $row->s_location;?></p>
					  </div>
					  <?php if(count($row->storecategories)){ ?>
					   <div class="form-group form-group-default">
						<label>Store categories</label>
						<p><?php foreach($row->storecategories as $rows ){
						    echo "<span class='btn btn-success m-t-5 m-r-5'>".$rows->c_name."</span>";
						}
						?></p>
						  </div>
					  	<?php	} ?>
					 </div>
					

					</div>

				   </div>
  
                  </div>
               
              </div></div></div></div>
              <!-- /.modal-content -->
			  
			        <!--- Store Modal Image-->       
          <div class="modal img-popup fade slide-up disable-scroll" id="modalSlideUpimg-<?php echo $row->s_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">					     					
					  <div class="form-group form-group-default">						
						<img src="<?php echo base_url('/uploads/'.$row->s_image);?>" >
					  </div>	
					</div>
				   </div>
                  </div>
              </div>
              <!-- /.modal-content -->
			  
            </div>
          </div>
		  </div>
 <!--- End Store Modal Image--> 
 
 
 
   <!--- Add manage categories Modal -->       
              
    <div id="modalSlideUpct-<?php echo $row->s_id;?>" class="modal fade" role="dialog">
            <div class="modal-dialog ">
                
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add Category For <span class="semi-bold"><?php echo $row->s_name;?></span></h5>
                  </div>
                         <div class="modal-body">
					         <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/stores/managecategories'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Category Name</label>
                            <input type="text" class="form-control" name="cname" placeholder="Enter Category Name " required>
                            <input type="hidden" name="storeid" id="storeid" value="<?php echo $row->s_id;?>">
                          </div>
                        </div>
                      </div>
                                            

                      <div class="clearfix"></div>
                     
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Category" >
                    </form>
                  </div>
              </div>
              <!-- /.modal-content -->
			  
          </div>
		  </div>
 <!--- End manage categories Modal --> 


			      
                                <?php endforeach; ?>
                                   
                        </tbody>
                    </table>
                </div>
            </div>
       
  
    <!-- END PANEL -->
</div>

</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->
