<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
<!-- START PAGE CONTENT -->
<div class="content ">
<div class="container-fluid container-fixed-lg">
<!-- START PANEL -->
<div class="panel panel-transparent">
<div class="panel-heading">
<div class="panel-title">Notification </div>
	<div class="pull-right">	<div class="col-xs-12">
	<a data-text="You will not be able to recover this record"  data-url="<?php echo base_url('admin/notification/notificationdelete');?>" href="javascript:void(0);" class="btn btn-danger btn-cons m-b-10 deleterecord"><i class="fa fa-trash-o"></i> Clear All </a>	</div>	</div>
       
<div class="pull-right">
	<div class="col-xs-12">
		<input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
</div>
<div class="clearfix"></div>
</div>
<div class="panel-body">
<table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
	<thead>
		<tr>
			
			<th>Type</th>			<th>Name</th>
			<th>Added By</th>
			<th>Time</th>
		</tr>
	</thead>
	<tbody>
<?php if(isset($notificationdata)){ foreach($notificationdata as $notify): ?>
			<tr>
							<td>				<?php if ($notify->n_type == 1){ echo "Product"; }  if ($notify->n_type == 2){ echo "Brand"; } if ($notify->n_type == 3){ echo "Store"; }?> 				</td>
				<td class="v-align-middle semi-bold">
					<p>
						<span class="bold"><?php echo $notify->n_name; ?></span> 
						
					</p>
				</td>
				<td class="v-align-middle semi-bold">
					<p>
					<?php echo $notify->n_added_by; ?>
						</p>
				</td>
				<td class="v-align-middle semi-bold">
					<p>
				<?php echo $notify->created_at; ?>
						</p>
				</td>
				
							
			</tr>
			
<?php endforeach; } ?>
	</tbody>
</table>
</div>
</div>
</div>
</div>
<!-- END PANEL -->
</div>
</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->
