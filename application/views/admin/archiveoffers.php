<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Archive Offers
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">

                            <a href="<?php echo base_url('admin/offers/'); ?>" id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Brand</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <form method='post' action="<?php echo base_url();?>admin/offers/archiveofferslist" id="userform"><input type="submit" value='Delete' data-text="You will not be able to recover these records" class="del_event btn btn-danger submitallids"  style='display:none' name='bulk_delt'>
              <input type="hidden" name="deleteallhidden" id="deleteallhidden" value="0">
              <input type="hidden" name="deleteallids" id="deleteallids">
               <input type="hidden" name="submitdone" id="submitdone" value="submitdone">
                <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block tableclass" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>
                                 <div class="checkbox check-success">                 
                                 <input type="checkbox" name="deleteall[]" class="checkAll"  id="checkbox0"> 
                                 <label for="checkbox0"></label>
                                 </div>
                                </th>
                                <th>Offer Title</th>
                                <th>Offer Detail</th>
								<th>Offer Date</th>
                                <th>Offer Status</th>
                                <th>Offer Added By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($archiveofferslist as $row):?>
                                <tr>
                                                                          <td>
                                    <div class="checkbox check-success">                 
                                    <input type='checkbox' class='checkdata' value="<?php echo $row->o_id;?>" name='checked_events[]' id="checkbox<?php echo $row->o_id;?>"> 
                                    <label for="checkbox<?php echo $row->o_id;?>"></label>
                                     </div>
                                   </td>
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->o_title;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                       <p> <?php echo $row->o_detail;?></p>
                                            
                                    </td>
                                    <td class="v-align-middle"> 
									<p><?php echo $row->o_date;?></p>
									</td>
                                   		 <td class="v-align-middle">
				           <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
				     	 <a class="updatestatusoffer" theid="<?php echo $row->o_id ; ?>" thestatus="<?php echo $row->o_status ; ?>"  href="javascript:;" id="statsoffer<?php echo $row->o_id ;?>">
					     <div class="statuschangeoffer<?php echo $row->o_id; ?>">
				     	 <?php if($row->o_status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
				    	 </div>
			      	   	 </a>
				               	 </td>
									 <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->o_added_by;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                        <span data-toggle="modal" data-target="#modalSlideUpp-<?php  echo $row->o_id;?>">
                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="View Offer" class="btn btn-primary  m-b-10"><i class="fa fa-eye"></i> </a>
                                       </span>
                                        <a href="<?php echo base_url('admin/offers/recoveroffers/'.$row->o_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Recover Offer" class="btn btn-info  m-b-10"><i class="fa fa-paste"></i> </a>
                                    
                                        <!-- Indicates a dangerous or potentially negative action -->
                                 <a data-url="<?php echo base_url('admin/offers/deleteoffers/'.$row->o_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Delete Offer" href="javascript:void(0);" class="btn btn-danger m-b-10 deleterecord"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                
                                	<!--- Single Product Modal Detail-->
			<div class="modal fade slide-up disable-scroll" id="modalSlideUpp-<?php echo $row->o_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog view_product offer_lst">
					<div class="modal-content-wrapper">
						<div class="modal-content">
							<div class="modal-header clearfix text-left">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
								<h5>Offer <span class="semi-bold">Detail</span></h5>
							</div>
							<div class="modal-body">
								<div class="form-group-attached single-product-detail">
									<div class="row dialog__overview">
									<div class="col-sm-6">
									<div class="form-group form-group-default">
												<label>Offer Title</label>
													<p><?php echo $row->o_title;?></p>
												</div>		
												</div>																			<div class="col-sm-6">												<div class="form-group form-group-default">
												<label>Offer Date</label>
												<p>
													<?php echo $row->o_date;?>
												</p>			
												</div>	
												</div>	
												<div class="col-sm-12">	<div class="form-group form-group-default">
												<label>Offer Deiscription</label>
												<p>
												   <?php echo $row->o_detail;?>
												</p></div>	</div>										
								
									</div>
								</div>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				</div>
			</div>
			<!--- End Single product Modal Detail-->
                                <?php endforeach;?>
                                    
                        </tbody>
                    </table>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END PANEL -->
</div>

</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->
