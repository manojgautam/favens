<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="relative">
			  <div class="gallery-filters p-t-20 p-b-10">
                    <ul class="list-inline text-right">
                        <li><button type="button" class="btn btn-primary btn-cons" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add New Logos</button></li>
                    </ul>
                </div>
            <div class="gallery">
			<div class="clearfix"></div>
                <?php  foreach($logoslist as $row):?>
               
             <div class="gallery-item first" data-width="1" data-height="1">
                        <!-- START PREVIEW -->
                        <img class="image-responsive-height" src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url('/uploads/'.$row->l_logo);?>&h=300&w=550">
			
                        <!-- END PREVIEW -->
                        <!-- START ITEM OVERLAY DESCRIPTION -->
                        <div class="overlayer bottom-left full-width">
                            <div class="overlayer-wrapper item-info ">
                                <div class="gradient-grey p-l-20 p-r-20 p-t-20 p-b-5">
                                    <div class="m-t-50">
                                        <div class="inline m-l-10">
                                            <p class="pull-left bold text-white fs-14 p-t-15">
                                                <?php echo $row->l_name;?>
                                            </p>
                                            
                                        </div>
                                        <div class="pull-right m-t-10">
                                             <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpimg-<?php echo $row->l_id;?>" class="btn btn-white btn-xs btn-mini bold fs-14 "><i class="fa fa-eye"></i></a>
                                        
                                            <a data-url="<?php echo base_url('admin/logos/deletelogos/'.$row->l_id);?>" href="javascript:void(0);" class="btn btn-white btn-xs btn-mini bold fs-14 deleterecord">X</a>
                                  
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PRODUCT OVERLAY DESCRIPTION -->
                    </div>
                    
                    			
			 <!--- Logo single Modal Image-->       
          <div class="modal img-popup  fade slide-up disable-scroll" id="modalSlideUpimg-<?php echo $row->l_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">					     					
					  <div class="form-group form-group-default">						
						<img src="<?php echo base_url('/uploads/'.$row->l_logo);?>" >
						
					  </div>	
					</div>
				   </div>
                  </div>
              </div>
              <!-- /.modal-content -->
			  
            </div>
          </div>
		  </div>
 <!--- End Logo Single Modal Image--> 
                    <?php endforeach; ?>

            <?php if(count($logoslist)){}else{ ?>
           <h3 class="center nologo">No Logo Found</h3>
            <?php } ?>

            </div>
            </div>
			<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Logos</h4>
      </div>
      <div class="modal-body">
       <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/logos/logoslist'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Logo Name</label>
                            <input type="text" class="form-control" name="lname" placeholder="Enter Logo Name" required>
                            
                          </div>
                        </div>
                      </div>
                                            

                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Logo</label>
                            <input type="file" class="form-control" name="userfile" id="img-prev" required>
                            <div class="imageprview"></div>
                            
                          </div> 
                        </div>
                      </div>
       
                      <div class="clearfix"></div>
                     
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Logo" >
                    </form>
      </div>
         </div>

  </div>
</div>
            <!-- /.modal-dialog -->
        </div>
    </div>
</div>
</div>
<!-- END PANEL -->
</div>
</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->
