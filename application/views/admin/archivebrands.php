<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Archive Brands Details
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">

                            <a href="<?php echo base_url('admin/brands/'); ?>" id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Brand</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!--Start Delete all-->
                <form method='post' action="<?php echo base_url();?>admin/brands/archivebrandslist" id="userform"><input type="submit" value='Delete' data-text="You will not be able to recover these records" class="del_event btn btn-danger submitallids"  style='display:none' name='bulk_delt'>
              <input type="hidden" name="deleteallhidden" id="deleteallhidden" value="0">
              <input type="hidden" name="deleteallids" id="deleteallids">
               <input type="hidden" name="submitdone" id="submitdone" value="submitdone">
              <div class="panel-body"> 
                <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block tableclass" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>
<?php if(count($archivebrandslist)){?>
                                 <div class="checkbox check-success">                 
                                 <input type="checkbox" name="deleteall[]" class="checkAll"  id="checkbox0"> 
                                 <label for="checkbox0"></label>
                                 </div>
<?php } ?>
                                </th>           
                                <th>Brand Name</th>
                                <th>Logo</th>
                                <th>Products Count</th>
								<th>Brand Status</th>
								<th>Added By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach($archivebrandslist as $row):?>
                                <tr>
                                  <td>
                                    <div class="checkbox check-success">                 
                                    <input type='checkbox' class='checkdata' value="<?php echo $row->b_id;?>" name='checked_events[]' id="checkbox<?php echo $row->b_id;?>"> 
                                    <label for="checkbox<?php echo $row->b_id;?>"></label>
                                     </div>
                                   </td>                  
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->b_name;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle"> <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpimg-<?php echo $row->b_id;?>" ><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url('/uploads/'.$row->b_logo);?>&h=50&w=50"></a> </td>
                                    <td class="v-align-middle semi-bold">
                                        <span class="label label-success"> 
                                            <a href="<?php echo base_url('admin/products/productslist/'.$row->b_id); ?>"><span class="label label-success"> <?php echo $row->b_p_count;?></span></a>
                                        </span>
                                    </td>
									  <td class="v-align-middle semi-bold">
                                       <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
				     	 <a class="updatestatusbrand" theid="<?php echo $row->b_id ; ?>" thestatus="<?php echo $row->b_status ; ?>"  href="javascript:;" id="statsbrand<?php echo $row->b_id ;?>">
					     <div class="statuschangebrand<?php echo $row->b_id; ?>">
				     	 <?php if($row->b_status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
				    	 </div>
			      	   	 </a>
                                    </td>
									  <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->b_added_by;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                        <span data-toggle="modal" data-target="#modalSlideUpb-<?php echo $row->b_id;?>">
									    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="View Brand"  class="btn btn-primary m-b-10"><i class="fa fa-eye"></i></a>
									</span>
                                        <a href="<?php echo base_url('admin/brands/recoverbrands/'.$row->b_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Recover Brand" class="btn btn-info  m-b-10"><i class="fa fa-paste"></i></a>
                                    
                                        <!-- Indicates a dangerous or potentially negative action -->
                                        <a data-url="<?php echo base_url('admin/brands/deletebrands/'.$row->b_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Delete Brand" data-text="There are some products associated with this Brand "  href="javascript:void(0);" class="btn btn-danger  m-b-10 deleterecord"><i class="fa fa-trash-o"></i></a>
                                   </td>
                                </tr>
								
								
                                  <!--- Brand Modal Detail-->                  
   <div class="modal fade slide-up disable-scroll" id="modalSlideUpb-<?php echo $row->b_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Brand <span class="semi-bold">Detail</span></h5>
                    
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">
					  <div class="col-sm-6">
					  <div class="form-group form-group-default">
						<label>Brand Logo</label>
						<img src="<?php echo base_url('/uploads/'.$row->b_logo);?>" height="200" width="200">
					  </div>
					  </div>
					  <div class="col-sm-6">
					  <div class="form-group form-group-default">
						<label>Brand Name</label>
						<p><?php echo $row->b_name;?></p>
					  </div>
					
					  <div class="form-group form-group-default">
						<label>Brand Detail</label>
						<p><?php echo $row->b_details;?></p>
					  </div>
					 </div>
					

					</div>

				   </div>
  
                  </div>
               
              </div>
              <!-- /.modal-content -->
            </div>
          </div>
		  </div>
<!--- End Brand Modal Detail-->       

			        <!--- Brand Modal Image-->       
          <div class="modal img-popup fade slide-up disable-scroll" id="modalSlideUpimg-<?php echo $row->b_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">					     					
					  <div class="form-group form-group-default">						
						<img src="<?php echo base_url('/uploads/'.$row->b_logo);?>" >
					  </div>	
					</div>
				   </div>
                  </div>
              </div>
              <!-- /.modal-content -->
			  
            </div>
          </div>
		  </div>
 <!--- End Brand Modal Image--> 
								
								
                                <?php endforeach; ?>
                                    
                        </tbody>
                    </table>
                    
            </div>
            </form>
          </div>
          
                </div>
            </div>
        </div>
    </div>
    <!-- END PANEL -->
    
    
</div>

</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->
<script type="text/javascript">
    function confirmDelete() {
        return confirm('Are you sure you want to delete this category?');
    }
</script>
