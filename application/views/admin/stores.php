      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <div class="row">
              <div class="col-lg-12 col-md-12">
			  <div class="panel panel-transparent">
                <!-- START PANEL -->
                <?php if($this->session->flashdata('success')==true): ?>
				<div class="message_block">
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
                    </div>
        <?php endif;?>
                <div class="panel panel-transparent">
                  <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/stores'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Store Name</label>
                            <input type="text" class="form-control" name="sname" placeholder="Enter Store Name" required>
                            
                          </div>
                        </div>
                      </div>
                                            <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Store Location</label>
                            <input type="text" class="form-control" name="slocation" placeholder="Enter Store Location" required>
                          </div>
                        </div>
                      </div>
                      
                   <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Store Image</label>
                            <input type="file" class="form-control" name="userfile" id="img-prev" required>
                            <div class="imageprview"></div>
                          </div> 
                        </div>
                      </div>
                      
       
                      <div class="clearfix"></div>
                      <div class="add_btn show_screen">
                            <div class="checkbox check-success">
                        <input type="checkbox" id="size_s" name="show_screen" value="1">
                        <label for="size_s">Start On Show Screen</label>
                             </div>
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Store" >
					  </div>
                    </form>
                  </div>
                </div>
                <!-- END PANEL -->
              </div>
              </div>
			  
            </div>
          </div>
          
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
