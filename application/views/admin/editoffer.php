      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
			<div class="container-fluid container-fixed-lg">
				<div class="row">
				  
                         <!-- START PANEL -->
                          <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Update Offer
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                <?php if($this->session->flashdata('success')==true): ?>
                <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
        <?php endif;?>
                <div class="modal_form">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" method="post" action="<?php echo base_url('admin/offers/updateoffer/'.$offeredit[0]->o_id); ?>">
					
					<div class="row">
                        <div class="col-sm-6">
                          <div class="form-group form-group-default">
                            <label>Offer Title</label>
                            <input type="text" class="form-control" name="otitle" value="<?php echo $offeredit[0]->o_title ;?>" required>
                            
                          </div>
                        </div>
                        <div class="col-sm-6">
                         <div class="form-group form-group-default">
                            <label>Offer Date</label>
                            
                    <div id="datepicker-component" class="input-group date col-sm-8">
    <input type="text"  name="date" class="form-control"  value="<?php echo date_format(date_create($offeredit[0]->o_date),'m/d/Y');?>" required><span class="input-group-addon" style="background:transparent;border:none;"></span>
                        </div>
                          </div>
                        </div>
						 <div class="col-sm-12">
                           <div class="form-group form-group-default">
                            <label>Offer Description</label>
                            <input type="text" class="form-control" name="odetail" value="<?php echo $offeredit[0]->o_detail ;?>" required>
                          </div>
                        </div>

                      </div>
                     
                     
					  
					
                      <div class="clearfix"></div>
						 <div class="add_btn">
                      <input type="submit" name="submit" class="btn btn-primary" value="Update Offer" >
					  </div>
                    </form>
                </div>
                </div>
                <!-- END PANEL -->
			</div>
			</div>
             
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
