<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Product Details
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">

                            <a href="<?php echo base_url('admin/products/'); ?>" id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Products</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                                <!--Start Delete all-->
                <form method='post' action="<?php echo base_url();?>admin/products/archiveproductslist" id="userform"><input type="submit" value='Delete' data-text="You will not be able to recover these records" class="del_event btn btn-danger submitallids"  style='display:none' name='bulk_delt'>
              <input type="hidden" name="deleteallhidden" id="deleteallhidden" value="0">
              <input type="hidden" name="deleteallids" id="deleteallids">
               <input type="hidden" name="submitdone" id="submitdone" value="submitdone">
                <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block tableclass" id="tableWithSearch">
                        <thead>
                            <tr>
                                 <th>
                                 <div class="checkbox check-success">                 
                                 <input type="checkbox" name="deleteall[]" class="checkAll"  id="checkbox0"> 
                                 <label for="checkbox0"></label>
                                 </div>
                                </th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Price</th>
								<th>Brand</th>
								<th>Store</th>
								<th>Status</th>
                                <th>Added By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($archiveproductslist as $row):?>
                                <tr>
                                      <td>
                                    <div class="checkbox check-success">                 
                                    <input type='checkbox' class='checkdata' value="<?php echo $row->p_id;?>" name='checked_events[]' id="checkbox<?php echo $row->p_id;?>"> 
                                    <label for="checkbox<?php echo $row->p_id;?>"></label>
                                     </div>
                                   </td>
                                     <td class="v-align-middle semi-bold">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpimg-<?php echo $row->p_id;?>">
                                            <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo $product_image = ($row->p_image == 'product_default.png')?DEFAULT_IMAGE_URL . $row->p_image : 
		  		BASE_IMAGE_URL . $row->p_image ; ?>&h=80&w=80">
                                           
				</a>
                                     </td>
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->p_name;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                       <span class="text-success <?php if( $row->p_new_price != ''){ echo 'linethrough'; } ?> badges"><?php echo $row->p_currency.$row->p_original_price;?></span>
                                          <?php if( $row->p_new_price != ""){ ?><span class="text-danger badges current"><?php echo $row->p_currency.$row->p_new_price;?></span><?php } ?>
                                    </td>
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                           <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpb-<?php echo $row->b_id;?>"> <?php echo $row->b_name;?></a>
                                        </p>
                                    </td>
									 <td class="v-align-middle semi-bold">
                                        <p>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUp-<?php echo $row->s_id;?>"><?php echo $row->s_name;?></a>
                                        </p>
                                    </td>
									 <td class="v-align-middle">
				           <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
				     	 <a class="updatestatusproduct" theid="<?php echo $row->p_id ; ?>" thestatus="<?php echo $row->p_status ; ?>"  href="javascript:;" id="statsproduct<?php echo $row->p_id ;?>">
					     <div class="statuschangeproduct<?php echo $row->p_id; ?>">
				     	 <?php if($row->p_status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
				    	 </div>
			      	   	 </a>
				               	 </td>
									 <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->p_added_by;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
		   <span data-toggle="modal" data-target="#modalSlideUpp-<?php echo $row->p_id;?>" >
				<a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" data-original-title="View product"  class="btn btn-primary m-b-10"><i class="fa fa-eye"></i></a>
				</span>
				
				<a href="<?php echo base_url('admin/products/recoverproducts/'.$row->p_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Recover product" class="btn btn-info m-b-10"><i class="fa fa-paste"></i></a>
		   

		<!-- Indicates a dangerous or potentially negative action -->
				
		<a data-text="You will not be able to recover this product" data-toggle="tooltip" data-placement="top" data-original-title="Delete product"  data-url="<?php echo base_url('admin/products/deleteproducts/'.$row->p_id);?>" href="javascript:void(0);" class="btn btn-danger m-b-10 deleterecord"><i class="fa fa-trash-o"></i> </a>
          </td>
                               </tr>
								
											<!--- Brand Modal Detail-->
			<div class="modal fade slide-up disable-scroll" id="modalSlideUpb-<?php echo $row->b_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog ">
					<div class="modal-content-wrapper">
						<div class="modal-content">
							<div class="modal-header clearfix text-left">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
								<h5>Brand <span class="semi-bold">Detail</span></h5> </div>
							<div class="modal-body">
							<div class="form-group-attached single-product-detail">
								<div class="row dialog__overview">
									<div class="col-sm-6 no-padding item-slideshow-wrapper full-height">
										<div class="form-group form-group-default"> <img src="<?php echo base_url('/uploads/'.$row->b_logo);?>" height="250" width="300"> </div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-group-default">
											<label>Brand Name</label>
											<div class="panel-title"><?php echo $row->b_name;?>
											</div>
											<label>Brand Detail</label>
											<p>
												<?php echo $row->b_details;?>
											</p>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
						<!-- /.modal-content -->
					</div>
				</div>
			</div>
			<!--- End Brand Modal Detail-->
			<!--- Store Modal Detail-->
			<div class="modal fade slide-up disable-scroll" id="modalSlideUp-<?php echo $row->s_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog ">
					<div class="modal-content-wrapper">
						<div class="modal-content">
							<div class="modal-header clearfix text-left">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
								<h5>Store <span class="semi-bold">Detail</span></h5> </div>
							<div class="modal-body">
						<div class="form-group-attached single-product-detail">
							<div class="row dialog__overview">
								<div class="col-sm-7 no-padding item-slideshow-wrapper full-height">
									<div class="form-group form-group-default"> <img src="<?php echo base_url('/uploads/'.$row->s_image);?>" height="250" width="300"> </div>
								</div>
								<div class="col-sm-5">
									<div class="form-group form-group-default">
										<div class="panel-title">
										<label>Store Name</label><?php echo $row->s_name;?>
										</div>
										<label>Store Location</label>
										<p>
											<?php echo $row->s_location;?>
										</p>
										
									</div>
								</div>
							</div>
						</div>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				</div>
			</div>
			<!--- End Store Modal Detail-->
			<!--- Single Product Modal Detail-->
			<div class="modal fade slide-up disable-scroll" id="modalSlideUpp-<?php echo $row->p_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog modal-lg view_product">
					<div class="modal-content-wrapper">
						<div class="modal-content">
							<div class="modal-header clearfix text-left">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>
							</div>
							<div class="modal-body">
								<div class="form-group-attached single-product-detail">
									<div class="row dialog__overview">
										<div class="col-sm-7 no-padding item-slideshow-wrapper full-height">
											<div class="flexslider">
  <ul class="slides">
    <li data-thumb="<?php echo $product_image = ($row->p_image == 'product_default.png')?DEFAULT_IMAGE_URL . $row->p_image : BASE_IMAGE_URL . $row->p_image ; ?>">
      <img src="<?php echo $product_image = ($row->p_image == 'product_default.png')?DEFAULT_IMAGE_URL . $row->p_image : BASE_IMAGE_URL . $row->p_image ; ?>" />
    </li>
<?php foreach($row->multiple as $rows){ ?>
    <li data-thumb="<?php echo base_url('/uploads/'.$rows->p_image_name);?>">
      <img src="<?php echo base_url('/uploads/'.$rows->p_image_name);?>" />
    </li>
    <?php } ?>
  </ul>
</div>
										</div>
										<div class="col-sm-5">
											<div class="form-group form-group-default">
												<div class="panel-title">
													<?php echo $row->p_name;?>
												</div>
												<div class="price_sec">
										<p class="text-success <?php if($row->p_new_price!=''){echo 'linethrough';}?>">
														<?php echo $row->p_currency.$row->p_original_price;?>
													</p>
													<p class="text-danger">
											<?php if($row->p_new_price!=''){echo $row->p_currency.$row->p_new_price;}?>
													</p>
												</div>
												<label>Product Detail</label>
												<p>
													<?php echo $row->p_detail;?>
												</p>
												<label>Product Size</label>
												<p>
													<?php $sizo= explode(',', $row->p_size)?>
														<?php foreach($sizo as $siz){?> <span class="btn btn-success m-t-5"><?php echo $siz;?></span>
															<?php } ?>
												</p>
												<label>Product Color</label>
												<p>
									<?php $colo = explode('|',$row->p_color)?>
							<?php foreach($colo as $co){?> <div class="padding-15 pull-left m-r-5" style="background-color:<?php echo $co;?>"></div>
															<?php } ?>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				</div>
			</div>
			<!--- End Single product Modal Detail-->
			
			 <!--- Product single Modal Image-->       
          <div class="modal img-popup scrollclass fade slide-up disable-scroll" id="modalSlideUpimg-<?php echo $row->p_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">					     					
					  <div class="form-group form-group-default">						
						<img src="<?php echo $product_image = ($row->p_image == 'product_default.png')?DEFAULT_IMAGE_URL . $row->p_image : 
		  		BASE_IMAGE_URL . $row->p_image ; ?>">
						
					  </div>	
					</div>
				   </div>
                  </div>
              </div>
              <!-- /.modal-content -->
			  
            </div>
          </div>
		  </div>
 <!--- End Product Single Modal Image--> 
							
								
								
                                <?php endforeach; ?>
                                    
                        </tbody>
                    </table>
                </div>
                 </form>
            </div>
        </div>
		
		
		
    </div>
    <!-- END PANEL -->
</div>

</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->
