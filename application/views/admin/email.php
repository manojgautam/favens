<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1" name="viewport">
<title>Partykarlo</title>
<style type="text/css">
@media all and (max-width:560px) {
*[class].width312 {
width: 100%!important;

}

*[class].width312_left {
width: 100% !important;
display: block; 

}
*[class].width312 li {
display: inline-block;
}
*[class].resImage {
width: 100%!important;
height:auto !important;
}
*[class].heightauto {
height:auto !important;
}
*[class].font30 {
font-size: 30px!important
}
*[class].font20 {
font-size: 20px!important
}
*[class].font16 {
font-size: 16px!important
}
*[class].font12 {
font-size: 12px!important
}
*[class].lessWidth {
width: 0px!important
}
*[class].logoWidth {
width: 100%!important
}
*[class].aligncenter {
text-align: center!important
}


}
.mailtemplate a{
	color: #07a8e9;
}
</style>
</head>
    
<body style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size adjust:100%; margin:0; padding:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="width312" style="background:#fff;padding: 10px 0px 0px 0px;">
<tbody>
<tr>
<td class="width312" width="600">
<table class="width312" width="600" border="0" cellpadding="0" cellspacing="0" style="
    padding-top: 0;
    padding-bottom: 0;  background: #000;
">
<tbody style="">

<tr>
<td class="width312">
<table class="width312" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tbody><tr>
<td style="padding:20px 0 8px 14px;" align="center" class="width312 no-padding">
    <a href="#"><img border="0" src=""  align="center"></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="background-color: transparent;" height="17" bgcolor="#ffffff"></td>
</tr>
</tbody></table></td>
</tr>
<tr>
<td align="left" style="background:#d10001;padding:20px">
<table align="left" cellspacing="0" style="max-width:650px;min-width:320px">
<tbody>

<tr>
<td align="left" style="background:#fff;padding:30px 20px">

<table align="left">
<tbody>
<tr>
<td style="color:#666;text-align:center">

<table align="left" style="margin:auto">
<tbody>

<tr>
<td style="text-align:left;">
	<table>
		<tbody>
			<tr>
			<tr>
<td style="color:#666;font-size:20px;font-weight:bold;text-align:left;font-family:arial">
Hi <?php echo $name;?>,
</td>
			
			</tr>
		</tbody>
	</table>
</td>
</tr>
</tbody>
</table>

<table align="left" style="margin:auto">
<tbody>
<tr>
<td class="mailtemplate" style="color:#666;font-size:14px;padding-bottom:8px;text-align:left;font-family:arial; line-height : 21px; padding-top: 5px;">
<p><?php echo $message;?></p>
<?php if(isset($link_text)){ ?>
    <p><a href="<?php echo $link;?>"><?php echo $link_text;?></a></p>
<?php } ?>
    <p><?php echo $message1; ?></p>
</td>
</tr>
</tbody></table>


</td>
</tr>
<tr>
<td style="color:#666;padding:0 15px;font-size:14px;line-height:18px;text-align:left">
<div style="padding-top:10px;text-align:left;font-family:arial">
<img alt="Thanks" src=""/>
</div>

</td>
</tr>

</tbody>
</table>


</td>
</tr>

</tbody>
</table>

</td>
</tr>

<tr>
<td align="left" valign="bottom" height="1" style="font-size:2px; line-height:2px; _height:2px;"></td>
</tr>

<tr style='background-color:#000;'>
<td>
<table align="left" cellspacing="0" style="max-width:650px; min-width:320px; padding: 0 22px;">
<tbody>
<tr>
<td align="left" valign="bottom" style="font-family: arial,helvetica,sans-serif; font-size: 18px; color: #444444; text-align: center; padding-top: 5px;"><table width="100%" border="0" cellspacing="0" cellpadding="5">
<tbody><tr>
<td align="left" style="font-family: arial,helvetica,sans-serif; font-size: 15px; color: #fff; text-decoration:none; padding-top : 15px;">
   <!-- <img src="<?php echo base_url();?>uploads/ic_fb.png" alt="facebook" width="28px"> <img src="<?php echo base_url();?>uploads/ic_twitter.png" alt="twitter" width="28px"> <img src="<?php echo base_url();?>uploads/ic_linkdin.png" alt="linkdin" width="28px"> <img src="<?php echo base_url();?>uploads/ic_youtube.png" alt="youtube" width="28px"> -->  </td>
</tr>
</tbody></table></td>
</tr>
<tr>
<td align="left" style="font-size:10px;line-height:14px; font-family:Arial, Helvetica, sans-serif; color:#fff;padding-bottom : 15px;">  &copy; 2017-18. http://favens.devskart.com/ | All rights reserved.<br>
</td>
   
    
</tr>
</tbody>
</table>
</td>
</tr>

</tbody>
</table>	
</body>
</html>
