<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Brands Details
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">

                            <a href="<?php echo base_url('admin/brands/'); ?>" id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Brand</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>Brand Name</th>
                                <th>Logo</th>
                                <th>Products Count</th>
								<th>Brand Status</th>
								<th>Added By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  foreach($brandlist as $row):?>
                                <tr>
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->b_name;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle"><a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpimg-<?php echo $row->b_id;?>" ><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url('/uploads/'.$row->b_logo);?>&h=50&w=50">
				</a></td>
                                    <td class="v-align-middle semi-bold">
                                        
                                           <a href="<?php echo base_url('admin/products/productslist/brand/'.$row->b_id); ?>"><span class="label label-success"> <?php echo $row->b_p_count;?></span></a>
                                       
                                    </td>
									     <td class="v-align-middle">
				           <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
				     	 <a class="updatestatusbrand" theid="<?php echo $row->b_id ; ?>" thestatus="<?php echo $row->b_status ; ?>"  href="javascript:;" id="statsbrand<?php echo $row->b_id ;?>">
					     <div class="statuschangebrand<?php echo $row->b_id; ?>">
				     	 <?php if($row->b_status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
				    	 </div>
			      	   	 </a>
				               	 </td>
									  <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->b_added_by;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                        <span data-toggle="modal" data-target="#modalSlideUpb-<?php echo $row->b_id;?>">
                                       <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="View Brand"  class="btn btn-primary  m-b-10"><i class="fa fa-eye"></i> </a>
                                       </span>
                                       <a href="<?php echo base_url('admin/brands/editbrands/'.$row->b_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit Brand"  class="btn btn-info m-b-10"><i class="fa fa-edit"></i></a>
                                    
                                        <!-- Indicates a dangerous or potentially negative action -->
                                        <a href="<?php echo base_url('admin/brands/archivebrands/'.$row->b_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Archive Brand"  class="btn btn-danger m-b-10"><i class="fa fa-warning"></i> </a>
                                    </td>
                                </tr>
                                
                                
                                
                                  <!--- Brand Modal Detail-->                  
   <div class="modal fade slide-up disable-scroll" id="modalSlideUpb-<?php echo $row->b_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Brand <span class="semi-bold">Detail</span></h5>
                    
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">
					  <div class="col-sm-6">
					  <div class="form-group form-group-default">
						<label>Brand Logo</label>
						<img src="<?php echo base_url('/uploads/'.$row->b_logo);?>" height="200" width="200">
					  </div>
					  </div>
					  <div class="col-sm-6">
					  <div class="form-group form-group-default">
						<label>Brand Name</label>
						<p><?php echo $row->b_name;?></p>
					  </div>
					
					  <div class="form-group form-group-default">
						<label>Brand Detail</label>
						<p><?php echo $row->b_details;?></p>
					  </div>
					 </div>
					

					</div>

				   </div>
  
                  </div>
               
              </div>
              <!-- /.modal-content -->
            </div>
          </div>
		  </div>
<!--- End Brand Modal Detail-->      

			        <!--- Brand Modal Image-->       
          <div class="modal img-popup fade slide-up disable-scroll" id="modalSlideUpimg-<?php echo $row->b_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog ">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                  </div>
                         <div class="modal-body">
					   <div class="form-group-attached">
					 <div class="row">					     					
					  <div class="form-group form-group-default">						
						<img src="<?php echo base_url('/uploads/'.$row->b_logo);?>" >
					  </div>	
					</div>
				   </div>
                  </div>
              </div>
              <!-- /.modal-content -->
			  
            </div>
          </div>
		  </div>
 <!--- End Brand Modal Image--> 
 
                                <?php endforeach; ?>
                                  
                        </tbody>
                    </table>
                           
            </div>
          </div>
          <!-- /.modal-dialog -->
                </div>
            </div>
        </div>
    </div>
    <!-- END PANEL -->
</div>

</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->