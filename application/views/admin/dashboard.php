
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid padding-25 sm-padding-10">
             <div class="row">
                  <div class="col-md-3 m-b-10">
                    <!-- START WIDGET widget_progressTileFlat-->
                    <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="panel-heading  top-left top-right">
                              <div class="panel-title text-white">
                                <span class="font-montserrat fs-11 all-caps">Total Products <i
		                                class="fa fa-chevron-right"></i>
                                                    </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="p-l-20 p-t-15">
                              <h3 class="no-margin p-b-5 text-white"><?php echo $productcount; ?></h3>
                              <a href="#" class="btn-circle-arrow text-white"><i
								class="pg-arrow_minimize"></i>
						</a>
						<?php if($todayproductcount!= 0) { ?>
                              <span class="small hint-text"><?php echo $todayproductcount; ?>/<?php echo $productcount; ?> Product added today</span>
                              <?php } else {?>
                               <span class="small hint-text">No Product added today</span>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-bottom">
                            <div class="progress progress-small m-b-20">
                              <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                              <?php $value= ($todayproductcount/$productcount) * 100 ;?>
                              <div class="progress-bar progress-bar-white" style="width:<?php echo $value; ?>%"></div>
                              <!-- END BOOTSTRAP PROGRESS -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
				   <div class="col-md-3 m-b-10">
                    <!-- START WIDGET widget_progressTileFlat-->
                    <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="panel-heading  top-left top-right">
                              <div class="panel-title text-white">
                                <span class="font-montserrat fs-11 all-caps">Total Brands <i
		                                class="fa fa-chevron-right"></i>
                                                    </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="p-l-20 p-t-15">
                              <h3 class="no-margin p-b-5 text-white"><?php echo $brandcount; ?></h3>
                              <a href="#" class="btn-circle-arrow text-white"><i
								class="pg-arrow_minimize"></i>
						</a>
							<?php if($todaybrandcount!= 0){ ?>
                              <span class="small hint-text"><?php echo $todaybrandcount; ?>/<?php echo $brandcount; ?> Brands added today</span>
                              <?php } else { ?>
                               <span class="small hint-text">No Brands added today</span>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-bottom">
                            <div class="progress progress-small m-b-20">
                              <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                              <?php $value= ($todaybrandcount/$brandcount) * 100 ;?>
                              <div class="progress-bar progress-bar-white" style="width:<?php echo $value;?>%"></div>
                              <!-- END BOOTSTRAP PROGRESS -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
				   <div class="col-md-3 m-b-10">
                    <!-- START WIDGET widget_progressTileFlat-->
                    <div class="widget-9 panel no-border bg-success no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="panel-heading  top-left top-right">
                              <div class="panel-title text-white">
                                <span class="font-montserrat fs-11 all-caps">Total Stores <i
		                                class="fa fa-chevron-right"></i>
                                                    </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="p-l-20 p-t-15">
                              <h3 class="no-margin p-b-5 text-white"><?php echo $storecount; ?></h3>
                              <a href="#" class="btn-circle-arrow text-white"><i
								class="pg-arrow_minimize"></i>
						</a>
						<?php if($todaystorecount!= 0){ ?>
                              <span class="small hint-text"><?php echo $todaystorecount; ?>/<?php echo $storecount; ?> Stores added today</span>
                              <?php } else { ?>
                               <span class="small hint-text">No Stores added today</span>
                            <?php  }?>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-bottom">
                            <div class="progress progress-small m-b-20">
                              <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                               <?php $value= ($todaystorecount/$storecount) * 100 ;?>
                              <div class="progress-bar progress-bar-white" style="width:<?php echo $value;?>%"></div>
                              <!-- END BOOTSTRAP PROGRESS -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
				   <div class="col-md-3 m-b-10">
                    <!-- START WIDGET widget_progressTileFlat-->
                    <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="panel-heading  top-left top-right">
                              <div class="panel-title text-white">
                                <span class="font-montserrat fs-11 all-caps">Total Users <i
		                                class="fa fa-chevron-right"></i>
                                                    </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top">
                            <div class="p-l-20 p-t-15">
                              <h3 class="no-margin p-b-5 text-white"><?php echo $usercount; ?></h3>
                              <a href="#" class="btn-circle-arrow text-white"><i
								class="pg-arrow_minimize"></i>
						</a>
                              <?php if($todayusercount!= 0){ ?>
                              <span class="small hint-text"><?php echo $todayusercount; ?>/<?php echo $usercount; ?> User added today</span>
                              <?php } else { ?>
                               <span class="small hint-text">No User added today</span>
                            <?php  }?>
                            </div>
                          </div>
                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-bottom">
                            <div class="progress progress-small m-b-20">
                              <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                              <?php $value= ($todayusercount/$usercount) * 100 ;?>
                              <div class="progress-bar progress-bar-white" style="width:<?php echo $value; ?>%"></div>
                              <!-- END BOOTSTRAP PROGRESS -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
                </div>
		   
		   
            <div class="row">
              <div class="col-md-8 col-lg-6 col-xlg-6 m-b-10">
                <div class="row">
                  <div class="col-md-12">
                    <!-- START WIDGET D3 widget_graphWidget-->
                    <div class="widget-12 panel no-border widget-loader-circle no-margin">
                      <div class="row">
                        <div class="col-xlg-8 ">
                          <div class="panel-heading pull-up top-right ">
                            <div class="panel-controls">
                              <ul>
                                <li>
                                  <a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xlg-8 ">
                            <div class="p-l-10">
                              <h2 class="pull-left">All Details</h2>
                              
                              <div class="clearfix"></div>
                              <div class="full-width">
                                <ul class="list-inline">
                                    
                                  <li><a href="<?php echo base_url();?>admin/products/productslist" class="font-montserrat text-master">Products <span class="label label-success"><?php echo $productcount; ?></span></a>
                                  </li>
                                  <li><a href="<?php echo base_url();?>admin/brands/brandslist" class="font-montserrat text-master">Brands <span class="label label-success"><?php echo $brandcount; ?></span></a>
                                  </li>
                                  <li><a href="<?php echo base_url();?>admin/stores/storeslist" class="font-montserrat text-master">Stores <span class="label label-success"><?php echo $storecount; ?></span></a>
                                  </li>
								  <li><a href="<?php echo base_url();?>admin/users" class="font-montserrat text-master">Users <span class="label label-success"><?php echo $usercount; ?></span></a>
                                  </li>
                                </ul>
                              </div> 

            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart" style="height:250px"></canvas>
              </div>
          </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
                </div>
              </div>
			  
			     <div class="col-md-8 col-lg-6 col-xlg-6 m-b-10">
                <div class="row">
                  <div class="col-md-12">
                    <!-- START WIDGET D3 widget_graphWidget-->
                    <div class="widget-12 panel no-border widget-loader-circle no-margin">
                      <div class="row">
                        <div class="col-xlg-8 ">
                          <div class="panel-heading pull-up top-right ">
                            <div class="panel-controls">
                              <ul>
                                <li>
                                  <a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-xlg-8 ">
                            <div class="p-l-10">
                              <h2 class="pull-left">Today's Update</h2>
                              
                              <div class="clearfix"></div>
                              <div class="full-width">
                                <ul class="list-inline">
                                   <li><a href="<?php echo base_url();?>admin/users/today" class="font-montserrat text-master">Users <span class="label label-success"><?php echo $todayusercount; ?></span></a>
                                  </li>
								  
                                  <li><a href="<?php echo base_url();?>admin/products/productslist/today" class="font-montserrat text-master">Products <span class="label label-success"><?php echo $todayproductcount; ?></span></a>
                                  </li>
                                  
                                </ul>
                              </div> 
    <div class="box-body">
              <div class="chart">
                <canvas id="areaChart1" style="height:250px"></canvas>
              </div>
            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- END WIDGET -->
                  </div>
                </div>
              </div>
              
            </div>
			
			
			  
			
			
			
			
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

      </div>
      <!-- END PAGE CONTENT WRAPPER -->
