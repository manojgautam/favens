      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
			<div class="container-fluid container-fixed-lg">
				<div class="row">
				  <div class="col-lg-12 col-md-12">
                         <!-- START PANEL -->
                          <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Edit Product
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                <?php if($this->session->flashdata('success')==true): ?>
                <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
        <?php endif;?>
                <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" method="post" action="<?php echo base_url('admin/products/editproduct/'.$productdetail[0]->p_id); ?>">
			 <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>		
					<div class="row">
					<div class="col-sm-6">
                        <div class="col-sm-12 no-padding">
                          <div class="form-group form-group-default required">
                            <label>Name</label>
                            <input type="text" class="form-control" name="pname" placeholder="Enter Name" value="<?php echo $productdetail[0]->p_name;?>" required>
                            
                          </div>
                        </div>
<input type="hidden" name="editpage" id="editpage" value="editpage">

						
                        <div class="col-sm-5 no-padding">
                           <div class="form-group form-group-default required">
                            <label>Original Price</label>
                            <input type="text" class="form-control" name="poprice" placeholder="Original Price" value="<?php echo $productdetail[0]->p_original_price;?>" required>
                          </div>
                        </div>
						 <div class="col-sm-5 no-padding">
                           <div class="form-group form-group-default">
                            <label>New Price</label>
                            <input type="text" class="form-control" name="pnprice" placeholder="New Price" value="<?php echo $productdetail[0]->p_new_price;?>">
                          </div>
                        </div>
						<div class="col-sm-2 no-padding">
                           <div class="form-group form-group-default">
                            <label>Currency</label>
                              <select class="cs-select cs-skin-slide" data-init-plugin="cs-select"  name="p_currency">
                               <?php foreach($currency as $curr){?>
    <option  <?php if( $productdetail[0]->p_currency == $curr->currency){echo "selected";}?>  value="<?php echo $curr->currency;?>"><?php echo $curr->currency;?></option> 
                              <?php } ?> 
                           </select>
                          </div>
                        </div>
						
        <div class='col-sm-12'>
            <div class="form-group">
			 <label>Offer Date Range</label>
			 <?php if(count($activeproduct)){
			    $daterange =  date_format(date_create($activeproduct[0]->p_start_date),'m/d/Y h:i A') ." - ".date_format(date_create($activeproduct[0]->p_end_date),'m/d/Y h:i A');
			     } 
			     else{
			         
			         $daterange = "";
			     }
			 
			 ?>
			 
                 <div class="input-prepend input-group">
                      <span class="add-on input-group-addon"><i
									class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                      <input type="text" style="width: 100%" name="p_date_range" id="daterangepicker" class="form-control" value="<?php echo $daterange;?>" />
                    </div>
                    <input type="hidden" value="<?php if(count($activeproduct)){echo "update"; }else {echo "add"; } ?>" name="check_product_date">
            </div>
        </div>       
						
                        </div>
                     

                          <div class="col-sm-6">
                          <div class="form-group form-group-default product_details required">
                            <label>Details</label>
                          
                            <textarea class="form-control" name="pdetail" placeholder="Enter Details" required><?php echo $productdetail[0]->p_detail;?></textarea>
                             
                          </div>
                        </div>
                          

					   </div>
					  
                        <div class="row">
                        <div class="col-sm-6">
					<div class="form-group form-group-default">
                            <label>Add New Primary Image</label>
                            <input type="file" class="form-control" name="userfile1"  >
        <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo $product_image = ($productdetail[0]->p_image == 'product_default.png')?DEFAULT_IMAGE_URL . $productdetail[0]->p_image : 
		  		BASE_IMAGE_URL . $productdetail[0]->p_image ; ?>&h=80&w=80">

<input type="hidden" name="oldimage" value="<?php echo $productdetail[0]->p_image;?>">
                          </div> 
                        </div>
						<div class="col-sm-6">
					<div class="form-group form-group-default seconday_images">
                            <label>Add New Secondary Images</label>
                            <input type="file" class="form-control" name="userfile2[]" multiple  >
<?php foreach($multimg as $val){ ?>
<div class="sec_edit_images">
<img  alt="your image" style="width:80px;height:80px;" class="delmultimages<?php echo $val->image_id;?>" src="<?php echo base_url();?>uploads/<?php echo $val->p_image_name;?>">
<button data-image="<?php echo $val->image_id;?>" class="delmultimages" id="delmul<?php echo $val->image_id;?>">X</button>
</div>

<?php } ?>
                          </div> 
                        </div>
                      </div>
					  

					  
					  <div class="row">
                        <div class="col-sm-6">
                          <div class="sizes_avail">
                            <label>Sizes Available <span style="color:#f55753">*</span></label>



				<?php  $sizearray=explode(',',$productdetail[0]->p_size);?>			
                         <div class="checkbox check-success">
		       <input type="checkbox" id="size_s" name="size[]" <?php if (in_array('S',$sizearray)) {echo 'checked';}?>  value="S">
                          <label for="size_s">S</label>
                          <input type="checkbox" id="size_m" name="size[]" <?php if (in_array('M',$sizearray)) {echo 'checked';}?> value="M">
                          <label for="size_m">M</label>
                          <input type="checkbox" id="size_l" name="size[]" <?php if (in_array('L',$sizearray)) {echo 'checked';}?> value="L">
                          <label for="size_l">L</label>
		<input type="checkbox" id="size_xl" name="size[]" <?php if (in_array('XL',$sizearray)) {echo 'checked';}?> value="XL">
                          <label for="size_xl">XL</label>
	       <input type="checkbox" id="size_xxl" name="size[]" <?php if (in_array('XXL',$sizearray)) {echo 'checked';}?> value="XXL">
                          <label for="size_xxl">XXL</label>
                        </div>
                            
                          </div>
                          <p id="chk-error" class="error"></p>

                        </div>
						   <div class="col-sm-6">
                          <div class="sizes_avail">
                            <label>Colour Available <span style="color:#f55753">*</span></label>
                       <div class="color-field row">     
<?php  $colorarray=explode(',',$productdetail[0]->p_color);?>
<?php $i=100; foreach($colorarray as $colval){ ?>
<div class="form-group removediv<?php echo $i?> <?php if($i==100){}else{echo 'col-sm-3 no-padding addons';}?>">
<div class="input-group colorpicker-element">
<input type="color" class="form-control" name="color[]" value="<?php echo $colval;?>">
<?php if($i==100){?>
<div class="addcolorbutton">
<a href="javascript:void(0)" class="btn btn-white btn-xs btn-mini bold fs-14 add-color">Add</a>
</div>
<?php }else{ ?>
<div class="deletecolor">
<a href="javascript:void(0)" attrid="<?php echo $i?>" class="btn btn-white btn-xs btn-mini bold fs-14 delete-field">x</a>
</div>
<?php } ?>
</div></div>
<?php $i++;} ?>
</div>
						
                         <!--  <div class="checkbox check-success inline">
			  <input type="checkbox" id="color_b" name="color[]" <?php if (in_array('Black',$colorarray)) {echo 'checked';}?>  value="Black">
                          <label for="color_b">Black</label>
                          <input type="checkbox" id="color_bl" name="color[]" <?php if (in_array('Blue',$colorarray)) {echo 'checked';}?> value="Blue">
                          <label for="color_bl">Blue</label>
                          <input type="checkbox" id="color_w" name="color[]" <?php if (in_array('White',$colorarray)) {echo 'checked';}?> value="White">
                          <label for="color_w">White</label>
                        </div>-->
                            
                          </div>
                        </div>
                      </div>
					  
					  					  
					  
					  <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group form-group-default select_brands branddiv">
                            <label>Brand</label>
                            <select class="cs-select cs-skin-slide selbrand" data-init-plugin="cs-select" name="pbrand">
								<?php if($brandlist): foreach($brandlist as $row):?>
	<option value="<?php echo $row->b_id; ?>" <?php if($productdetail[0]->p_brand==$row->b_id){echo 'selected';}?>><?php echo $row->b_name; ?></option>
                                <?php endforeach; else:?>
								<p>No Brands Found</p>
								<?php endif; ?>
                    </select>
                            
                          </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group form-group-default select_brands storediv">
                            <label>Store Name</label>
                            <select class="cs-select cs-skin-slide selstore" data-init-plugin="cs-select" id="prodselopt" name="pstore">
							 <?php if($storeslist): foreach($storeslist as $row):?>
		<option value="<?php echo $row->s_id; ?>" <?php if($productdetail[0]->p_store==$row->s_id){echo 'selected';}?>><?php echo $row->s_name; ?></option>
                                <?php endforeach; else:?>
								<p>No Stores Found</p>
								<?php endif; ?>
                     
                    </select>
                          </div>
                        </div>
						
						 <div class="col-sm-4">
						 <div class="form-group form-group-default">
						                 <input type="hidden" name="hiddencats" id="hiddencats" value="<?php echo implode(',',$p_s_categories);?>">
                       <label><b>Select Categories</b></label>
                      <div class="row ajaxcateg">
                            <select id="multi" class="full-width" name="thecats[]" multiple>
                                <?php foreach( $catlist as $row):?>
                      <option value="<?php echo $row->c_id; ?>" <?php if (in_array($row->c_id,$p_s_categories)) {echo 'selected="selected"';}?>><?php echo $row->c_name; ?></option>
                      <?php endforeach; ?>
                           </select>
                     </div>
					 </div>
						
                      </div>
					  

			 

    <?php if(count($expiredproduct)){?>	
    	 <div class="expired_dates">
		 <div class="panel-heading">
                    <div class="panel-title">Expired Offers
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
    	 <table class="table table-hover table-condensed no-footer">
		 <thead>
		 <th>S.no</th>
		 <th>Start Date</th>
		 <th>End Date</th>
		 </thead>
		 <tbody>
		 <?php $i=1; foreach( $expiredproduct as $expired ){ ?>
		 <tr>
		 <td><?php echo $i;?></td>
		 <td><?php echo date_format(date_create($expired->p_start_date),'d F, Y h:i A');?></td>
		 <td><?php echo date_format(date_create($expired->p_end_date),'d F, Y h:i A'); ?></td>
		 </tr>
		 	<?php	$i++; } ?>
		 </tbody>
	</table>
	</div>
<?php } ?>
	
       
                      <div class="clearfix"></div>
						 <div class="add_btn show_screen">
                          <?php $show = $productdetail[0]->p_show_screen; ?>
                          <div class="checkbox check-success">
			  <input type="checkbox" id="showscreen" name="show_screen" <?php if( $show == 1){ echo "checked"; }?> value="1">
                          <label for="showscreen">Start On Show Screen</label>
                          </div>
                      <input type="submit" name="submitedit" class="btn btn-primary" id="prochk" value="Update Product" >
					  </div>
                    </form>
                </div>
                </div>
                <!-- END PANEL -->
			</div>
			</div>
			</div>
             
        </div>
        <!-- END COPYRIGHT -->
      </div>

      <!-- END PAGE CONTENT WRAPPER -->


