<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Active Offers
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">

                            <a href="<?php echo base_url('admin/offers/'); ?>" id="show-modal" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Offers</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>Offer Title</th>
                                <th>Offer Detail</th>
								<th>Offer Date</th>
                                <th>Offer Status</th>
                                <th>Offer Added By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($activeofferslist as $row):?>
                                <tr>
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->o_title;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                       <p> <?php echo $row->o_detail;?></p>
                                            
                                    </td>
                                    <td class="v-align-middle"> 
									<p><?php echo date_format(date_create($row->o_date),'d M,Y');?></p>
									</td>
                                   		 <td class="v-align-middle">
				           <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
				     	 <a class="updatestatusoffer" theid="<?php echo $row->o_id ; ?>" thestatus="<?php echo $row->o_status ; ?>"  href="javascript:;" id="statsoffer<?php echo $row->o_id ;?>">
					     <div class="statuschangeoffer<?php echo $row->o_id; ?>">
				     	 <?php if($row->o_status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
				    	 </div>
			      	   	 </a>
				               	 </td>
									 <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->o_added_by;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
									<span data-toggle="modal" data-target="#modalSlideUpp-<?php  echo $row->o_id;?>">
									     <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" data-original-title="View Offer" class="btn btn-primary  m-b-10"><i class="fa fa-eye"></i></a>
										 </span>
                                         <a href="<?php echo base_url('admin/offers/editoffer/'.$row->o_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Edit Offer" class="btn btn-info  m-b-10"><i class="fa fa-edit"></i></a>
                                      
                                        <!-- Indicates a dangerous or potentially negative action -->
                                        <a href="<?php echo base_url('admin/offers/archiveoffers/'.$row->o_id);?>" data-toggle="tooltip" data-placement="top" data-original-title="Archive Offer" class="btn btn-danger  m-b-10"><i class="fa fa-warning"></i></a>
                                      </td>
                                </tr>
								
								
											<!--- Single Product Modal Detail-->
			<div class="modal fade slide-up disable-scroll" id="modalSlideUpp-<?php echo $row->o_id;?>" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog view_product offer_lst">
					<div class="modal-content-wrapper">
						<div class="modal-content">
							<div class="modal-header clearfix text-left">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i> </button>																<h5>Offer <span class="semi-bold">Detail</span></h5>
							</div>
							<div class="modal-body">
								<div class="form-group-attached single-product-detail">
									<div class="row dialog__overview">
									<div class="col-sm-6">									<div class="form-group form-group-default">
												<label>Offer Title</label>
													<p><?php echo $row->o_title;?></p>
												</div>												</div>																								<div class="col-sm-6">												<div class="form-group form-group-default">
												<label>Offer Date</label>
												<p>
													<?php echo $row->o_date;?>
												</p>												</div>												</div>														<div class="col-sm-12">														<div class="form-group form-group-default">
												<label>Offer Deiscription</label>
												<p>
												   <?php echo $row->o_detail;?>
												</p>												</div>												</div>										
								
									</div>
								</div>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
				</div>
			</div>
			<!--- End Single product Modal Detail-->
                                <?php endforeach; ?>
                                   
                                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END PANEL -->
</div>

</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->
