      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
			<div class="container-fluid container-fixed-lg">
				<div class="row">
				 <div class="col-lg-12 col-md-12">
				  
                         <!-- START PANEL -->
                          <div class="panel panel-transparent">
           
                    
                    <div class="clearfix"></div>
               
                <?php if($this->session->flashdata('success')==true): ?>
				 <div class="message_block">
                <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
                    </div>
        <?php endif;?>
                <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" method="post" action="<?php echo base_url('admin/products'); ?>">
			 <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>		
	
<div class="row"><div class="col-sm-6">
                        <div class="col-sm-12 no-padding">						<div class="row">
                          <div class="form-group form-group-default required">
                            <label>Name</label>
                            <input type="text" class="form-control" name="pname" placeholder="Enter Name" value="<?php echo set_value('pname');?>" required>
                            
                          </div>
                        </div>                        </div>
                        <div class="col-sm-5 no-padding">						<div class="row">
                           <div class="form-group form-group-default required">
                            <label>Original Price</label>
                            <input type="text" class="form-control" name="poprice" placeholder="Original Price" value="<?php echo set_value('poprice');?>" required>
                          </div>
                        </div>                        </div>
						 <div class="col-sm-5 no-padding">						 <div class="row">
                           <div class="form-group form-group-default">
                            <label>New Price</label>
                            <input type="text" class="form-control" name="pnprice" placeholder="New Price" value="<?php echo set_value('pnprice');?>">
                          </div>
                        </div>                        </div>
						
						<div class="col-sm-2 no-padding currency">						<div class="row">
                           <div class="form-group form-group-default">
                            <label>Currency</label>
                              <select class="cs-select cs-skin-slide" data-init-plugin="cs-select" name="p_currency">
                              <?php foreach($currency as $curr){?>
			      <option value="<?php echo $curr->currency;?>"><?php echo $curr->currency;?></option> 
                              <?php } ?>                              
                              </select>
                          </div>
                        </div>                        </div>        <div class="col-sm-12 no-padding">		<div class="row">            <div class="form-group form-group-default">			 <label>Offer Date Range</label>                 <div class="input-prepend input-group">                      <span class="add-on input-group-addon"><i									class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>                      <input type="text" style="width: 100%" name="p_date_range" id="daterangepicker" class="form-control"  />                    </div>            </div>        </div>                      </div>                      </div>       
                          <div class="col-sm-6">
                          <div class="form-group form-group-default product_details required">
                            <label>Details</label>
                            <textarea class="form-control" name="pdetail" placeholder="Enter Details" required><?php echo set_value('pdetail');?></textarea>
                            
                          </div>
                        </div>					  					     </div>
					  
                        <div class="row">
                        <div class="col-sm-6">
					<div class="form-group form-group-default">
                            <label>Primary Image</label>
                            <input type="file" class="form-control" name="userfile1" id="img-prev">
<div class="imageprview"></div>
                          </div> 
                        </div>
						<div class="col-sm-6">
					<div class="form-group form-group-default">
                            <label>Secondary Image (multiple)</label>
                            <input type="file" class="form-control" name="userfile2[]" id="gallery-photo-add" multiple >
                         <div class="gallery2"></div>
                          </div> 
                        </div>
                      </div>
					  
					  <div class="row">
                        <div class="col-sm-6">
                          <div class="sizes_avail">
                            <label>Sizes Available <span style="color:#f55753">*</span></label>
							
                           <div class="checkbox check-success">
						  <input type="checkbox" id="size_s" name="size[]" value="S">
                          <label for="size_s">S</label>
                          <input type="checkbox" id="size_m" name="size[]" value="M">
                          <label for="size_m">M</label>
                          <input type="checkbox" id="size_l" name="size[]" value="L">
                          <label for="size_l">L</label>
						   <input type="checkbox" id="size_xl" name="size[]" value="XL">
                          <label for="size_xl">XL</label>
						   <input type="checkbox" id="size_xxl" name="size[]" value="XXL">
                          <label for="size_xxl">XXL</label>
                        </div>
                            
                          </div>
                          <p id="chk-error" class="error"></p>

                        </div>
						   <div class="col-sm-6">
                          <div class="sizes_avail">
                            <label>Colors Available <span style="color:#f55753">*</span></label>
                            
                       <div class="color-field row">     
                       <div class="form-group">
                        <div class="input-group colorpicker-element">
                          <input type="color" class="form-control" name="color[]">
                         
			  <div class="addcolorbutton">
                           <a href="javascript:void(0)" class="btn btn-white btn-xs btn-mini bold fs-14 add-color">Add</a>
                          </div>

                        </div>
                       </div>
                      </div>
                      
                         <!--  <div class="checkbox check-success inline">
						  <input type="checkbox" id="color_b" name="color[]" value="Black">
                          <label for="color_b">Black</label>
                          <input type="checkbox" id="color_bl" name="color[]" value="Blue">
                          <label for="color_bl">Blue</label>
                          <input type="checkbox" id="color_w" name="color[]" value="White">
                          <label for="color_w">White</label>
                        </div>-->
                            
                          </div>
                        </div>
                      </div>
                      

				
	  <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group form-group-default select_brands">
                            <label>Brand
							<div class="add-bs" style="float:right;"><a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpnewbrand" >Add Brand</a></div>
							</label>
                    <div class="newbrandadddiv">
                            <select class="cs-select cs-skin-slide" data-init-plugin="cs-select" name="pbrand">
								<?php if($brandlist): foreach($brandlist as $row):?>
				<option value="<?php echo $row->b_id; ?>"><?php echo $row->b_name; ?></option>
                                <?php endforeach; else:?>
								<p>No Brands Found</p>
								<?php endif; ?>
                           </select>
                    </div>
                            
                          </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group form-group-default select_brands">
                            <label>Store Name
							<div class="add-bs" style="float:right;"><a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpnewstore" >Add Store</a></div>
							</label>
                           <div class="newstoreadddiv">
                            <select class="cs-select cs-skin-slide" data-init-plugin="cs-select" id="prodselopt" name="pstore">
							 <?php if($storeslist): foreach($storeslist as $row):?>
					<option value="<?php echo $row->s_id; ?>"><?php echo $row->s_name; ?></option>
                                <?php endforeach; else:?>
								<p>No Stores Found</p>
								<?php endif; ?>
                     
                    </select></div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                        <div class="form-group form-group-default required">
        <label>Select Categories</label>
                       <div class="ajaxcateg">

                            <select id="multi" class="full-width" name="thecats[]" multiple required>
                                <?php foreach( $catlist as $row):?>
                      <option value="<?php echo $row->c_id; ?>"><?php echo $row->c_name; ?></option>
                      <?php endforeach; ?>
                    </select>
                      </div></div>
                          
                      </div>
                      </div>
					
                      <div class="clearfix"></div>
					  
						  <div class="add_btn show_screen">
						  <div class="checkbox check-success">
			  <input type="checkbox" id="showscreen" name="show_screen" value="1">
                          <label for="showscreen">Start On Show Screen</label>
                          </div>
                      <input type="submit" name="submit" class="btn btn-primary" id="prochk" value="Add Products" >
					  </div>
                    </form>
                </div>
                </div>
                </div>
				 </div>
                <!-- END PANEL -->
			</div>
			</div>
             
        </div>
        <!-- END COPYRIGHT -->
      </div>

      <!-- END PAGE CONTENT WRAPPER -->



			        <!--- Add New Brand Modal -->       
    <div id="modalSlideUpnewbrand" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Brand </h4>
      </div>
      <div class="modal-body">
           <form id="form-personal" class="addbrandForm" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/brands'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Brand Name</label>
                            <input type="text" class="form-control" name="bname" placeholder="Enter Brand Name" required>
                            
                          </div>
                        </div>
                      </div>
                                            <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Description</label>
                            <textarea class="form-control" name="bdetails" placeholder="Enter Description" required></textarea>
                         
                          </div>
                        </div>
                      </div>
                      

                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Logo</label>
                            <input type="file" class="form-control" name="userfile" id="img-prev1" required>
                            <div class="imageprview1"></div>
                          </div> 
                        </div>
                      </div>
       
                      <div class="clearfix"></div>
						 <div class="add_btn">
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Brand" >
					  </div>
                    </form>	
      </div>
         </div>

  </div>
</div>
 <!--- End Add new brand Modal --> 
 
 
			        <!--- Add New Store Modal -->       
    <div id="modalSlideUpnewstore" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Store </h4>
      </div>
      <div class="modal-body">
        <form id="form-personal" role="form" enctype="multipart/form-data" class="addstoreForm" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/stores'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Store Name</label>
                            <input type="text" class="form-control" name="sname" placeholder="Enter Store Name" required>
                            
                          </div>
                        </div>
                      </div>
                                            <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Store Location</label>
                            <input type="text" class="form-control" name="slocation" placeholder="Enter Store Location" required>
                          </div>
                        </div>
                      </div>
                      

                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Store Image</label>
                            <input type="file" class="form-control" name="userfilestore" id="img-prev2" required>
                            <div class="imageprview2"></div>
                          </div> 
                        </div>
                      </div>
       
                      <div class="clearfix"></div>
                      <div class="add_btn">
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Store" >
					  </div>
                    </form>
      </div>
         </div>

  </div>
</div>
 <!--- End Add new Store Modal --> 

