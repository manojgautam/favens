      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <!-- START PANEL -->
             
                <div class="panel panel-transparent">
                       <?php if($this->session->flashdata('success')==true): ?>                 
                    <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
        <?php endif;?>
                  <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/stores/updatestore/'.$storeedit[0]->s_id); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Store Name</label>
                            <input type="text" class="form-control" name="sname" value="<?php echo $storeedit[0]->s_name ;?>" required>
                            
                          </div>
                        </div>
                      </div>
                                            <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Store Location</label>
                            <textarea class="form-control" name="slocation" required><?php echo $storeedit[0]->s_location ;?></textarea>
                          </div>
                        </div>
                      </div>
                      
                        
                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Store Image</label>
                               <input type="file" class="form-control" name="userfile" id="imgInp">
                            <img id="blah" alt="your image" style="width:150px;height:100px;" src="<?php if($storeedit[0]->s_image == "") echo base_url('uploads/no-image.jpg'); else echo  base_url('uploads/'.$storeedit[0]->s_image);?>">
                            
                          </div> 
                        </div>
                      </div>
                      
                      
                      <input type="hidden" name="oldimage" value="<?php echo $storeedit[0]->s_image;?>"> 
       
                      <div class="clearfix"></div>
                      <div class="add_btn show_screen">
                           <div class="checkbox check-success">
                              <?php $show = $storeedit[0]->s_show_screen; ?>
                        <input type="checkbox" <?php if($show == 1){ echo "checked"; } ?> id="size_s" name="show_screen" value="1">
                        <label for="size_s">Start On Show Screen</label>
                             </div>
                      <input type="submit" name="submit" class="btn btn-primary" value="Update Store" >
                      </div>
                    </form>
                  </div>
                </div>
                <!-- END PANEL -->
              </div>
	
            </div>
          </div>
          
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
