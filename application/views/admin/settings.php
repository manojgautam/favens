      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
		    <div class="panel panel-transparent">
                  <div class="panel-body">
            <div class="row">
              <div class="col-lg-6 col-md-6 ">
                <!-- START PANEL -->
                <?php if($this->session->flashdata('success')==true): ?>
				<div class="message_block">
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
                    </div>
      
        <?php endif;?>
                
                    <form id="form-project" role="form" enctype="multipart/form-data" autocomplete="off" method="post" action="<?php echo base_url('admin/settings/settings'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label> Name</label>
                            <input type="text" class="form-control" name="name" value="<?php echo $details[0]->name ;?>" required>
                            
                          </div>
                        </div>
                      </div>


                 
<input type="hidden" name="oldimage" value="<?php echo $details[0]->image;?>"> 

                        <div class="row">
                        <div class="col-sm-12">
          <div class="form-group form-group-default">
                            <label>Profile Image</label>
                           <input type="file" name="userfile" class="form-control">
                            <img src="<?php if($details[0]->image == "") echo base_url('uploads/default.png'); else echo  base_url('uploads/'.$details[0]->image);?>" alt="" width="75" height="75">
                      
                          </div> 
                        </div>
                      </div>
                         
       
                      <div class="clearfix"></div>
                     <div class="add_btn">
                      <input type="submit" name="submitaccount" class="btn btn-primary" value="Update account" >
					  </div>
                    </form>
            
                <!-- END PANEL -->


              </div>
        
        <div class="col-lg-6 col-md-6">
               
                    <!-- password form start -->

          <form id="form-personal" role="form" autocomplete="off" method="post" action="<?php echo base_url('admin/settings/changepassword'); ?>">
  <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Old Password</label>
                           <input type="text" class="form-control" placeholder="Enter Old Password" name="oldpass" required>
                          </div>
                        </div>
                      </div>

                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>New Password</label>
                           <input type="text" class="form-control" placeholder="Enter New Password" name="newpass" required>
                          </div>
                        </div>
                      </div>

                    <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Confirm Password</label>
                           <input type="text" class="form-control" placeholder="Confirm Password" name="confirmpass" required>
                          </div>
                        </div>
                      </div>
                         
       
                      <div class="clearfix"></div>
                     
					 <div class="add_btn">
                      <input type="submit" name="submitpass" class="btn btn-primary" value="Update Password" >
					  </div>
                    </form>
               
                
<!-- password form end -->
                  </div>
                </div>
				
                </div>
                </div>
				
				
                <!-- END PANEL -->
             
          </div>
          
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
