      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <div class="row">
              <div class="col-lg-7 col-md-6 ">
                <!-- START PANEL -->
                <?php if($this->session->flashdata('success')==true): ?>
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
      
        <?php endif;?>
                <div class="panel panel-transparent">
                  <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/logos'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Logo Name</label>
                            <input type="text" class="form-control" name="lname" placeholder="Enter Logo Name" required>
                            
                          </div>
                        </div>
                      </div>
                                            

                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Logo</label>
                            <input type="file" class="form-control" name="userfile" id="imgInp" required>
                            <img id="blah" alt="your image" style="width:150px;height:100px;" src="<?php echo base_url();?>uploads/no-image.jpg"/>
                          </div> 
                        </div>
                      </div>
       
                      <div class="clearfix"></div>
                     
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Brand" >
                    </form>
                  </div>
                </div>
                <!-- END PANEL -->
              </div>
			  
			  <div class="col-lg-5 col-md-6">
                <!-- START PANEL -->
                <div class="panel panel-transparent">
                  <div class="panel-body">
                    <h3>Add Logos details</h3>
                    <p class="small hint-text m-t-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      <br> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
					  
					   <p class="small hint-text m-t-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      <br> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                  </div>
                </div>
                <!-- END PANEL -->
              </div>
            </div>
          </div>
          
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
