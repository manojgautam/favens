      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <div class="row">
              <div class="col-lg-12 col-md-12">
               
                <div class="panel panel-transparent">
				 <!-- START PANEL -->
				
                <?php if($this->session->flashdata('success')==true): ?>
				 <div class="message_block">
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
						</div>
					<?php endif;?>
				
                  <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/brands'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Brands Name</label>
                            <input type="text" class="form-control" name="bname" placeholder="Enter Brand Name" required>
                            
                          </div>
                        </div>
                      </div>
                      
                         <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Description</label>
                            <textarea class="form-control" name="bdetails" placeholder="Enter Description" required></textarea>
                         
                          </div>
                        </div>
                      </div>
                      

                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Logo</label>
                            <input type="file" class="form-control" name="userfile" id="img-prev" required>
                            <div class="imageprview"></div>
                          </div> 
                        </div>
                      </div>
       
                      <div class="clearfix"></div>
						 <div class="add_btn show_screen">
						     
                           <div class="checkbox check-success">
                        <input type="checkbox" id="brandshow" name="show_screen" value="1">
                        <label for="brandshow">Start On Show Screen</label>
                             </div>
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Brand" >
					  </div>
                    </form>
                  </div>
                </div>
                <!-- END PANEL -->
              </div>
            </div>
          </div>
          
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
