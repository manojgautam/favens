<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
    <!-- START PAGE CONTENT -->
    <div class="content ">
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Categories Details
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">

                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalSlideUpcate" class="btn btn-primary btn-cons"><i class="fa fa-plus"></i> Add New Categories</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <input type="text" id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                 <form method='post' action="<?php echo base_url();?>admin/stores/categories/" id="userform"><input type="submit" value='Delete' data-text="You will not be able to recover these records" class="del_event btn btn-danger submitallids"  style='display:none' name='bulk_delt'>
              <input type="hidden" name="deleteallhidden" id="deleteallhidden" value="0">
              <input type="hidden" name="deleteallids" id="deleteallids">
               <input type="hidden" name="submitdone" id="submitdone" value="submitdone">
                <div class="panel-body">
                    <table class="table table-hover demo-table-search table-responsive-block tableclass" id="tableWithSearch">
                        <thead>
                            <tr>
                                <th>
                                 <?php if(count($categorlist)){ ?>
                                 <div class="checkbox check-success">                 
                                 <input type="checkbox" name="deleteall[]" class="checkAll"  id="checkbox0"> 
                                 <label for="checkbox0"></label>
                                 </div>
                                 <?php } ?>
                                </th>
			        <th>Categories Name</th>
                                <th>Store Name</th>
                                <th>Status</th>
                                <th>Added By</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($categorlist as $row):?>
                                <tr>
                                     <td>
                                    <div class="checkbox check-success">                 
                                    <input type='checkbox' class='checkdata' value="<?php echo $row->c_id;?>" name='checked_events[]' id="checkbox<?php echo $row->c_id;?>"> 
                                    <label for="checkbox<?php echo $row->c_id;?>"></label>
                                     </div>
                                   </td>
                                    <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->c_name;?>
                                        </p>
                                    </td>
                                     <td class="v-align-middle semi-bold">
                                       <p>  <?php if( $row->c_store  != 0)
                                         { echo $row->s_name; }
                                         else {  echo "Default";  }?>
                                        </p>
                                    </td>
                                    
								  <td class="v-align-middle">
				                <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url(); ?>">
				     	      
					           <div class="statuschangecat<?php echo $row->c_id; ?>">
					               
					                <a class="updatestatuscat" theid="<?php echo $row->c_id ; ?>" thestatus="<?php echo $row->c_status ; ?>"  href="javascript:;" id="statcat<?php echo $row->c_id ;?>">
					           <div class="statuschangecat<?php echo $row->c_id; ?>">
				         	  <?php if($row->c_status == "1") {?><span class="label label-success">Active</span><?php } else { ?><span class="label label-danger">Deactive</span><?php } ?>
				            	 </div>
			      	        	 </a>
				         	  
				            	 </div>
			      	        	 </a>
				               	 </td>
									 <td class="v-align-middle semi-bold">
                                        <p>
                                            <?php echo $row->c_added_by;?>
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                                                           <!-- Indicates a dangerous or potentially negative action -->
                                       <a data-text="You will not be able to recover this category" data-toggle="tooltip" data-placement="top" data-original-title="Delete Category"  data-url="<?php echo base_url('admin/stores/categoriesdelete/'.$row->c_id);?>" href="javascript:void(0);" class="btn btn-danger  m-b-10 deleterecord"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            
                                <?php endforeach; ?>
                                
                                   
                        </tbody>
                    </table>
                </div>
                </form>
            </div>
       
  
    <!-- END PANEL -->
</div>

</div>
<!-- START PAGE CONTENT -->
</div>
<!-- START PAGE CONTENT WRAPPER -->

			<!-- Modal -->
    <div id="modalSlideUpcate" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
	
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Cateories </h4>
      </div>
      <div class="modal-body">
       <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/stores/categories'); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Category Name</label>
                            <input type="text" class="form-control" name="cname" placeholder="Enter Logo Name" required>
                            
                          </div>
                        </div>
                      </div>
                                            
                      <div class="clearfix"></div>
                     
                      <input type="submit" name="submit" class="btn btn-primary" value="Add Category" >
                    </form>
      </div>
         </div>

  </div>
</div>
            <!-- /.modal-dialog -->
