      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <div class="row">
              <div class="col-lg-12 col-md-12 ">
                <!-- START PANEL -->
              
                <div class="panel panel-transparent">
                      <?php if($this->session->flashdata('success')==true): ?>
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
      
        <?php endif;?>
                  <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" autocomplete="off" runat="server" method="post" action="<?php echo base_url('admin/brands/updatebrand/'.$brandedit[0]->b_id); ?>">
                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Brands Name</label>
                            <input type="text" class="form-control" name="bname" value="<?php echo $brandedit[0]->b_name ;?>" required>
                            
                          </div>
                        </div>
                      </div>
                      
                        <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Description</label>
                            <textarea class="form-control" name="bdetails" rows="10" required><?php echo $brandedit[0]->b_details ;?></textarea>
                            
                          </div>
                        </div>
                      </div>
                      
                              
                

                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default ">
                            <label>Logo</label>
                            <input type="file" class="form-control" name="userfile" id="imgInp">
                            <img id="blah" alt="your image" style="width:150px;height:100px;" src="<?php if($brandedit[0]->b_logo == "") echo base_url('uploads/no-image.jpg'); else echo  base_url('uploads/'.$brandedit[0]->b_logo);?>">
                            
                          </div> 
                        </div>
                      </div>
                      
                      <input type="hidden" name="oldimage" value="<?php echo $brandedit[0]->b_logo;?>"> 
       
                      <div class="clearfix"></div>
                      <div class="add_btn show_screen">
                             <div class="checkbox check-success">
                               
                               <?php  $show= $brandedit[0]->b_show_screen; ?>	
                        <input type="checkbox" <?php if($show == 1){ echo "checked"; }?> id="size_s" name="show_screen" value="1">
                        <label for="size_s">Start On Show Screen</label>
                             </div>
                      <input type="submit" name="submit" class="btn btn-primary" value="Update Brand" >
                      </div>
                    </form>
                  </div>
                </div>
                <!-- END PANEL -->
              </div>
	
            </div>
          </div>
          
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
