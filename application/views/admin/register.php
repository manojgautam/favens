      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <div class="row">
              <div class="col-lg-12 col-md-12">
			  <div class="panel panel-transparent">
                <!-- START PANEL -->
                <?php if($this->session->flashdata('success')==true): ?>
				
				<div class="message_block">
                  <div class="alert alert-success" role="alert">
                      <button class="close" data-dismiss="alert"></button>
                      <strong>Success: </strong><?php echo $this->session->flashdata('success'); ?>
                    </div>
                    </div>
        <?php endif;?>
                <div class="panel panel-transparent">
                  <div class="panel-body">
                    <form id="form-personal" role="form" enctype="multipart/form-data" runat="server" autocomplete="off" method="post" action="<?php echo base_url('admin/register'); ?>">
          <?php
        if (validation_errors()) {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-close"></i></button>
                <b><?php echo 'Alert'; ?>!</b><?php echo validation_errors(); ?>
            </div>

        <?php } ?>             

                       <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Username</label>
                            <input type="text" class="form-control" name="username" placeholder="Enter Your Name" value="<?php echo set_value('username'); ?>" required>
                            
                          </div>
                        </div>
                      </div>
                                            <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter Your Email" value="<?php echo set_value('email'); ?>" required>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group form-group-default required">
                            <label>Password</label>
                 <input type="password" class="form-control" name="tpassword" placeholder="Minimum of 4 characters." value="<?php echo set_value('tpassword'); ?>" required>
                          </div>
                        </div>
                      </div>

                        <div class="row">
                        <div class="col-sm-12">
					<div class="form-group form-group-default required">
                            <label>Image</label>
                            <input type="file" class="form-control" name="userfile" id="imgInp" required>
                            <img id="blah" alt="your image" style="width:100px;height:100px;" src="<?php echo base_url();?>uploads/default.png"/>
                          </div> 
                        </div>
                      </div>
       
                      <div class="clearfix"></div>
                     
					  <div class="add_btn">
                      <input type="submit" name="submit" class="btn btn-primary" value="Add account" >
					  </div>
                    </form>
                  </div>
                </div>
                <!-- END PANEL -->
              </div>
              </div>
			  
            </div>
          </div>
          
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
