</div> 
    <input type="hidden" id="thebaseurl" value="<?php echo base_url();?>">
 <!-- BEGIN VENDOR JS -->
    <script src="<?php echo base_url('assets/plugins/pace/pace.min.js')?>" type="text/javascript"></script>
   

    <script src="<?php echo base_url('assets/plugins/modernizr.custom.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrapv3/js/bootstrap.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/jquery/jquery-easy.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-unveil/jquery.unveil.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-bez/jquery.bez.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-ios-list/jquery.ioslist.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-actual/jquery.actual.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/select2/js/select2.full.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/classie/classie.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/switchery/js/switchery.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/lib/d3.v3.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/nv.d3.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/src/utils.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/src/tooltip.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/src/interactiveLayer.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/src/models/axis.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/src/models/line.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/nvd3/src/models/lineWithFocusChart.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/mapplic/js/hammer.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/mapplic/js/jquery.mousewheel.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/mapplic/js/mapplic.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/rickshaw/rickshaw.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-metrojs/MetroJs.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/skycons/skycons.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')?>" type="text/javascript"></script>	
    <script src="<?php echo base_url('assets/js/moment.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')?>"></script>
	<script src="<?php echo base_url('assets/plugins/jquery-validation/js/jquery.validate.min.js')?>" type="text/javascript"></script>		 	
	<script src="<?php echo base_url('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/plugins/datatables-responsive/js/datatables.responsive.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/plugins/datatables-responsive/js/lodash.min.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/js/datatables.js')?>" type="text/javascript"></script>	
    <!-- END VENDOR JS -->		
	<script src="<?php echo base_url('assets/plugins/dropzone/dropzone.min.js')?>" type="text/javascript"></script>	
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo base_url('assets/js/pages.min.js')?>"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->	
     <script src="<?php echo base_url('assets/js/bootstrap-colorpicker.min.js')?>" type="text/javascript"></script>	
    	
    <script src="<?php echo base_url('assets/js/modal.js')?>" type="text/javascript"></script>	
    <script src="<?php echo base_url('assets/plugins/switchery/js/switchery.min.js')?>" type="text/javascript"></script>	
    <script src="<?php echo base_url('assets/plugins/jquery-metrojs/MetroJs.min.js')?>" type="text/javascript"></script>	
    <script src="<?php echo base_url('assets/plugins/imagesloaded/imagesloaded.pkgd.min.js')?>" type="text/javascript"></script>	
    <script src="<?php echo base_url('assets/plugins/jquery-isotope/isotope.pkgd.min.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/js/gallery.js')?>" type="text/javascript"></script>	
	<script src="<?php echo base_url('assets/js/modal.js')?>" type="text/javascript"></script>	
    <script src="<?php echo base_url('assets/js/scripts.js')?>" type="text/javascript"></script>   
    <script src="<?php echo base_url('assets/js/form_layouts.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/form_elements.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/summernote/js/summernote.min.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/jquery.flexslider-min.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/Chart.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/custom.js')?>" type="text/javascript"></script>
    <script src="<?php //echo base_url('assets/js/footerscript.js')?>" type="text/javascript"></script>
       


<!-- DElete confirmation pop-up  -->   
    <script type="text/javascript">
     $(document).ready(function(){
     $("#datepicker-component").datepicker();

     $("body").on('click','.deleterecord',function(){
  var theurl=$(this).attr('data-url');
   var thetext=$(this).attr('data-text');
   //alert(thetext);
   var newtext="";
   if(typeof thetext  === "undefined"){
      newtext = "You will not be able to recover this record";
   }
   else{
        newtext=thetext;
   }
swal({  
 title: "Are you sure?",  
 text: newtext,  
 type: "warning",  
 showCancelButton: true,  
 confirmButtonColor: "#DD6B55",  
 confirmButtonText: "Yes, delete it!", 
 closeOnConfirm: false }, function(){
window.location.href = theurl; 
});
   
    });
    });
   </script>
   <!-- end DElete confirmation pop-up -->   

<!-- check box delete all functionality --> 
<script type="text/javascript">
$(document).ready(function(){
 var IDs = [];
$('body').on('click','.checkdata',function(){
$('#deleteallhidden').val('0');
$('.checkAll').prop('checked',false);

 var emailvalue=$(this).val();

  if($(this).is(":checked")){
           if(emailvalue!=''){
           IDs.push($(this).val());}
      }
      else{
           if(emailvalue!=''){
           index = IDs.indexOf($(this).val());
           IDs.splice(index, 1);}
       }

if($('.checkdata:checked').length > 0){
$(".del_event").css('display','block');


}
else{
$(".del_event").css('display','none');

}
});

$('body').on('click','.submitallids',function(){
var alllids=IDs;
$('#deleteallids').val(alllids);
});

});
</script>
<script type="text/javascript">
     $(document).ready(function(){
    

 $("body").on('click','.submitallids',function(e){
   

e.preventDefault();
  
   var thetext=$(this).attr('data-text');
 
   var newtext="";
   if(typeof thetext  === "undefined"){
      newtext = "You will not be able to recover this record";
   }
   else{
        newtext=thetext;
   }
swal({  
 title: "Are you sure?",  
 text: newtext,  
 type: "warning",  
 showCancelButton: true,  
 confirmButtonColor: "#DD6B55",  
 confirmButtonText: "Yes, delete it!", 
 closeOnConfirm: false }, function(){
   $('form').submit();
});
   
    });
    });
</script>
<!-- end check box delete all functionality --> 

<!-- select store in add product page --> 
   <script>
        $(document).ready(function(){
var theselected=$(".selstore option:selected").text();
 $('.storediv').children().children().children('span').html(theselected);
  });
   </script>
<!-- end select store in add product page --> 

<!-- select brand in add product page --> 
   <script>
        $(document).ready(function(){
var theselecte=$(".selbrand option:selected").text();
 $('.branddiv').children().children().children('span').html(theselecte);
  });
   </script>
<!-- select brand in add product page --> 


 
<!-- delete multiple images add product page --> 
 <script type="text/javascript">
     $(document).ready(function(){
 
      $("body").on('click','.delmultimages',function(e) {
   var id=$(this).attr('data-image');
e.preventDefault();
swal({  
 title: "Are you sure?",  
 text: "Do you really want to delete this image",  
 type: "warning",  
 showCancelButton: true,  
 confirmButtonColor: "#DD6B55",  
 confirmButtonText: "Yes, delete it!", 
 closeOnConfirm: false }, function(){
$.ajax({
          url: "<?php echo base_url();?>admin/products/deleteimages",
          type: "POST",
          data:{imgid:id},
          success: function(data)
        {$('#delmul'+id).remove();
         $('.delmultimages'+id).remove();
          $('.cancel').click();
        },
        error: function() {}           
     });

});
   
    });
    });
   </script>
<!-- end delete multiple images add product page --> 


 <script type="text/javascript">
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;
            $(placeToInsertImagePreview).html('');
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery2');
    });
});
   </script>


 <script type="text/javascript">
$(function() {
    // Single images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    $(placeToInsertImagePreview).html($($.parseHTML('<img>')).attr('src', event.target.result))
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#img-prev').on('change', function() {
        imagesPreview(this, 'div.imageprview');
    });
   $('#img-prev1').on('change', function() {
        imagesPreview(this, 'div.imageprview1');
    });
   $('#img-prev2').on('change', function() {
        imagesPreview(this, 'div.imageprview2');
    });
});
   </script>
    <!-- END PAGE LEVEL JS -->

 <!-- store category add product  -->
	
   <script type="text/javascript">
     jQuery(document).ready(function(){
    $('#categ').change(function(){
   var id=$(this).val();
    $('#storeid').val(id);
    });
    });
   </script>

<script type="text/javascript">
    
    $('body').on('change','#prodselopt',function(){
    var id=$(this).val();
     $.ajax({
          url: "<?php echo base_url();?>admin/products/getcategories",
          type: "POST",
          data:{id:id},
          success: function(data)
        { $('.ajaxcateg').html(data);
         $("#multi").val(["Jim", "Lucy"]).select2();
        },
        error: function() {}           
     });
     });
    
   </script>
 <!-- end store category add product  -->

 <!-- category multi select edit product  -->
<script type="text/javascript">
     jQuery(document).ready(function(){
var pageid=$('#editpage').val();

if(pageid === undefined ){}
else{

    var str=$('#hiddencats').val();
    var res = str.split(",");
    $("#multi").val(res).select2();

}
     });
   </script>
 <!-- end category multi select edit product  -->


 <!--add brand on add product  -->
<script>
$(document).ready(function (e) {
  $(".addbrandForm").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
          url: "<?php echo base_url();?>admin/brands/ajaxadd",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      success: function(data)
        {var newdata=data.split("sepratortext");
        $('.newbrandadddiv').html(newdata[0]);
        $('.newstoreadddiv').html(newdata[1]);
        $('.close').click();
        },
        error: function() 
        {
        }           
     });
  }));
});
</script>
 <!--end add brand on add product  -->

 <!--add store on add product  -->

<script>
$(document).ready(function (e) {
  $(".addstoreForm").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
          url: "<?php echo base_url();?>admin/stores/ajaxadd",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      success: function(data)
        {var newdata=data.split("sepratortext");
        $('.newbrandadddiv').html(newdata[0]);
        $('.newstoreadddiv').html(newdata[1]);
        $('.close').click();
        },
        error: function() 
        {
        }           
     });
  }));
});
</script>
    <!--end add store on add product  -->

 <!--slider on view product  -->
  <script>
(function($) {
            $(window).ready(function(){
                $('.flexslider').flexslider({
                    controlNav: "thumbnails",
                    start: function(slider){
                        $('body').removeClass('loading');
                    }
                });
            });
        })(jQuery);
	    </script>
 <!--end slider on view product  -->
		
<script>
$(document).ready(function(){
    $('[data-tooltip="true"]').tooltip(); 
});
</script>

 <!--dashboard charts  -->
<?php if(isset($productcount) || isset($brandcount) || isset($storecount) || isset($todayusercount) || isset($todayproductcount) || isset($usercount)){?>
	  <script>
   $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      labels: ["Products", "Brands", "Stores", "Users"],
      datasets: [
        
        {
          label: "user",
          fillColor: "rgb(149,223,216)",
          strokeColor: "rgb(149,223,216)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgb(149,223,216)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgb(149,223,216)",
          data: [<?php if(isset($productcount)){echo $productcount; }?>,<?php if(isset($brandcount)){echo $brandcount;} ?>, <?php if(isset($storecount)){echo $storecount; }?>,<?php if(isset($usercount)){echo $usercount; }?>]
        }
      ]
    };
       var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);

        var areaChartCanvas = $("#areaChart1").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      labels: ["User", "Product"],
      datasets: [
        {
          label: "group",
          fillColor: "rgb(221,221,221)",
          strokeColor: "rgb(221,221,221)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgb(221,221,221)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgb(221,221,221)",
          data: [<?php if(isset($todayusercount)){echo $todayusercount; } ?>,<?php if(isset($todayproductcount)){echo $todayproductcount; }?>]
        },
        
      ]
    };
       var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);
   });
</script>

<?php } ?>
 <!--end dashboard charts  -->


 <!--forget password -->
<script type="text/javascript">
    $(document).ready(function(){
       $('#forget_submit').click(function(e){
e.preventDefault();
var useremail=$('#forgetemail').val().trim();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/;

if(!(regex.test(useremail)))
{$('#err-forgot').text("The email field must contain a vaild email.");
return false;
}

$('#err-forgot').text("");

   
    $.ajax({
               url:'<?php echo base_url();?>admin/login/forgot',
               type:'POST',
                data:{email:useremail},
               success:function(data){
                   if(data=='not match'){$('#err-forgot').text("The email is not registered with us.");}
                   else{
                   $('#success_message').text("Please check your mail to reset password.");
                   $("#success_message").delay(4000).fadeOut();
                   }
               },
                error(error){console.log(error);}
           }); 
       });
    });
    
</script>
 <!--end forget password -->

 <!--color picker -->
<script type="text/javascript">
    $(function() {
       // $('.my-colorpicker2').colorpicker();
    });
    
    $(document).ready(function(){
        var i=1;
        $('.add-color').click(function(){
          $('.color-field').append('<div class="form-group col-sm-3 no-padding addons removediv'+i+'"><div class="input-group colorpicker-element"><input type="color" class="form-control" name="color[]"><div class="deletecolor"><a href="javascript:void(0)" attrid="'+i+'" class="btn btn-white btn-xs btn-mini bold fs-14 delete-field">x</a></div></div></div>');  
         //$('.my-colorpicker2').colorpicker();
            i=i+1;
        });
        
        $('body').on('click','.delete-field',function(){
        
          var id =  $(this).attr('attrid');
        $('.removediv'+id).remove();
        });
       });
    
</script>
 <!--end color picker -->
<script>
$(document).ready(function(){
$('#forgot-pass').click(function(){
$('#forgetemail').val('');
$('#err-forgot').text('');
$('#success_message').text('');
});});
</script>

<script> $(window).load(function() {         $('#preloader').fadeOut('slow', function() {            $(this).hide();  }); }); </script>
 <script>
           $('#daterangepicker').daterangepicker({
            timePicker: true,
            timePickerIncrement: 5,
            format: 'MM/DD/YYYY h:mm A'
        }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        </script>
<script src="<?php echo base_url('assets/js/dashboard.js')?>" type="text/javascript"></script>
    
  </body>
</html>
