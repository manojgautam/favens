<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php if(isset($title)) echo $title; else echo "Favens";?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="assets/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/ico/76.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/ico/120.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/ico/152.png"')?>>
    <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets/ico/favicon.png') ?> "/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?php echo base_url('assets/plugins/pace/pace-theme-flash.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/bootstrapv3/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
	  <link href="<?php echo base_url('assets/plugins/dropzone/css/dropzone.css')?>" rel="stylesheet" type="text/css" media="screen" />
	
	
	
	<link class="main-stylesheet" href="<?php echo base_url('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
	<link class="main-stylesheet" href="<?php echo base_url('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')?>" rel="stylesheet" type="text/css" />
	<link class="main-stylesheet" href="<?php echo base_url('assets/plugins/datatables-responsive/css/datatables.responsive.css')?>" rel="stylesheet" type="text/css" />
	
	
    <link href="<?php echo base_url('assets/plugins/nvd3/nv.d3.min.css')?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/plugins/mapplic/css/mapplic.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/rickshaw/rickshaw.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/datepicker3.css')?>" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url('assets/plugins/jquery-metrojs/MetroJs.css')?>" rel="stylesheet" type="text/css" media="screen" />
    
   
	
	<link href="<?php echo base_url('assets/css/pages-icons.css')?>" rel="stylesheet" type="text/css">
	 <link class="custom-stylesheet" href="<?php echo base_url('assets/css/sweetalert.css')?>" rel="stylesheet" type="text/css" />
	 <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css')?>" rel="stylesheet" type="text/css">
	 <link href="<?php echo base_url('assets/plugins/jquery-metrojs/MetroJs.css')?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/css/bootstrap-colorpicker.min.css')?>" rel="stylesheet" type="text/css" />
     
	
	 <link class="main-stylesheet" href="<?php echo base_url('assets/css/pages.css')?>" rel="stylesheet" type="text/css" />
	   <link href="<?php echo base_url('assets/plugins/summernote/css/summernote.css')?>" rel="stylesheet" type="text/css" media="screen">
	   
	   
    <link href="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet" type="text/css" media="screen">
	  <link class="custom-stylesheet" href="<?php echo base_url('assets/css/flexslider.css')?>" rel="stylesheet" type="text/css" />
	  <link class="custom-stylesheet" href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css" />
     
          <script src="<?php echo base_url('assets/plugins/jquery/jquery-1.11.1.min.js')?>" type="text/javascript"></script>

    <!--[if lte IE 9]>
	<link href="<?php echo base_url('assets/plugins/codrops-dialogFx/dialog.ie.css')?>" rel="stylesheet" type="text/css" media="screen" />

	<![endif]-->
  </head>
