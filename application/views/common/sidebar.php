  <body class="fixed-header dashboard  windows desktop pace-done sidebar-visible menu-pin">
      
        <div id="preloader" >
<img id="loading-image" src="<?php echo base_url('assets/img/cube.gif')?>">
    </div>
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
	
	<!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="<?php echo base_url('assets/img/favens_logo_white.png')?>" alt="logo" class="brand" data-src="<?php echo base_url('assets/img/favens_logo_white.png')?>" data-src-retina="<?php echo base_url('assets/img/favens_logo_white.png')?>" width="150">
        <div class="sidebar-header-controls">
          <!-- <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i> -->
          <!-- </button> -->
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
	    <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-30 <?php if(isset($dashboard)){echo 'active';}?>">
			<a class="detailed" href="<?php echo base_url('admin/dashboard');?>">
              <span class="title">Dashboard</span>
              <!-- <span class="details">12 New Updates</span> -->
            </a>
            <span class="<?php if(isset($dashboard)){echo 'bg-success';}?> icon-thumbnail"><i class="pg-home"></i></span>
          </li>
          <li class="<?php if(isset($padd) || isset($plist) || isset($parchive)){echo 'open active';}?>">
            <a href="javascript:;"><span class="title">Products</span>
            <span class=" arrow"></span></a>
            <span class="<?php if(isset($padd) || isset($plist) || isset($parchive)){echo 'bg-success';}?> icon-thumbnail"><i class="pg-social"></i></span>
            <ul class="sub-menu">
              <li class="<?php if(isset($plist)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/products/productslist'); ?>">All Products</a>
				<span class="icon-thumbnail">AP</span>
              </li>
              <li class="<?php if(isset($padd)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/products'); ?>">Add Products</a>
				<span class="icon-thumbnail">AP</span>
              </li>
              <li class="<?php if(isset($parchive)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/products/archiveproductslist'); ?>">Archived Products</a>
               <span class="icon-thumbnail">AP</span>
              </li>
            </ul>
          </li>
		  
		  <li class="<?php if(isset($oadd) || isset($oactive) || isset($oarchive) || isset($oexpired)){echo 'open active';}?>">
            <a href="javascript:;"><span class="title">Offers</span>
            <span class=" arrow"></span></a>
            <span class="<?php if(isset($oadd) || isset($oactive) || isset($oarchive) || isset($oexpired)){echo 'bg-success';}?> icon-thumbnail"><i class="pg-layouts"></i></span>
            <ul class="sub-menu">
              <li class="<?php if(isset($oactive)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/offers/activeofferslist'); ?>">All Active Offers</a>
				<span class="icon-thumbnail">AO</span>
              </li>
              <li class="<?php if(isset($oadd)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/offers'); ?>">Add Offer</a>
				<span class="icon-thumbnail">AO</span>
              </li>
              <li class="<?php if(isset($oarchive)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/offers/archiveofferslist'); ?>">Archived Offers</a>
				<span class="icon-thumbnail">AO</span>
              </li>
			  <li class="<?php if(isset($oexpired)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/offers/expireoffers'); ?>">Expired Offers</a>
				<span class="icon-thumbnail">EO</span>
              </li>
            </ul>
          </li>
		  
		  <li class="<?php if(isset($abrand) || isset($lbrand) || isset($archbrand)){echo 'open active';}?>">
            <a href="javascript:;"><span class="title">Brands</span>
            <span class=" arrow"></span></a>
            <span class="<?php if(isset($abrand) || isset($lbrand) || isset($archbrand)){echo 'bg-success';}?> icon-thumbnail"><i class="pg-layouts2"></i></span>
            <ul class="sub-menu">
              <li class="<?php if(isset($lbrand)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/brands/brandslist'); ?>">All Brands</a>
				<span class="icon-thumbnail">AB</span>
              </li>
              <li class="<?php if(isset($abrand)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/brands'); ?>">Add Brands</a>
				<span class="icon-thumbnail">AB</span>
              </li>
              <li class="<?php if(isset($archbrand)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/brands/archivebrandslist'); ?>">Archived Brands</a>
				<span class="icon-thumbnail">AB</span>
              </li>
             
            </ul>
          </li>
		  
		  <li class="<?php if(isset($astore) || isset($lstore) || isset($archstore) || isset($cstore) ){echo 'open active';}?>">
            <a href="javascript:;"><span class="title">Stores</span>
            <span class=" arrow"></span></a>
            <span class="<?php if(isset($astore) || isset($lstore) || isset($archstore)){echo 'bg-success';}?> icon-thumbnail"><i class="fa fa-window-restore"></i></span>
            <ul class="sub-menu">
              <li class="<?php if(isset($lstore)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/stores/storeslist'); ?>">All Stores</a>
				<span class="icon-thumbnail">AS</span>
              </li>
              <li class="<?php if(isset($astore)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/stores'); ?>">Add Stores</a>
				<span class="icon-thumbnail">AS</span>
              </li>
              <li class="<?php if(isset($archstore)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/stores/archivestoreslist'); ?>">Archived Stores</a>
				<span class="icon-thumbnail">AS</span>
              </li>
               <li class="<?php if(isset($cstore)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/stores/categories/'); ?>">Categories</a>
				<span class="icon-thumbnail">AC</span>
              </li>
            </ul>
          </li>
		  
		  <li class="<?php if(isset($ladmin) || isset($aadmin)){echo 'open active';}?>">
            <a href="javascript:;"><span class="title">Admins</span>
            <span class=" arrow"></span></a>
            <span class="<?php if(isset($ladmin) || isset($aadmin)){echo 'bg-success';}?> icon-thumbnail"><i class="fa fa-user-circle-o"></i></span>
            <ul class="sub-menu">
              <li class="<?php if(isset($ladmin)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/register/adminlist');?>">All Admins</a>
				<span class="icon-thumbnail">AD</span>
              </li>
              <li class="<?php if(isset($aadmin)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/register'); ?>">Add Admins</a>
				<span class="icon-thumbnail">AD</span>
              </li>
            </ul>
          </li>
		  
		  <li class="<?php if(isset($users)){echo 'active';}?>">
            <a href="<?php echo base_url('admin/users'); ?>">
              <span class="title">Users</span>
            </a>
            <span class="<?php if(isset($users)){echo 'bg-success';}?> icon-thumbnail"><i class="fa fa-user-o"></i></span>
          </li>
		  
		  
		  <li class="<?php if(isset($lldmin) || isset($aldmin)){echo 'open active';}?>">
            <a href="javascript:;"><span class="title">Logos</span>
            <span class=" arrow"></span></a>
            <span class="<?php if(isset($lldmin) || isset($aldmin)){echo 'bg-success';}?> icon-thumbnail"><i class="pg-menu_lv"></i></span>
            <ul class="sub-menu">
              <li class="<?php if(isset($lldmin)){echo 'active';}?>">
                <a href="<?php echo base_url('admin/logos/logoslist');?>">All Logos</a>
				<span class="icon-thumbnail">AL</span>
              </li>

             </ul>
           </li>
		  
		   <li class="<?php if(isset($setting)){echo 'active';}?>">
            <a href="<?php echo base_url('admin/settings');?>">
              <span class="title">Settings</span>
            </a>
            <span class="icon-thumbnail"><i class="pg-settings_small"></i></span>
          </li>
		  
		  
		  
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
	
	    <div class="page-container ">
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
          <div class="pull-center hidden-md hidden-lg">
            <div class="header-inner">
              <div class="brand inline">
                <img src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" alt="logo" data-src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" data-src-retina="<?php echo base_url('assets/img/favens_logo_blue.png')?>" width="78">
              </div>
            </div>
          </div>
          <!-- RIGHT SIDE -->
          <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
                <span class="icon-set menu-hambuger-plus"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm">
          <div class="header-inner">
            <div class="brand inline">
              <img src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" alt="logo" data-src="<?php echo base_url('assets/img/favens_logo_blue.png')?>" data-src-retina="<?php echo base_url('assets/img/favens_logo_blue.png')?>" width="78">
            </div>
            <!-- START NOTIFICATION LIST -->
            <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
              <li class="p-r-15 inline">
                <div class="dropdown">
                  <a href="javascript:;" id="notification-center" class="icon-set globe-fill" data-toggle="dropdown">
                    <span class="bubble"></span>
                  </a>
                  <!-- START Notification Dropdown -->
                  <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                    <!-- START Notification -->
                    <div class="notification-panel">
                      <!-- START Notification Body-->
                         <div class="notification-body scrollable">
                     <?php if(isset($notificationdata)){ foreach($notificationdata as $notify): ?>
                     <div class="notification-item  clearfix">
  <div class="heading">
	<div class="text-complete noti_text pull-left">
	  <span class="fs-12 m-l-10"><?php if ($notify->n_type == 1){ echo "Product"; }  if ($notify->n_type == 2){ echo "Brand"; } if ($notify->n_type == 3){ echo "Store"; }?></span>
	  <span class="text-complete"> <?php echo $notify->n_name; ?></span>
	  <span class="">Added by </span>
	  <span class="text-complete"><?php echo $notify->n_added_by; ?></span>
	</div>
	<span class="pull-right time"><?php echo $notify->created_at; ?></span>
  </div>
  <!-- START Notification Item Right Side-->
  <div class="option">
	<a href="#" class="mark"></a>
  </div>
  <!-- END Notification Item Right Side-->
</div>
                     
                     <?php endforeach; } ?>
                     </div>
                      <!-- END Notification Body-->
                      <!-- START Notification Footer-->
                      <div class="notification-footer text-center">
                        <a href="<?php echo base_url();?>admin/notification" class="">Read all notifications</a>
                        <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                          <i class="pg-refresh_new"></i>
                        </a>
                      </div>
                      <!-- START Notification Footer-->
                    </div>
                    <!-- END Notification -->
                  </div>
                  <!-- END Notification Dropdown -->
                </div>
              </li>
             
            </ul>
            <!-- END NOTIFICATIONS LIST -->
            </div>
        </div>
       
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="semi-bold"><?php echo $details[0]->name ;?></span> 
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="<?php if($details[0]->image == "") echo base_url('uploads/default.png'); else echo  base_url('uploads/'.$details[0]->image);?>" alt="" width="32" height="32">
            </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li><a href="<?php echo base_url('admin/settings'); ?>"><i class="pg-settings_small"></i> Settings</a>
                </li>
                  <li class="bg-master-lighter">
                  <a href="<?php echo base_url('admin/login/logout'); ?>" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
	
