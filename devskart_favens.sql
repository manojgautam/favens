-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 21, 2017 at 12:10 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devskart_favens`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(40) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `image` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for admin and 0 for superadmin',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `image`, `status`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '26dc318942685872cf79c5eb96c9bb13', 'RTXkq628JHOViNKhzogC351spbYG9ZrPydMAeanU0DWctLEjmv.jpg', 1, 1, '2017-06-28 13:30:49', '2017-06-29 18:20:25'),
(4, 'prashant', 'prashant.techindustan@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', 1, 1, '2017-06-07 02:26:19', '2017-07-03 10:10:05'),
(6, 'Favens', 'favens@gmail.com', 'b8013862067dd746ee54d5ce4d7c18b7', 'Mg9i7KNdmeonfCxuBJbRUtZakLhW2vG0FrlP5p31.jpg', 1, 1, '2017-06-07 17:49:18', '0000-00-00 00:00:00'),
(13, 'favens', 'admin.favens@gmail.com', '95eb700d46278845481bab92fabc3af8', '', 1, 0, '2017-06-14 05:29:49', '2017-06-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `b_id` int(11) NOT NULL,
  `b_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `b_logo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `b_details` text CHARACTER SET utf8 NOT NULL,
  `b_p_count` smallint(5) UNSIGNED NOT NULL,
  `b_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `b_show_screen` tinyint(1) NOT NULL COMMENT '1 for show 0 for hide',
  `b_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `b_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archived 0 for not archive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`b_id`, `b_name`, `b_logo`, `b_details`, `b_p_count`, `b_status`, `b_show_screen`, `b_added_by`, `b_archive`, `created_at`, `updated_at`) VALUES
(7, 'Nike', 'xH5Jh6pNGYQiBgDt9f7yOLzuZraU1XemW8qnFM0j.jpg', 'Nike, originally known as Blue Ribbon Sports (BRS), was founded by University of Oregon track athlete Phil Knight and his coach Bill Bowerman in January 1964. The company initially operated as a distributor for Japanese shoe maker Onitsuka Tiger (now ASICS), making most sales at track meets out of Knight\'s automobile.', 1, 1, 0, 'Admin', 0, '2017-06-15 20:53:06', '0000-00-00 00:00:00'),
(8, 'Adidas', '1COU0uWgZKEsF6VbL4vkDAMie2wQtXlzThGSxYNp.jpg', 'Adidas AG is a German multinational corporation, headquartered in Herzogenaurach, Germany, that designs and manufactures shoes, clothing and accessories. It is the largest sportswear manufacturer in Europe, and the second largest in the world', 0, 1, 0, 'Admin', 0, '2017-06-15 20:53:52', '0000-00-00 00:00:00'),
(9, 'Woodland', 'swKyOMZtpJ20QC6dv9eTxuiIDnYmcafbgXLA8G7B.jpg', 'Woodland\'s parent company, Aero Group, has been a well known name in the outdoor shoe industry since the early 50s. Founded in Quebec, Canada, it entered the Indian market in 1992. Before that, Aero Group was majorly exporting its leather shoes to Russia. ', 1, 1, 0, 'Admin', 0, '2017-06-15 20:54:58', '0000-00-00 00:00:00'),
(10, 'Puma', 'dxhVc4DGzSgmHRjvXULCf7BiAPtYe9TEKkubF3pQ.jpg', 'PUMA SE, branded as PUMA, is a German multinational company that designs and manufactures athletic and casual footwear, apparel and accessories, headquartered in Herzogenaurach, Germany. The company was founded in 1948 by Rudolf Dassler', 6, 1, 0, 'Admin', 0, '2017-06-15 20:55:25', '0000-00-00 00:00:00'),
(11, 'Tommy Hilfiger', 'obWRvFlD61IcsPjzXN8ryL7pGtZqwu5ng0kHefUChx2MiSmQAV.jpg', 'Chain featuring the designer\'s classic sportswear & accessories. Some focus on apparel for kids.', 4, 1, 0, 'Admin', 0, '2017-06-15 20:55:57', '2017-06-29 17:30:36'),
(28, 'Jack & Jone', 'LgX3zYqsNQOj978lfERwVMBCx0tUcPvWAoDreTk4.jpg', ' It is the largest sportswear manufacturer in Europe, and the second largest in the world', 4, 1, 0, 'Admin', 0, '2017-06-27 08:32:22', '2017-07-20 18:16:13'),
(42, 'brandnew', 'eUcLQfzCjb1u7WlBr4E0tRoaqxp8ZiDy29gKJNMYS5G6kHPmVA.jpg', 'dsfdfsf', 1, 1, 1, 'parshant', 0, '2017-07-03 06:35:49', '2017-07-21 12:08:56'),
(45, 'branddd', 'lVFq2OdXt5L9ChE8fRYAzvPwgu4UTDQkIZN6ySWcj13MBKGinx.jpg', 'dfgdf', 5, 1, 1, 'Admin', 0, '2017-07-03 11:31:45', '2017-07-20 18:13:23');

-- --------------------------------------------------------

--
-- Stand-in structure for view `brands_view`
-- (See below for the actual view)
--
CREATE TABLE `brands_view` (
`b_id` int(11)
,`b_name` varchar(100)
,`b_details` text
);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `c_id` int(11) NOT NULL,
  `c_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `c_store` tinyint(1) NOT NULL COMMENT '0-default category',
  `c_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive	',
  `c_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`c_id`, `c_name`, `c_store`, `c_status`, `c_added_by`, `created_at`) VALUES
(15, 'qwerty', 7, 1, 'Admin', '2017-06-21 01:50:22'),
(16, 'tees', 7, 1, 'Admin', '2017-06-21 01:50:32'),
(18, 'shirt', 8, 1, 'Admin', '2017-06-21 01:51:07'),
(27, 'jeans', 10, 1, 'Admin', '2017-06-21 18:44:39'),
(28, 'Shoes', 0, 1, 'Admin', '2017-06-22 19:59:45'),
(29, 'zoomato', 6, 1, 'Admin', '2017-06-22 21:05:07'),
(34, 'moon', 8, 1, 'Admin', '2017-06-22 21:17:29'),
(35, 'Tshirts', 0, 1, 'Admin', '2017-06-22 22:03:01');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency`) VALUES
(1, '$'),
(2, '€');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `l_id` int(11) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `l_logo` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`l_id`, `l_name`, `l_logo`, `created_at`) VALUES
(29, 'Ahlens City', 'A5bkvyJt4BRfjrze68xZTm0FGluWSnDVpoUw2QKX.jpg', '2017-07-13 09:47:50'),
(30, 'Store2', 'tskiOmD2XTQq560PHrGZog1yK87jaenYWMlbuBNL.jpg', '2017-07-13 09:49:30'),
(31, 'Store3', 'MnsGqvYFJtdyZD352Vk7xmbHKCi8ucrwa09AOLzj.jpg', '2017-07-13 09:50:02'),
(32, 'Store4', 'ob3yJwtUf1ErCD0GSX6QMxe2K4Vd7kFvapm8ZPq9.jpg', '2017-07-13 09:55:00'),
(33, 'Store5', 'iCuYXto6EfBF1yN4Gj0LzU5n7Q2mHSwPpdI3Thq9.jpg', '2017-07-13 09:55:51'),
(34, 'Store6', '9O4JZiNaEg8H2jDYVnqdhUzxmr6t0W7ukeBSIF3R.jpg', '2017-07-13 09:56:46'),
(35, 'Store7', 'WQ9DieZpsKvAYEbxTXgn3HjGISOFdCM2alo6zqJt.jpg', '2017-07-13 09:57:14'),
(36, 'Store8', 'cVupg9DPvGd6O5xM7EeZTjXz8aSiYhLrkKw3QBnf.jpg', '2017-07-13 09:58:00'),
(37, 'Store9', 'GbSr0N6RpA2nEgaOfVCQ1Bec7wovju3PDqzmZtUI.jpg', '2017-07-13 09:58:18'),
(38, 'New', 'JCyfMl5AiBPFE8GD9zs4XwoO2UIZavLVNdcTWxrY.jpg', '2017-07-20 08:57:16'),
(39, 'Test Logo', 'I3Do8GdqZizfvQ0rBTRsNFx67KXn1cW9UaVMkl45.jpg', '2017-07-20 08:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `n_id` int(11) NOT NULL,
  `n_type` tinyint(1) NOT NULL COMMENT '(1 for product,2 for brand, 3 for store)',
  `n_name` varchar(100) NOT NULL,
  `n_seen` tinyint(1) NOT NULL COMMENT '1 for read 0 for unread',
  `n_added_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `n_id`, `n_type`, `n_name`, `n_seen`, `n_added_by`, `created_at`) VALUES
(49, 56, 1, 'testing', 1, 'Admin', '2017-07-14 10:27:55'),
(50, 57, 1, 'hiii', 1, 'Admin', '2017-07-14 11:46:24'),
(51, 58, 1, 'zzz', 1, 'Admin', '2017-07-14 11:48:48'),
(52, 59, 1, 'szss', 1, 'Admin', '2017-07-14 11:49:26'),
(53, 63, 1, 'aaa', 1, 'Admin', '2017-07-18 12:03:02'),
(54, 65, 1, 'aaa', 1, 'Admin', '2017-07-18 13:02:29'),
(55, 66, 1, 'aaa', 1, 'Admin', '2017-07-18 13:06:31'),
(56, 67, 1, 'aaa', 1, 'Admin', '2017-07-18 13:08:04'),
(57, 68, 1, 'fhgh', 1, 'prashant', '2017-07-20 17:54:29'),
(58, 69, 1, 'hjkhk', 1, 'prashant', '2017-07-20 18:26:43');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `o_id` int(11) NOT NULL,
  `o_title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `o_detail` text CHARACTER SET utf8 NOT NULL,
  `o_date` date NOT NULL,
  `o_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `o_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `o_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archive 0 for no archive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`o_id`, `o_title`, `o_detail`, `o_date`, `o_status`, `o_added_by`, `o_archive`, `created_at`, `updated_at`) VALUES
(10, 'Minimum 40% off', 'On season Latest Casual Style', '2017-06-29', 1, 'Admin', 0, '2017-06-29 06:09:57', '2017-06-29 11:39:57'),
(11, '30% to 50% off', 'It\'s Now or Never', '2017-06-30', 1, 'Admin', 0, '2017-06-29 06:08:06', '0000-00-00 00:00:00'),
(12, 'Upto 60% off', 'Denims To love!', '2017-06-30', 1, 'Admin', 0, '2017-06-29 06:08:14', '0000-00-00 00:00:00'),
(13, '75% off', 'Only for Sports Shoe ', '2017-06-30', 1, 'Admin', 0, '2017-06-29 06:08:10', '0000-00-00 00:00:00'),
(14, 'Testing', 'lorem ipsum dolor sit', '2017-06-28', 1, 'Admin', 0, '2017-06-28 13:13:08', '2017-06-28 18:43:08'),
(16, 'testing', 'lorem ipsum dolor sit', '2017-06-23', 1, 'Admin', 1, '2017-06-21 14:11:50', '0000-00-00 00:00:00'),
(17, 'dummy', 'lorem ipsum dolor sit', '2017-06-29', 1, 'Admin', 1, '2017-06-29 06:09:45', '2017-06-29 11:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(11) NOT NULL,
  `p_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `p_currency` varchar(5) NOT NULL,
  `p_original_price` varchar(15) CHARACTER SET utf8 NOT NULL DEFAULT '$',
  `p_new_price` varchar(15) CHARACTER SET utf8 NOT NULL,
  `p_detail` text CHARACTER SET utf8 NOT NULL,
  `p_image` varchar(255) NOT NULL DEFAULT 'product_default.png',
  `p_brand` int(11) NOT NULL,
  `p_store` int(11) NOT NULL,
  `p_size` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_color` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `p_show_screen` tinyint(1) NOT NULL COMMENT '1 for visible 0 for hide',
  `p_added_by` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archive 0 for no archive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_name`, `p_currency`, `p_original_price`, `p_new_price`, `p_detail`, `p_image`, `p_brand`, `p_store`, `p_size`, `p_color`, `p_status`, `p_show_screen`, `p_added_by`, `p_archive`, `created_at`, `updated_at`) VALUES
(30, 'Tommy Hilfiger Navy & Red Laptop Backpack', '$', '2000', '1199', 'Navy blue, red and white laptop backpack One haul loop, has two padded and adjustable shoulder straps Two main zip compartments, one has a padded laptop sleeve secured with a tab and a Velcro closure A mesh stash pocket on ei', 'gfuJObjo2lLc1xkMr3V76GdqUZpKXW.jpg', 11, 10, 'M', '#ff0080,#000000,#0000ff', 1, 0, 'Admin', 0, '2017-06-28 11:19:16', '2017-07-07 14:26:42'),
(32, 'Tommy Hilfiger Men Blue Slim Fit Mid-Rise Clean Look Jeans', '$', '9999', '7777', 'Blue medium wash 5-pocket mid-rise jeans, clean look with light fade, has a button and zip closure, waistband with belt loops', 'Stg1RbOrw8BEC7ovcTFAVlhW4DPLKm.jpg', 11, 8, 'S,M,L,XL', '#ff0000,#004080,#000000', 1, 0, 'Admin', 0, '2017-06-28 13:01:10', '2017-07-07 14:29:13'),
(33, 'Nike Shoe', '$', '10,190', '8000', 'Designed to Move in Any Direction.\r\nA Revolution in Motion.', '4P0ZCon6mVrluLkfOth8NH1MpaeRzA.jpg', 7, 10, 'M,L,XL', '#800000,#00ffff', 1, 0, 'Admin', 0, '2017-06-15 21:26:22', '2017-07-07 14:30:01'),
(34, 'PUMA Black Tight Fit Tights', '$', '1499', '974', 'Black mid-rise three-fourth tights, has an elasticated waistband', 'tFyW6hiSHcAakCvgLXpMO591Ix2U0D.jpg', 10, 9, 'M,L', '#ff8040,#00ffff', 1, 0, 'Admin', 0, '2017-06-28 13:16:19', '2017-07-03 15:41:17'),
(40, 'Jacket Black Tight Fit ', '$', '6000', '5000', 'Blue medium wash 5-pocket mid-rise jeans, clean look with light fade, has a button and zip closure, waistband with belt loops', 'c29NTmOYkAesZtdL06QSfigHhrMuxw.jpg', 9, 6, 'XL,XXL', '#ff8000,#ff0080,#000000', 1, 0, 'Admin', 0, '2017-06-28 13:16:40', '2017-07-07 14:27:43'),
(43, 'abc', '$', '12345', '123', 'fdgdfg', 'Sxuy5mROYBcPAdiUM3fQ7krb8gzoTn.jpg', 28, 9, 'S,M,L', '#000080,#000000,#ff0000', 1, 0, 'Admin', 0, '2017-06-29 13:01:54', '2017-07-07 14:30:57'),
(44, 'Tommy', '$', '600', '500', 'best product', 'zRJdunkoqCbSm6281XsD04w5WNM3jc.jpg', 28, 10, 'M,L', '#ff0000,#0000a0', 1, 0, 'Admin', 0, '2017-06-29 13:49:27', '2017-07-03 15:47:50'),
(45, 'Trouser', '$', '1234', '12', 'fgjhghj', 'HYTVDIyZsjMg4xkhSFquiWOQpU7R3l.jpg', 28, 10, 'S,M', '#4d1313,#2057db,#14b543,#bb1717', 1, 0, 'parshant', 0, '2017-06-30 09:52:09', '2017-07-03 13:16:59'),
(49, 'Mobile', '€', '6005', '5000', 'best mobile', 'tIn5YPdhk7TXvU0ypN4S6CsLjl8WeM.jpg', 42, 8, 'S,M', '#000000,#c71c1c,#0d0883', 1, 0, 'prashant', 0, '2017-07-03 07:49:50', '2017-07-19 19:24:47'),
(56, 'testing', '$', '100000', '5000', 'hiii ', 'TnQHXm09F5YEywikZlo4CU3vs7xWSN.jpg', 28, 8, 'S,M,L', '#00ffff,#ff80c0,#1d5cce', 1, 0, 'prashant', 0, '2017-07-14 04:57:55', '2017-07-19 19:15:31'),
(57, 'hiii', '$', '500', '', 'fyguhijokp ', '0YLWxJngkD8T7GBb32deaUNXfyHZI9.jpg', 10, 9, 'S,M', '#000000', 1, 0, 'Admin', 1, '2017-07-14 06:16:24', '0000-00-00 00:00:00'),
(58, 'zzz', '$', '400', '', 'ghfgh', '5xsBiPtlaoXFzdIqRE62GDkyvJUVW9.jpg', 10, 8, 'S,M', '#000000', 1, 0, 'Admin', 1, '2017-07-14 06:18:48', '0000-00-00 00:00:00'),
(59, 'szss', '$', '444', '', 'ftyguhijokpl', 'iD9QCungq1XEoJfL6rOYKhmj7P3Sy8.jpg', 11, 9, 'M,L', '#000000,#80ff00', 1, 0, 'Admin', 1, '2017-07-14 06:19:26', '0000-00-00 00:00:00'),
(60, 'aa', '$', '122', '132', 'wds', 'n20ScJK5BuFfYe4Z1m8rsGv7AMgLIi.jpg', 11, 8, 'S,M', '#0000ff', 1, 0, 'Admin', 1, '2017-07-18 06:25:55', '0000-00-00 00:00:00'),
(61, 'aaa', '$', '22', '22', 'ssd', 'tdRKnfaAUDSJLCTH7WEXBkqjyMsuhG.jpg', 10, 10, 'M,L', '#000000', 1, 0, 'Admin', 1, '2017-07-18 06:27:38', '2017-07-18 13:00:22'),
(62, 'aaa', '$', '22', '22', 'ssd', 'DAURxfdlseWBYJ4yL2ocESMPwuqhNm.jpg', 10, 10, 'M,L', '#000000', 1, 0, 'Admin', 1, '2017-07-18 06:31:57', '2017-07-18 12:36:02'),
(63, 'aaa', '$', '22', '22', 'ssd', 'PnLzYA3cy0OoWmdKx19QRb4HwZF7Tv.jpg', 10, 10, 'M,L', '#000000', 1, 0, 'Admin', 1, '2017-07-18 06:33:02', '2017-07-18 13:11:53'),
(64, 'xxx', '$', '123', '', 'fdf', 'j7NSvUpKFODhW0tCsJwHVrMIzkeQf4.jpg', 45, 10, 'S,M', '#000000', 1, 0, 'Admin', 1, '2017-07-18 07:18:44', '0000-00-00 00:00:00'),
(65, 'aaa', '$', '200', '', 'kk', 'p3ExoCFYqb8guhl7nAtJmU51k2zsOc.jpg', 45, 10, 'XL', '#000000', 1, 0, 'Admin', 1, '2017-07-18 07:32:29', '2017-07-18 13:13:39'),
(66, 'aaa', '$', '200', '', 'kk', 'u1Fo6q5JWhzZTVmwMy4n8gjQHLsUaA.jpg', 45, 10, 'XL', '#000000', 1, 0, 'Admin', 1, '2017-07-18 07:36:31', '0000-00-00 00:00:00'),
(67, 'aaa', '$', '13', '12', 'sss', '0BDk5AwKo9vF38qfOyQzLN4IaiGh6g.jpg', 45, 10, 'XL,XXL', '#000000', 1, 0, 'Admin', 1, '2017-07-18 07:38:04', '2017-07-18 13:12:12'),
(68, 'dummy', '€', '5000', '', 'fghgh', 'product_default.png', 45, 10, 'M,L', '#000000', 1, 1, 'Admin', 0, '2017-07-20 12:24:29', '2017-07-21 12:08:37');

-- --------------------------------------------------------

--
-- Stand-in structure for view `products_view`
-- (See below for the actual view)
--
CREATE TABLE `products_view` (
`p_id` int(11)
,`p_name` varchar(255)
,`p_detail` text
,`p_image` varchar(255)
,`p_brand` int(11)
,`p_color` varchar(100)
,`p_size` varchar(100)
,`p_original_price` varchar(15)
,`p_new_price` varchar(15)
);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `image_id` int(11) NOT NULL,
  `p_image_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_image_type` tinyint(1) NOT NULL COMMENT '1 for Primary and 2 for others',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `p_image_name`, `p_id`, `p_image_type`, `created_at`, `updated_at`) VALUES
(123, 'tVEhRFTYD4gS6BIkcA2vwufjmMsy97.jpg', 43, 2, '2017-06-29 13:01:54', '0000-00-00 00:00:00'),
(124, 'xaUAw96DfOBlPmc0uNt5vdWRCipj2F.jpg', 43, 2, '2017-06-29 13:01:54', '0000-00-00 00:00:00'),
(131, 'e2KRBsdYWZzuiF1DPvTk3EM69OrgN8.jpg', 32, 2, '2017-06-29 13:56:51', '0000-00-00 00:00:00'),
(132, 'zML0rg5P9HyD6nVvWjx4kqaU2NFme7.jpg', 32, 2, '2017-06-29 13:56:51', '0000-00-00 00:00:00'),
(135, 'bYTf53P7dk4umtoBhSWD1Q2FlnrREA.jpg', 40, 2, '2017-06-30 05:37:09', '0000-00-00 00:00:00'),
(136, 'JiTt4EFXLKZ2P8qbHQAGwyWS1uVod0.jpg', 40, 2, '2017-06-30 05:37:09', '0000-00-00 00:00:00'),
(137, 'skKENvanUTLCDXiJu5lAMoFeGBYfhO.jpg', 30, 2, '2017-06-30 05:46:13', '0000-00-00 00:00:00'),
(138, 'ZuzpVcd8JUql7GeFfxEoBnCXY9hT0i.jpg', 30, 2, '2017-06-30 05:46:13', '0000-00-00 00:00:00'),
(139, 'oOfVMl5gyCKFsHqGEm6Sk7jvpzQeUh.jpg', 33, 2, '2017-06-30 05:46:46', '0000-00-00 00:00:00'),
(140, 'HigoyPRd0GtNXhBqbpY5fzEV1TnmSK.jpg', 33, 2, '2017-06-30 05:46:46', '0000-00-00 00:00:00'),
(141, 'k1MRgsmXiO3EjTS5ZouYWedz2rxnHL.jpg', 34, 2, '2017-06-30 05:47:47', '0000-00-00 00:00:00'),
(142, 'gvFtcNbhIHTRpL9suy5e6D0UAlid1n.jpg', 34, 2, '2017-06-30 05:47:47', '0000-00-00 00:00:00'),
(143, '8nQ3JAPwdhjROBDE0aioHLImMGcszV.jpg', 45, 2, '2017-06-30 09:52:09', '0000-00-00 00:00:00'),
(144, 'BXKcRPurUk4vdGlAJSq8mCs5byEWog.jpg', 46, 2, '2017-07-03 06:14:11', '0000-00-00 00:00:00'),
(145, 'gc1nasXo40N2Qu5bVyBGCftPl9JDHW.jpg', 46, 2, '2017-07-03 06:14:11', '0000-00-00 00:00:00'),
(146, '7UzHoPeu0jftqbmKBWXRhyw9x5VlJ3.jpg', 47, 2, '2017-07-03 06:47:52', '0000-00-00 00:00:00'),
(147, 'OxujQDMgwnIq2z35LoHAJikNTeG6Uy.jpg', 47, 2, '2017-07-03 06:47:52', '0000-00-00 00:00:00'),
(148, 'v2azxWjZTyflQeK8kYMsmJGrP91o56.jpg', 48, 2, '2017-07-03 06:55:51', '0000-00-00 00:00:00'),
(151, 'I8sUupKMwRiHDj1oqynJekbh3QAfmS.jpg', 44, 2, '2017-07-03 10:17:50', '0000-00-00 00:00:00'),
(152, 'xl75NuLopTqvW4F36Uh9rMsRbn2yjS.jpg', 44, 2, '2017-07-03 10:17:50', '0000-00-00 00:00:00'),
(153, 'jWF3GftndDzQJSNUH4g1hyLo6OZwp9.jpg', 55, 2, '2017-07-04 05:00:42', '0000-00-00 00:00:00'),
(154, 'SPrOMVmRCGavuQkFLeb7Tsf0ijd42Z.jpg', 55, 2, '2017-07-04 05:00:42', '0000-00-00 00:00:00'),
(155, 'CdAQVfKleivBg2aF9c3oTY8HN5mPWb.jpg', 56, 2, '2017-07-14 04:57:55', '0000-00-00 00:00:00'),
(156, 'QXYFZmI5VylT62a74Osjor9eRJUwNg.jpg', 56, 2, '2017-07-14 04:57:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_offer_limit`
--

CREATE TABLE `product_offer_limit` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `p_start_date` datetime NOT NULL,
  `p_end_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_offer_limit`
--

INSERT INTO `product_offer_limit` (`id`, `product_id`, `p_start_date`, `p_end_date`, `created_at`, `updated_at`) VALUES
(1, 56, '2017-07-18 00:00:00', '2017-07-22 23:59:00', '2017-07-14 04:57:55', '2017-07-19 13:45:31'),
(2, 49, '2017-07-12 11:42:00', '2017-07-13 11:42:00', '2017-07-14 06:12:59', '2017-07-14 06:42:54'),
(3, 56, '2017-07-18 00:00:00', '2017-07-22 23:59:00', '2017-07-14 07:05:15', '2017-07-19 13:45:31'),
(4, 63, '2013-08-01 13:00:00', '2013-08-02 13:30:00', '2017-07-18 06:33:02', '2017-07-18 06:33:02'),
(5, 62, '2017-07-18 12:36:02', '2017-07-18 12:36:02', '2017-07-18 07:06:02', '2017-07-18 07:06:02'),
(6, 61, '2017-07-18 12:39:20', '2017-07-18 12:39:20', '2017-07-18 07:09:20', '2017-07-18 07:09:20'),
(7, 61, '2017-07-18 12:55:56', '2017-07-18 12:55:56', '2017-07-18 07:25:56', '2017-07-18 07:25:56'),
(8, 61, '2017-07-18 11:00:00', '2017-07-21 12:00:00', '2017-07-18 07:30:22', '2017-07-18 07:30:22'),
(9, 63, '2017-07-18 13:00:55', '2017-07-18 13:00:55', '2017-07-18 07:30:55', '2017-07-18 07:30:55'),
(10, 65, '2017-07-16 00:00:00', '2017-07-17 23:59:00', '2017-07-18 07:32:29', '2017-07-18 07:43:24'),
(11, 67, '2017-07-18 00:00:00', '2017-07-22 23:59:00', '2017-07-18 07:42:12', '2017-07-18 07:42:12'),
(12, 65, '2017-07-16 00:00:00', '2017-07-17 23:59:00', '2017-07-18 07:43:04', '2017-07-18 07:43:24'),
(13, 65, '2017-07-18 00:00:00', '2017-07-21 23:59:00', '2017-07-18 07:43:39', '2017-07-18 07:43:39'),
(14, 56, '2017-07-18 00:00:00', '2017-07-22 23:59:00', '2017-07-18 07:45:14', '2017-07-18 07:45:14');

-- --------------------------------------------------------

--
-- Table structure for table `p_s_categories`
--

CREATE TABLE `p_s_categories` (
  `p_s_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_s_categories`
--

INSERT INTO `p_s_categories` (`p_s_id`, `p_id`, `s_id`, `cat_id`, `created_at`, `updated_at`) VALUES
(142, 45, 10, 28, '2017-07-03 07:46:59', '0000-00-00 00:00:00'),
(143, 45, 10, 35, '2017-07-03 07:46:59', '0000-00-00 00:00:00'),
(155, 34, 9, 0, '2017-07-03 10:11:17', '0000-00-00 00:00:00'),
(156, 44, 10, 35, '2017-07-03 10:17:50', '0000-00-00 00:00:00'),
(162, 30, 10, 27, '2017-07-07 08:56:42', '0000-00-00 00:00:00'),
(163, 40, 6, 28, '2017-07-07 08:57:43', '0000-00-00 00:00:00'),
(164, 40, 6, 29, '2017-07-07 08:57:43', '0000-00-00 00:00:00'),
(165, 40, 6, 35, '2017-07-07 08:57:43', '0000-00-00 00:00:00'),
(166, 32, 8, 18, '2017-07-07 08:59:13', '0000-00-00 00:00:00'),
(167, 33, 10, 27, '2017-07-07 09:00:01', '0000-00-00 00:00:00'),
(168, 33, 10, 28, '2017-07-07 09:00:01', '0000-00-00 00:00:00'),
(169, 33, 10, 35, '2017-07-07 09:00:01', '0000-00-00 00:00:00'),
(170, 43, 9, 28, '2017-07-07 09:00:57', '0000-00-00 00:00:00'),
(180, 58, 8, 34, '2017-07-14 06:18:48', '0000-00-00 00:00:00'),
(186, 62, 10, 0, '2017-07-18 07:06:02', '0000-00-00 00:00:00'),
(189, 61, 10, 0, '2017-07-18 07:30:22', '0000-00-00 00:00:00'),
(193, 66, 10, 28, '2017-07-18 07:36:31', '0000-00-00 00:00:00'),
(195, 63, 10, 27, '2017-07-18 07:41:53', '0000-00-00 00:00:00'),
(196, 63, 10, 35, '2017-07-18 07:41:53', '0000-00-00 00:00:00'),
(197, 67, 10, 28, '2017-07-18 07:42:12', '0000-00-00 00:00:00'),
(201, 65, 10, 28, '2017-07-18 07:43:39', '0000-00-00 00:00:00'),
(211, 56, 8, 18, '2017-07-19 13:45:31', '0000-00-00 00:00:00'),
(212, 56, 8, 28, '2017-07-19 13:45:31', '0000-00-00 00:00:00'),
(213, 56, 8, 34, '2017-07-19 13:45:31', '0000-00-00 00:00:00'),
(215, 49, 8, 28, '2017-07-19 13:54:47', '0000-00-00 00:00:00'),
(229, 68, 10, 28, '2017-07-21 06:38:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `s_id` int(11) NOT NULL,
  `s_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `s_location` varchar(100) CHARACTER SET utf8 NOT NULL,
  `s_image` varchar(100) CHARACTER SET utf8 NOT NULL,
  `s_p_count` int(11) NOT NULL,
  `s_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `s_show_screen` tinyint(1) NOT NULL COMMENT '1 for show 0 for hide',
  `s_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `s_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archived 0 for no archivee',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`s_id`, `s_name`, `s_location`, `s_image`, `s_p_count`, `s_status`, `s_show_screen`, `s_added_by`, `s_archive`, `created_at`, `updated_at`) VALUES
(6, ' Mall of America', '60 E Broadway, Bloomington, MN 55425, USA', 'BuVka46y9qSY7Jg3jIR1UZxez8oAm5ELXnQ0swHW.jpg', 1, 1, 0, 'Admin', 0, '2017-06-15 20:30:30', '0000-00-00 00:00:00'),
(7, 'King of Prussia Mall', '160 N Gulph Rd, King of Prussia, PA 19406, USA', 'PHDTL6aS1mEfsNJCX3FqnWUjI4wpQO2xgVBetRGh.jpg', 0, 1, 0, 'Admin', 0, '2017-06-15 20:31:14', '0000-00-00 00:00:00'),
(8, 'Sawgrass Mills', '12801 W Sunrise Blvd, Sunrise, FL 33323, USA', 'el7Mx0qNprHW8KobFzCVwDnjhI4c3fQ9LSgskXu1.jpg', 5, 1, 0, 'Admin', 0, '2017-06-15 20:35:06', '2017-07-20 18:22:41'),
(9, 'The Shops at Columbus Circle', 'With quirky boutiques and fashionable neighborhoods abound, it takes a lot to stand out on New York’', 'Pi4fo3MXUHJzrhOtAGdy0jZeLbSIp5RNg6KYQD97.jpg', 4, 1, 0, 'Admin', 0, '2017-06-15 20:37:36', '0000-00-00 00:00:00'),
(10, ' The Galleria', '5085 Westheimer  Rd, Houston, TX 7056, USA', 'xya2uSgOICNimHrG0KXQ3L1tYsUvochPkFnBR7Dz.jpg', 12, 1, 1, 'Admin', 0, '2017-06-15 20:38:08', '2017-07-20 18:20:16');

-- --------------------------------------------------------

--
-- Stand-in structure for view `stores_view`
-- (See below for the actual view)
--
CREATE TABLE `stores_view` (
`s_id` int(11)
,`s_image` varchar(100)
,`s_name` varchar(100)
,`s_location` varchar(100)
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `u_email` varchar(50) DEFAULT NULL,
  `u_access_token` varchar(255) NOT NULL,
  `u_device_token` varchar(255) NOT NULL,
  `u_device_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for android and 2 for iOS',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `u_email`, `u_access_token`, `u_device_token`, `u_device_type`, `is_active`, `created_at`) VALUES
(22, 'satish@techindustan.com', 'f317ae0938a930c9d21bff18cd661a766e32cbba31bb0a2c8d68f74bb118e0d6', 'eyumheOPX6s:APA91bG-jhXVugawOeThZ3mZrHSJDZpAuvgMh0CqQm5j9o35YOmQgBmQce-6G4lUgvgirMaRRVs1OfwbE8nj78oVHka8Who3-tMqtvXKZdbe3sCg4sNDBF3TLD9c2g7NK_3b41hScUC7', 1, 1, '2017-06-29 11:16:58'),
(23, 'sh@g.com', '181f05fcf49199408ed221e1040d2151b7de5a88724bc213cdd89365f1e6ed84', 'eyumheOPX6s:APA91bG-jhXVugawOeThZ3mZrHSJDZpAuvgMh0CqQm5j9o35YOmQgBmQce-6G4lUgvgirMaRRVs1OfwbE8nj78oVHka8Who3-tMqtvXKZdbe3sCg4sNDBF3TLD9c2g7NK_3b41hScUC7', 1, 1, '2017-06-29 12:19:24'),
(24, 'shruti@gmail.com', '17f1cf1be433f61e8d661e50fe0a98a8687fae3a0d5d5eb222915fdc8f1a6902', 'dOsMWeGkqDM:APA91bEwZlVBrkwwIv9UetODoCY37uCJ9OrhYsr8zVl2DJkuGoHwJnnl-2DRN6gzezwiXBbc9zhDy7Gnyjp5LNzdPqlLN_0ht6ZYn1KoTSYCI6VgpAMnI3CkXL7C6A-YIO21rTpbpal7', 1, 1, '2017-06-29 12:23:00'),
(25, 'sh@gma.xom', '209272a6a2ddbb82c659acbed752d613bdc61a0c1a5a5932508dadacfcd22b7e', 'cY2mMgeIW2E:APA91bHLvz0KpHJklCN7tJXnziWlQ4QCeJGg53KH66bA7nn4RDyKMF4Kz1n_KkVMO0x_0xm3feLGcX7u2yrF2EAIgdrVu7hYMHgDZjDUC5_jWO-H8gDca-bVDg9y_4o6wy5dBds4TB-e', 1, 1, '2017-06-29 12:34:42'),
(26, 'shru@gmail.com', 'c27c7a3083f6ed393505a8cf5baf56de2a8ea6b2b34d2f97031600124ea7d0cc', 'fWwmNA1lulI:APA91bFGkvjAB24rIl5X3vx301gMGARjj6hdH401oqFhnyuTiBrJsU8LkX0VJ42ne7SJ3wA_qUD_k5oKxoooDyzqCr8BfUtbYXkRYvPBeuhgsC1LE25Ri9gt0jXOSgFnuYNg8n_topXP', 1, 1, '2017-06-29 12:40:51'),
(27, 'miniwe@gmail.com', '60c49f01b00eced505c357cb08c25b6ff7da99824a713333d69b8e4879d517f2', 'fWwmNA1lulI:APA91bFGkvjAB24rIl5X3vx301gMGARjj6hdH401oqFhnyuTiBrJsU8LkX0VJ42ne7SJ3wA_qUD_k5oKxoooDyzqCr8BfUtbYXkRYvPBeuhgsC1LE25Ri9gt0jXOSgFnuYNg8n_topXP', 1, 1, '2017-06-29 12:45:55'),
(28, 'sh@gmail.c', '971c9f6a354259c5b0f3c320318ab13667b8c63f11f3e6a78d04320b4aedbcdb', 'eqln3XHkDO8:APA91bGlLqHf7oLuI8wg191qgtyG06xTjDLx1TDMgDW8oyzwa8RNpMeIhhsASGe9Er-OgQsTLx3S4QFVo97CGa2VLrVuI_eOlW-7Q60E9Z29ODRTcgawevj5DytRg8S6hEtXbd7kdX7I', 1, 1, '2017-06-30 07:46:13'),
(29, 'test@gmail.com', '29b7cc127621c2e89ccddf34fa4b96500c5c752fe2e7ad8159d0dd1860e703c8', '', 2, 1, '2017-07-03 05:51:53'),
(30, 'sam@techindustan.com', '09c237f2b33ac35701b70a50947a70761bf0393a9ac89cc9d9dc7dbb2852afb3', 'fpFUC_z8r3k:APA91bH89nUSzclBZ9nvDCZHvAyvVkcizvaVw0B0J569J5qR1PrFc7VIE0SoI3Epd7LojzJkIQcpnCGOy-3zjIkqiIvg6CnXn3VTnKr8Kzqe-AaZ5jChQNX4CSheZ3WqebCDyVZQUVtF', 1, 1, '2017-07-03 05:55:49'),
(31, 'test@gmail.com', '4efdcbc5307096f75213d5892d95f4065e6fefab6c675164c1999d4f03c26956', '', 2, 1, '2017-07-05 06:38:24'),
(32, 'sh@gmail.com', 'e6bb41c174af6bc2b5f07a89bf8407fba4d5f89efdc0c7981c44f10b57365465', 'fxHePS9YbE8:APA91bGcfdexNngTrxByVUkGrJHtZIzRQ7lvIP4Pf7Z0TYuejO7zPCT7KV-QOWMO1Wzu7_LRmWLFQ8GtiEIfqKanfO_snRYKDCtGQMIF3HRSL9mpkcyutPOADT44XkpRruKSCMgVfQ6C', 1, 1, '2017-07-05 11:31:26'),
(33, 'abc@g.com', '576b098548a29807bf8e31a64b2ea9453eea6c4178c9fb020c412ac84e282e7c', 'dhGxSJsjH_w:APA91bEBdzbML2HJddkKBIrJK_rFj2yYIdo_EQ7X72v5pxCzAExuFc49Cn3Qy5s7h4LDN85RXjABQNwf7LlIT_D4INO9644r2hysG8XaoyxDPjFNT27LqdVuaQQZm3YTmiwWKk6xVnBR', 1, 1, '2017-07-05 11:35:18'),
(34, 'abc@gmail.com', '1be8c10c84ca75c12da7fe9420f0267f3acbad1897f13b3260e90283f88ad351', 'dV7Ay25l6zw:APA91bEQjhGInFbrn4A_QzADc-oFynixDwIkb4vZZon4JZRLOo3kN6LeqT6lxdvaIRUTnWhzlMWE1NValH1yVM7Ahp9l9z6tuqZ79ztkMrcyHGpo9TznONZoNkK7qWcuwnz9TfdQdHR2', 1, 1, '2017-07-06 08:23:59'),
(35, 'abc@gmai.co', 'f31f382afc2a74a6812049aaa03746767200790a3854692ea211cd79de7d84ff', 'fLuNwSFMdOc:APA91bHzINvMeYyZvqjMtGAlEePB_fhxDXLAHHt_xpgM4dZd04xjqQb13Go63BR_dRUNPy-S8yd_V3Mzpmj3x59mKq_FkyIC1svaTGwaA8ibvbM-G66JvuWr4D4x5PBcSlFp4XST0WYb', 1, 1, '2017-07-07 07:35:59'),
(36, 'abc@gma.com', 'ffc7b58d565a02e5f5813d09d6c9f66713677663afb0c1e2a53dc7277347322d', 'c03A2xeRl_I:APA91bEak4SLgGScXAYnxde_k3oht9mXPccvkeAoK5Iy-l3My6O5e-iWHwLO3cOp1-dE_SxtWkkuiWa3a5KaHNG34E3P1DncKh1dZnbPFmjQbb8mga9H7fvu5hb22zxg1JpvAvvNQ4ag', 1, 1, '2017-07-07 07:40:36'),
(37, 'abc@gmil.c', 'dfb54361eb8dc7faa092b1dd985e3d1675bf38c6615518c8a5cefe2c87432326', 'fUIJGnRNua8:APA91bG1e4aMvdLQ97C6dgHqXiNV9rwKPpR0teXZDB0RnseKRE91L6sIhvF3wCOj7xMFKotTbEpd9btrDnXm0sQF295p5r_uy0M8cjOLoJsL7_LqpdeAM-Cu8IRZPye79MDdl9wdXLUb', 1, 1, '2017-07-07 07:44:18'),
(38, 'satish@techindustan.com', 'c5925802d9fbc12f3e1c81165d75efe259510d51486075148bc717bed4f53bd2', 'ciPvOXp-hU8:APA91bE1f_oSCSczKbhYv6swM2up7PJKd-nw-hkJKDYT9Id9iftdO0LPpn104i101SD-Jb7hD56Elm7n9aG0Es8Ef70mjUfWfonJB1OFlPHU0ACUW4vzWf3s8OjK503v8fKB7ohjbmct', 1, 1, '2017-07-07 09:36:05'),
(39, 'abc@gmail.com', 'c27291f1f59d301aa648dd1070c6e7646fc17de46a583598fc6c407a9e49e8a1', 'cCLoOmK9z9w:APA91bEk3VCIcBYZ8F_TUbcJwtdd0_fYrB2OR5Ifn7UMGLwLO5C7Oa91lHvxMePX7dmMMbvblBzm_FakllmSGJ5ot1_vLm6u5IdiTnDhhtaLgsM9mwbnSJpy144sikrY7KNIPIW02Hwr', 1, 1, '2017-07-07 10:54:56'),
(40, '', '4cef557a9a890918b681bbe40fdc83ebcea74859d73eaea39b5794ff74be6ae9', 'cajp17j9OMw:APA91bGp_GGDcIXiC6WroCC0W9HvhxSMKxAiTXdDtB2s2TCN2saXq8k7n0AxMjCg1mYb8cwhMQneFSNEoCHnW8zX_gtsikrKYNzgCDBH8B1_cBuCJppIyvNY6M3MYHgOQDCTL2By-UCR', 1, 1, '2017-07-07 10:59:59'),
(41, '', 'b54a2b9b8f70c21ed0206ff19ea5253dd9d85dcf350fb2ba4825fc3f33e6556e', 'cfs92CvxrYw:APA91bFEBzOYd6FKydBf6-mZDJxVk4UZ2TEuYIXvIdwvYlYKrl6msqb8fxSMJG7H6buVrmFkO8III6eFbvIBRlaTM8K5OqepZHb6mWE3yylwvENNMsoXQaGdZkv6UpJinJtlweSWz_o4', 1, 1, '2017-07-07 11:16:42'),
(42, '', '63d9f392c4cfeb62be613ab8ed6da18c3750d8bc4d926562d65f48dcfc922d9a', 'cD_8fbEWc3E:APA91bEDbaagtOzjzJCMvK7eG4FwiHjOfmcJkazFaRdznBX_gRqYbkD26IKQhPvv9WOH3LGaRanb_b_DMJtLE17f3rLmRbYVE0FkFbPrAFIj2mo5_KDG_9f9mrH2tUw_HfrARe-Ku5G3', 1, 1, '2017-07-07 11:23:16'),
(43, '', 'c7bda84e2596b272a807ffe6c75ad11145a83c552dbff900669c4c171d5cc9e5', 'e9UYXVuXx6U:APA91bEs4PdwjbNMfJOoKlhA_FKvg0TQf659njQZwHgj0e313q9STQTf06_-CzbWD_TCJUkkzONUsCQZ_HN3e0TRGY35rGASclshkdsbM80OKsXkdpYStHxCFKnxtb0FlhMQyreiCFMo', 1, 1, '2017-07-07 11:38:48'),
(44, 'abc@gmail.com', '1e998fdd548fd018c032324a89100c38d210d4ef8a788ade2d35eef09af2906d', 'cl_J8D5vJ5E:APA91bFdEeIymCHQBBNGQhu7zwgiTE4lyHZtpZiM2eLY7uiyTm7_CK6umstpIUf8UewKjVvrYSK6Ns4ScVmhtmidzIMoO9Qg3gc67iwKiIlrUTXDjQSEZgSfiVMtg4e2_ndRaeToWdNO', 1, 1, '2017-07-07 11:41:26'),
(45, '', 'b130fe7c0cf22b111e0663a4d31888ed4fac8a62b027d15149127a5545a362f1', 'd0MIPI3Piok:APA91bGAH-kujEsBWGtF7_7TtQ-rPvsy4Q-tPJPqavGEm-_xiGfXDL-uvXiIlD06MPPadHzYkxiXPlLDW5iT8vPDpAjC0YnKFLRGLFjUl1ds9VKzdPqGyBDqR4T8z1ndVzeRTFnYzgKx', 1, 1, '2017-07-07 12:43:43'),
(46, '', '149a638502e43a6f2fa8e28f9caa393c74ab3e286e07f9cc42811a8d72882cff', 'fPt2VfRZ3_I:APA91bF8NIldAhV6JNxt8yrvm41zO4Nu0eFjhbTkNZBU1Rvps-UgbGC9LNeDR4QIjY4iHD4kPrPw40XqE4ePrEriR1oM8rjfyMrNEd2ZICyX8CvxSXH_OQiTMHnnPQn0dhBHvxHPAYzH', 1, 1, '2017-07-07 13:17:54'),
(47, 'abc@gmail.com', 'f3985a888de529eb173b588500acc541b2a78eb19d2eb26e604a156e390e5f96', 'dmj8IofEJd8:APA91bHoH2jc_uNjyfOV_0ptKd-oBZTAvhJ9_jKUyRqXmBeZc0lNW-CWxm6C8vt-lFRAIW_c_Bbf7EKQXBWdigVuNK6KfOQDdcAqeIZw0L_603EmRFnEOCfAXTGP8JHf_8DAuUJhOaNp', 1, 1, '2017-07-07 13:24:47'),
(48, 'abc@gmail.com', 'b6964aaf9c1a23bc4c9fd603fb85467dab7f477dec9c0a2008a0e0c421e5e282', 'eLKLYTVONCI:APA91bFDDgarMo9mcw908TflSdMG0KL2k7x5rIMfl-p9GJ-cDZwPgPJz_IukVX8cs-mz4CjTo4Wlps9NrTyXRiZiTZa2OiKzlkJcNkbE7l55AS1CM-gYwtNVD6A0x_dBEzySjv1VfHnc', 1, 1, '2017-07-07 13:47:45'),
(49, 'abc@gmail.com', '27a8e868a0be5b297ff4995651a574c2854e5238ee5e54da723144010b55f9f5', 'd-SwZwc1-Q8:APA91bHckhCQiURWgo767uxNQP02bAoHeGfKQvNKq3YF2HoCbGWGC6DML5-LT4NHNAp12qizu_-NyAWkAKbG2EjcIW-PdDCWQ9YCUBNR0pfZNJnMA6lUSp2oDd3SbPIHqMu0r4UuwoU0', 1, 1, '2017-07-07 14:59:05'),
(50, '', '1fe5d7adcb6934ef54048757aa9c580287f80d225b597313ee999e1bc600f887', 'eLVCM8ePYUk:APA91bE1wStjJWtAXdCP3aimja06gW-XUZ_zoBogUaUuQ25uRu_P1TWgFXbTErHUYYyhCWyrw0lr8eus4SfxNzslUjoX3-35Wvn3qnAnYRPQR1Kf_pt17w5AoDJQcq4k7embG3GkGkG3', 1, 1, '2017-07-10 04:44:03'),
(51, 'test@gmail.com', 'e3f62aba74ae0ccdad927e323124d71ab5337ea7588fe1c2d109c64f5de32c0d', 'fynAeiIID10:APA91bGpNinz3NNjvS-1S4xbyxZZnXi6_AgJc5wwnXIrPgmw435oU2jRry7b-QY0dkM9QwFXP3newigWCgc4DsbP7SJrPJ6vUf2pYmWVVgR4VlfNcuWyiWmGUmkVcvRMJBJAK7S_XYS2', 2, 1, '2017-07-10 05:14:15'),
(52, '', '26dbb16c4df5e7445f6240f5f40df26510a99dba7749395106322e70a2e27452', 'evWmtQf3a_I:APA91bEdPvx_zJj00lLrM5uQF32A_0VJedP156VGdQ6mrCzW-E5way6-Gq9NzePebg3kf58v7un7Dqu_laubPAImSp8_j1Hzf-OEo8UKqbwG9C3QgBV5wrXOIDwJJa1zYdrmJJQAzE9-', 1, 1, '2017-07-10 06:22:50'),
(53, '', 'a682d6be0b302a44d289682e6071c2e3649114a3e4791058bda50ae3a6c353c3', 'fNJZezpgkEc:APA91bEvebWCzfctrBHWywfuTUlpUtifMWzGT3FbaTZiUrX1cwagw2x558dhG3enIiksrLRgT2S4D9dwXB_XmNBry4-xgZXEha88st6BmZiZrD83diclWoioK5oQNuuKhxC-nAcfSj-f', 1, 1, '2017-07-10 07:53:03'),
(54, '', 'cf33258c0aa899d7bb6c2ffdd2f64de0d6af2cee7b8c149744e1828589f609f1', 'fmPxxeV79oM:APA91bFCMS-RXwyH9t-_p0px_PIoE2nuWnAyShqnP7b0TydXqVYCJt5SG4XlR8sOcW-rXBnbTenif97UCtboJOCoTmBE0oAJrImYnDO60yaXVchroI80URLeE7ljI9ifXixA_mSER0It', 1, 1, '2017-07-10 08:08:06'),
(55, '', 'e9675ea4579aa51ca60322b7c87b13e176fc5ea74d8cdc4752a3e49b1ddee4e5', 'cVK-ZoUwFu0:APA91bGvfbreivAron3dRfjyNWH-N_LaSV9b8535DJcuEfvuWvpjqbZcaqWBZ2XJWGYCoJ9aT54kI85mea8d61QxsmWP9enw_2UKiWcQZUbOqNODxYq55ZivHPkA0AQM4W-GH5rzqdHz', 1, 1, '2017-07-10 13:38:15'),
(56, '', '9a9c5555c8f8c467256010dceb3beb56922c1e37c14bc2f73049bcbe3ec76ab9', 'cxq7st_ARkQ:APA91bGc2fS86oV7obINle6mQbd56q5L4o8jGz251xae_XuI9k2xHaeBOi1bzDjeNO3-RwBC0hwuq_RNeLdpel7pFymOyV5dbjJntJ0OqRJ8aQtcQ1V6Dlsk19-5n-L8vuXnLpG3j-PM', 1, 1, '2017-07-10 16:58:40'),
(57, '', '46222f2f67e2e293dfd5156799f08bb2255c1dad82e37de3b8bdf2780c531b4c', 'e1VxJdc7sbU:APA91bH28zJoEO7soGV4j0HACnYNt0diZnhluwQIfQ7YfAlXjT35WL-jqOW2AhJRwKIWspodHKhNWgb3OADek277dUHJY14bjK6Fja4AkvLbhsvFtnnJc_4yjjUHFJxHYsInWkYNW2mQ', 1, 1, '2017-07-11 04:23:01'),
(58, 'abc@gmail.com', '605ed1dacf09c870ea7b18f8a09932ec795aca3e7c25aaa0b11966796c8af8c8', 'dytnErEneQ8:APA91bEX4karSiG0LlibZterhcbuu_GTsDtSqUKLau00GmEO26d_8JR0HuIzc-Mdk-aTOwqQRULhNF4SfYjoy05_87GpcSpvGsAv5aXvU-u_zu0v3SalAioyUENgdpVVirtL8dV6hlA-', 1, 1, '2017-07-11 06:37:53'),
(59, 'abc@gmail.com', '5a6bf1903a5995b769811afc0c4fafedaeb7536fa7f386244df23f0353df51ff', 'eiA59DXifxU:APA91bE-T0dEZBSwsLkQK4vDpw8Orn2RLupYYYwX1OzhHINK_iB04vaBQ6Xn9nP60Egjmnne2f0PDkmTs8hN6pDS__FXmEXal2QAqvxZpI_rSNMfHRboNeqImOv_imvik6Vwh1i1zUeI', 1, 1, '2017-07-11 06:41:10'),
(60, '', 'ae3243375931541c25bf44a566a2f539922f4bd455bac580d878b6df3c211d7b', 'fnWiUjUty44:APA91bFdS6-01S-pmV5LHuOXDKGGn2913bf8KHKY63YxkiUXLVE3z3I4zhlSzRPVRa1WZBeEjWrw6p23QW0GSPBdVraDVIeFPAOSO0WJisylcrhUclLQUi8_O1nkUEjwgVxKUQ7g0m9E', 1, 1, '2017-07-11 10:46:38'),
(61, 'abc@gmail.com', '79b4dd7bc213bb5211ad56d234e4081572368b4f4ea7f88ebc12ec5b64a2dcd9', 'fpqYaOV2sv8:APA91bF_Irv_XEcKDQ7ALCqIXKoPJ3xxRCjFQxKad5mm3WaOD4BciLUk36Zkcgc_Rg7Ci1PCaSc5JeoYiBPQQdDZBaTPxcSlfcMhAT0s-fPiP65SSryUoG2KEbckBvKnrjafHJnbzvSS', 1, 1, '2017-07-11 12:40:37'),
(62, 'abc@gmail.com', '5ed6c548a8a10f6871736a717bd7e4a9b583971d86bba81907df62a21edca3b7', 'dvD5Ig-5vME:APA91bEUzxXRshXVVfc_QsjO3lwlWo1vtxrjq5GexTd2iltEKyXClr-6AUXyTLbidzaMyHXr7jlDvgr2IQXbFTtSZ9kFOe2RdqFrxjVd22VRzviHKANo5MT3foG-0Dt2BSZCmH-xw4hN', 1, 1, '2017-07-11 12:42:27'),
(63, 'henrik.hjalmarsson@protonmail.com', '49e7d97be0aee06329cbe6678f94d5e8b8ec0d6dbe1c341d4d22999b2b98dd9f', 'e_MZ0balTjY:APA91bHcPxXrJJXHyBtk61Dk0tgGUVKRFaAEqEzo_z-JREUIaMf_n_BzhBhr-T5eGLKH7LBEiOjsUAclEUxmVxKZDMN51-knteIxE6gPeDvZM-w9glvdSRtAwFY1deaKFUu8SjPxn0II', 1, 1, '2017-07-11 15:02:40'),
(64, 'abc@gmail.com', 'df4aed26d8a9a05b1233695bafe73651824bc10051aec755c044133cd05a55b1', 'dsI65JuCOCo:APA91bGlcufT2frPWJAFWw0eys8KW0ldg14COSVQN1UFXjinqmtkhZOIM48gmVcNWFIKjxd7PRFwhNDeEALO1aJsxMNv8lc8o60vN94_kntUohJ-vLx92K9JiNNm4kx8d3MRI2Ygjgxd', 1, 1, '2017-07-12 05:50:26'),
(65, '', '26d7209377f4d9fb5bbcef0df241ffd84ca21ffb56a5f4f950aef9a06bc1ccc3', 'fJaNv05dahk:APA91bFFjRxiMcbDIFnb2sukfzh3iWl9Xmp8UXJKb_moLWBUk6DI1w8DRx1omINuRft8IRuUTSwxlq_6nW99VMVwJUpWOAb656erJ5lBDRqccRznrG1GtNWFDkCha3SwNv6pA2U5XrM_', 1, 1, '2017-07-12 12:21:05'),
(66, '', '353b3d25d918168fa79fff15738bb5130206ae20c116a57d28147e26ff7386aa', 'eB5nS-09JlY:APA91bFMZv2w6jNACD_h3IJ6XbOa9WlHlHM84D-PC_IfTe10oJEfOSQdBUr470v4NdtpP272glJIk5ypvstog-mxWKUPDmXuDnvVlZpBTjX0UlBQXrQ8R64SZB0ZNcE1GNgOdlVY5mHZ', 1, 1, '2017-07-13 06:31:49'),
(67, '', '2e9e0cd4fde4b50dddf603cb26aa7fde29fd5dd717e6cdc8897c1bf9bfd18238', 'fM1UrTVTO-4:APA91bG2YfGW865Qrmic6BLxqcCyvm28f4z0NcZngkE7Ms-tF7_u221JllLSFCXTM1wp-wlclNyzaVnZjkNoXa79gZh_lc7VPkk9Qy1-yIJd3t_jsAuQs3MHT--gtSAYE2l-ldwaS8AZ', 1, 1, '2017-07-13 10:12:10'),
(68, 'lofbergsmail@gmail.com', '97b200710addd64430a07b44767e319265434e19a5d6e0a4749f88e0fbb33d6e', 'fBS6utuoxDU:APA91bGIEU5KwnFZkx9ypYG_Dm7VQO8AxMtON0u2syHm6-yh4BvqoqJs3sLvSaUJm7H-WTxxS10i5OSoYwdK_ScpoEgMW1TddiPorX-xc7ZLgIIqH0dha6xXZIrxJBcrao48CFNOvMXk', 1, 1, '2017-07-13 22:40:10'),
(69, '', '644eae705423a78490a96c4fcc2688aa6dcd55eda06c9a70a82050278a982e18', 'eYs3NYgO0NY:APA91bHUuqHHTrY0xF8I5o0lLEYJ3VKC1Pp6OzG1YodtQVSALqkcUi2X8w5TikTGa7IOzdqqOEtG280_iSTdSmKTsujQZ8jRxYAAo48z42wSANZh0SygBk4FYXqlp7Ew9wfBb0QjC87R', 1, 1, '2017-07-14 07:09:43'),
(70, '', '83c017df02067d04346d39f07d79e9b1f605f8be4f9986b46171f54b7cc1ccda', 'cNxL70dMR30:APA91bHVSp-bncIqrENr2k0W5BiEv_8-KNLP_NCX1iDJk1v_C484iUQETIJAqVjEdFdcYcjnm6KIs72QbBy6-6B5upakl9UTRkm5zLxy9O6xKEIaUbEIAVHIWNnHL8Hd24Bf2if_t75J', 1, 1, '2017-07-14 07:13:11'),
(71, '', '0d33e95583e0bc3cc422f38878f96c850c2815bd7c7a9672cf9e1aa117a2714b', 'f38u3Zn7Hg8:APA91bGEN5iUVwQBAc8MePD9NxLeFp8Nb1h0JKHMgx63xZfZhFRfEpJAC1Ts7TmilOM0-I9cY7U65sT5Gt0FY7AEqiA8I71N0W8y5gtF8CaKUBmMR240aS5peE-9iAOUc94kqC9EpU_A', 1, 1, '2017-07-14 07:30:54'),
(72, '', '21e48c10aa954c44d374720d8a55a9b83b5d2b5c12a5e68ffeddc77eea5c1f53', 'frskignMR8o:APA91bFBuaxD9if17fXD5jYLkjPBxVJm4jt7tD-XP9TRRFQTU-uInglyhSuuLanG_-xFmHa1Oy0kdeFEOh81l66OAxgN9cwfP5qu8CN5lzJbruM1EqM7YaogcBSToF6uUTdXfsCko2JQ', 1, 1, '2017-07-14 17:32:17'),
(73, '', 'b12f1d860aeeae46f999f3407ccebffc46e2b2f45d8d37311e852567fc710449', 'exKQXazQlWM:APA91bFCoWsArNM-01yA0lDErNa54PVaSM9ur_LF8G9h0b6KIItFg0nm6SlvXSbDVCtTLAaUnDFsYtGL2S-tHhuf3FRrZZQfnAL8PE7yfAOfApEX8eDJY9PjJmKKmUXgRe6yUmpNfT61', 1, 1, '2017-07-17 07:25:51'),
(74, '', '4d014d953bcc28dc83ef206f2f20e3e62ec4354666375841758b44dbe567fa22', 'dyobVGWukP4:APA91bHDvYXv8q9RzuUzY2OKzDqYeJRcCKzCP4o01CANSSKhERW2lmNlYaPFywT3s4sVwonR1S-Dk5GcAjMi_mYNfyMiOEW2otflDUXgceMH0gLTzXVgN24o-A8-9Ny_6fmK0rPF8evl', 1, 1, '2017-07-17 11:55:18'),
(75, 'test@gmail.com', '211b09c2aa5d34c2edd027e3ce70ee30b1d20ca1ace67ac7c935dd2ba3daeea6', 'dlTYlTvi1UI:APA91bGA4D9IcIEg4XihVmt4UQtVpGrPlJyleCsJiJvhaDl1ot9HcdjjdZdO0QWQ0geF5ftl58BubagIFMi-WjJqMLqi1DwC7fldZSwhc_4La4J5q_ymBwyDls9PDJc7DVlbvWlDfSzZ', 1, 1, '2017-07-17 17:54:14'),
(76, '', 'f457d177caf795b935daf9ee53f92431583049f9fb6c361e00c53320bdece9c0', 'dB7pARCEOUg:APA91bGi6TLIY_2dLdHh4rUHkXh5cR-eaomtczPno6NnrbzV-8Ly2F5zw0wnnen9CAR-YgNnVPSVErPpjst6O3V70WI6YitRd1sNfEzMU9UYgz-RHZBlka3C95FcrMfygDkzYgIbvKLy', 1, 1, '2017-07-18 07:24:15'),
(77, '', '0a34e25783d01a7a439424d6afa9b1d8d3719d406e40bb761774324718efc922', 'eRC_x5y930o:APA91bEHvkyJ7uBT0QLp2Z78hR4exvlAXtkCmGjImhq3YnhsuI5x8AdSpTiR8ZPc_uaMN0II6F1fE3Tx-I1im61_PQo3djVtNo6iZjfTE6IwMzTUY9qufyU6aIJf1c4M4CaXedQMppXH', 1, 1, '2017-07-18 08:28:58'),
(78, '', 'c096f67c54b500f139ad293903ca8a5108895e394652165941a5a0a928ad57de', 'ccRYlentXwk:APA91bFyjs7TUZbtiM5jc9MGCXw5GONtUOErXJMXDEd71tSLwepaRwvjCEv3BdqA76bAbr_BHh3yW4Cf8JhBm8hNKHDBH7-PrFyVW9TvuttVLZO7AXCpmSFDxJS1bcYM2kwDa99wwCV-', 1, 1, '2017-07-18 09:53:45'),
(79, '', 'e42e38ecd002341ee98c2eb90b8fd23668b82250c49c56125c8713842097b4ff', 'e-6SbsvtYwY:APA91bH9Cy8WRUUDG7-lOmepdqaMOuFyUuTFsDaN3pBNiyg3cx3D1gTeottsE-U0kLh1SyyOGdE_wmDw0TYcEjJCE3wG5We4jaGdP9BScD4jX5FspFaNMoRkD6r4vn97yIM36l0m0Gjd', 1, 1, '2017-07-18 09:59:04'),
(80, 'test@gmail.com', '86bb5b369fcf0a718d784f1d785a3d5ef5a0f17b9b6b3c21fa90d07e76e9a4f8', 'eW7I5eMm45A:APA91bEAf0DG7npohciFsivaeNv--6nqkgCG_DRU0U2C_jWxhLEhUm_C3sYLF8JwQn-8-5oQyl1qetkvPUeT2CmcBUkKQdn02H1Y7zDDHLSxGpMsSLBN4nZOjR7VPNaf1O0XJgLFSsXB', 2, 1, '2017-07-18 16:42:46'),
(81, 'test@gmail.com', '7fc63d34a7798b34bd791ea8ebfd92477d121f1a831e4c5ad89f4d90a3db746c', 'fS81n0r_EeU:APA91bFmbO3ejrufcx3AKvlPigphw55XtKBCjkLJ3ingYdNXUxY55lI5JSWAykW7qX29RE2MrLIJsQNqO6eeU4EvpTvQ7NbHoXbnekS53ygjFX32JYPYt4Dc2VJcPzZn2U6st8UwELT3', 2, 1, '2017-07-18 16:52:33'),
(82, 'test@gmail.com', '9ee9857d8d14712e971d96e521a9249a0daa9a5f8cbf08b1bd089f2d9e107b5e', 'dJZcOnOT_fE:APA91bHd4Y8IGHjMN8M0uhjYiJexIxP2lsCcsLB73BRM8Q3M6H1S1GX3NvOxpbXkkiS0Xud3YTHUF0HcZDyx3yF8Eg3M1QpsV-yyPabJKLE9IdI4H6vpRiUDQhI9z6mqbGwWw_0P7jBk', 2, 1, '2017-07-18 16:56:33'),
(83, '', '0bf305103473c29f570863e87415346e8a7948979ea78cb02de97cd483661b09', 'e2WsEJcd6Mo:APA91bGkOKOL-LLyJ5rTJS7xP-Skx30xzRU8lf67Vc_60RETGs5AWg3QUBnPgUVzAwEILEvyEQLn2swmMVvvfwROZglb6CxpeEtkMpo3IxSXBgzYzK8A98daDmvViS0vSjT-IyUdWgBk', 2, 1, '2017-07-18 18:53:07'),
(84, 'test@gmail.com', '1e3a2279ca74f8b143b71460436f142ab501dda361ec14ed5ad28cce71c78012', 'cZjtdAiZGj0:APA91bFLnI-NYluAu6qdX_lyml5B2xglXuJ8vo8nRU978Wz-VBsnfdWMTUBPDOFpHZfIuzFlB3Cmr3PWX8IaWmUHOg1wochT74e2v14LbxBVZCHewoax5AtaNC_fvC2jO-YLRmsZiOxD', 1, 1, '2017-07-19 16:23:30'),
(85, '', 'e00a3aef15d2ff2ab1b54570875dd8b70e53dbcb7be4a25a14556a8b887f7957', 'cn13BG5aEUs:APA91bFCKfcOe3BNEcr92071Ihd40ahQXtEH7OopN98vr3WPOFDtDDnlBjhGL8K1Sg_eaflbWtCBAldQQHIMT0guv0opz6a8mjn7bXyG755fEo_FX6RuAhZrIJHyBRh0P56BW6y3pu2Q', 1, 1, '2017-07-20 04:21:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_bevaka`
--

CREATE TABLE `user_bevaka` (
  `bevaka_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `keyword` tinytext NOT NULL,
  `checkbox1` tinyint(1) NOT NULL DEFAULT '0',
  `checkbox2` tinyint(1) NOT NULL DEFAULT '0',
  `checkbox3` tinyint(1) NOT NULL DEFAULT '0',
  `checkbox1_value` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_bevaka`
--

INSERT INTO `user_bevaka` (`bevaka_id`, `u_id`, `brand_id`, `keyword`, `checkbox1`, `checkbox2`, `checkbox3`, `checkbox1_value`, `created_at`) VALUES
(5, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-07 11:20:46'),
(6, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-07 11:25:18'),
(7, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-07 12:51:50'),
(8, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-07 12:52:54'),
(9, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-07 12:56:09'),
(10, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-07 13:21:04'),
(11, 46, 7, 'abc', 1, 0, 1, 10, '2017-07-10 06:15:12'),
(12, 46, 7, 'abc', 1, 1, 0, 10, '2017-07-10 06:16:10'),
(13, 46, 7, 'abc', 1, 1, 0, 10, '2017-07-10 06:17:14'),
(14, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-10 06:17:41'),
(15, 52, 8, 'abc', 1, 0, 1, 1009, '2017-07-10 06:29:57'),
(16, 52, 7, 'nike', 1, 0, 1, 0, '2017-07-10 10:30:03'),
(17, 46, 9, 'nike', 1, 1, 0, 123, '2017-07-10 11:56:05'),
(18, 30, 1, 'abc', 1, 1, 0, 200, '2017-07-10 11:56:37'),
(19, 46, 8, 'ed', 1, 1, 0, 2345555, '2017-07-10 13:31:05'),
(20, 56, 11, 'test', 1, 0, 1, 200, '2017-07-10 17:00:27'),
(21, 46, 7, 'dfds', 1, 0, 1, 12, '2017-07-11 11:47:28'),
(22, 46, 7, 'abs', 1, 0, 0, 102, '2017-07-11 11:55:29'),
(23, 46, 7, 'abs', 1, 1, 0, 102, '2017-07-11 11:56:39'),
(24, 46, 7, 'abs', 1, 0, 1, 102, '2017-07-11 11:56:53'),
(25, 46, 7, 'abs', 1, 1, 0, 102, '2017-07-11 11:57:08'),
(26, 46, 7, '232', 1, 0, 1, 123, '2017-07-11 12:19:57'),
(27, 46, 7, '343', 1, 0, 1, 1222, '2017-07-11 12:21:01'),
(28, 46, 7, 'asasa', 0, 0, 0, 0, '2017-07-11 12:27:05'),
(29, 46, 7, 'abc', 1, 0, 1, 124, '2017-07-11 13:14:25'),
(30, 46, 9, 'avdf', 1, 1, 0, 123, '2017-07-11 13:41:34'),
(31, 46, 9, '23123', 1, 1, 0, 12, '2017-07-11 14:02:39'),
(32, 56, 9, 'puma', 1, 0, 1, 500, '2017-07-11 15:20:52'),
(33, 56, 10, 'shoes', 1, 0, 0, 200, '2017-07-11 15:25:29'),
(34, 46, 42, 'abc', 1, 1, 0, 100, '2017-07-12 05:02:27'),
(35, 46, 9, 'abcc', 1, 1, 0, 111, '2017-07-12 05:11:14'),
(36, 46, 9, 'err', 1, 0, 1, 100, '2017-07-12 05:12:41'),
(37, 46, 9, '124', 1, 0, 1, 123, '2017-07-13 09:55:13'),
(38, 46, 10, 'avcc', 1, 0, 1, 1235, '2017-07-13 09:57:53'),
(39, 67, 9, 'dr', 0, 0, 1, 555, '2017-07-14 05:29:47'),
(40, 74, 7, 'sbbs', 1, 1, 0, 797979, '2017-07-17 11:59:20'),
(41, 73, 8, 'eeee', 1, 0, 1, 1211, '2017-07-18 08:56:18'),
(42, 85, 10, 'scsgs', 1, 0, 1, 855, '2017-07-20 04:41:42'),
(43, 85, 28, 'tt', 1, 1, 0, 8555, '2017-07-20 04:45:05'),
(44, 85, 42, 'ddff', 1, 0, 1, 0, '2017-07-20 04:49:05'),
(45, 74, 45, 'r', 1, 0, 1, 0, '2017-07-20 05:45:19'),
(46, 85, 9, 'gsgs', 1, 0, 1, 4757, '2017-07-20 10:54:02'),
(47, 85, 45, 'sfsf', 1, 1, 0, 4545, '2017-07-20 10:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `user_favourite_stores`
--

CREATE TABLE `user_favourite_stores` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL COMMENT 'User ID',
  `s_id` int(11) NOT NULL COMMENT 'Store ID',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_favourite_stores`
--

INSERT INTO `user_favourite_stores` (`id`, `u_id`, `s_id`, `created_at`) VALUES
(8, 33, 8, '2017-07-05 12:22:03'),
(9, 29, 6, '2017-07-06 08:18:11'),
(50, 34, 8, '2017-07-07 06:53:59'),
(58, 34, 10, '2017-07-07 07:32:05'),
(59, 35, 6, '2017-07-07 07:36:00'),
(63, 35, 10, '2017-07-07 07:36:47'),
(64, 36, 6, '2017-07-07 07:40:37'),
(65, 36, 10, '2017-07-07 07:41:18'),
(67, 37, 6, '2017-07-07 07:44:19'),
(68, 37, 8, '2017-07-07 07:44:34'),
(71, 34, 6, '2017-07-07 09:33:10'),
(72, 33, 6, '2017-07-07 09:33:41'),
(76, 47, 10, '2017-07-07 13:25:33'),
(77, 48, 10, '2017-07-07 14:57:20'),
(78, 48, 6, '2017-07-07 14:57:40'),
(79, 49, 6, '2017-07-07 14:59:35'),
(84, 50, 6, '2017-07-10 04:46:27'),
(107, 59, 8, '2017-07-11 09:43:44'),
(114, 59, 10, '2017-07-11 10:37:21'),
(119, 30, 10, '2017-07-11 12:53:48'),
(126, 51, 6, '2017-07-11 13:36:20'),
(127, 51, 10, '2017-07-11 13:36:35'),
(128, 51, 9, '2017-07-11 13:36:46'),
(130, 56, 6, '2017-07-11 15:19:03'),
(131, 56, 8, '2017-07-11 15:19:26'),
(135, 62, 6, '2017-07-13 06:26:59'),
(136, 66, 6, '2017-07-13 06:41:42'),
(144, 72, 8, '2017-07-14 17:34:20'),
(145, 72, 9, '2017-07-14 23:12:47'),
(162, 77, 6, '2017-07-18 10:27:45'),
(177, 75, 6, '2017-07-19 15:55:13'),
(179, 84, 8, '2017-07-19 18:05:07'),
(185, 74, 6, '2017-07-20 05:56:08');

-- --------------------------------------------------------

--
-- Structure for view `brands_view`
--
DROP TABLE IF EXISTS `brands_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`devskart`@`localhost` SQL SECURITY DEFINER VIEW `brands_view`  AS  select `brands`.`b_id` AS `b_id`,`brands`.`b_name` AS `b_name`,`brands`.`b_details` AS `b_details` from `brands` where ((`brands`.`b_status` = 1) and (`brands`.`b_archive` = 0)) ;

-- --------------------------------------------------------

--
-- Structure for view `products_view`
--
DROP TABLE IF EXISTS `products_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`devskart`@`localhost` SQL SECURITY DEFINER VIEW `products_view`  AS  select `products`.`p_id` AS `p_id`,`products`.`p_name` AS `p_name`,`products`.`p_detail` AS `p_detail`,`products`.`p_image` AS `p_image`,`products`.`p_brand` AS `p_brand`,`products`.`p_color` AS `p_color`,`products`.`p_size` AS `p_size`,`products`.`p_original_price` AS `p_original_price`,`products`.`p_new_price` AS `p_new_price` from `products` where ((`products`.`p_status` = 1) and (`products`.`p_archive` = 0)) ;

-- --------------------------------------------------------

--
-- Structure for view `stores_view`
--
DROP TABLE IF EXISTS `stores_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`devskart`@`localhost` SQL SECURITY DEFINER VIEW `stores_view`  AS  select `stores`.`s_id` AS `s_id`,`stores`.`s_image` AS `s_image`,`stores`.`s_name` AS `s_name`,`stores`.`s_location` AS `s_location` from `stores` where ((`stores`.`s_status` = 1) and (`stores`.`s_archive` = 0)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`l_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `product_offer_limit`
--
ALTER TABLE `product_offer_limit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_s_categories`
--
ALTER TABLE `p_s_categories`
  ADD PRIMARY KEY (`p_s_id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_bevaka`
--
ALTER TABLE `user_bevaka`
  ADD PRIMARY KEY (`bevaka_id`);

--
-- Indexes for table `user_favourite_stores`
--
ALTER TABLE `user_favourite_stores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `product_offer_limit`
--
ALTER TABLE `product_offer_limit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `p_s_categories`
--
ALTER TABLE `p_s_categories`
  MODIFY `p_s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `user_bevaka`
--
ALTER TABLE `user_bevaka`
  MODIFY `bevaka_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `user_favourite_stores`
--
ALTER TABLE `user_favourite_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
