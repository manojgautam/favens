   //<!-- DElete confirmation pop-up  -->   
   
     $("#datepicker-component").datepicker();

     $("body").on('click','.deleterecord',function(){
  var theurl=$(this).attr('data-url');
   var thetext=$(this).attr('data-text');
   //alert(thetext);
   var newtext="";
   if(typeof thetext  === "undefined"){
      newtext = "You will not be able to recover this record";
   }
   else{
        newtext=thetext;
   }
swal({  
 title: "Are you sure?",  
 text: newtext,  
 type: "warning",  
 showCancelButton: true,  
 confirmButtonColor: "#DD6B55",  
 confirmButtonText: "Yes, delete it!", 
 closeOnConfirm: false }, function(){
window.location.href = theurl; 
});
   
    });
   

   //<!-- end DElete confirmation pop-up -->   

//<!-- check box delete all functionality --> 

 var IDs = [];
$('body').on('click','.checkdata',function(){
$('#deleteallhidden').val('0');
$('.checkAll').prop('checked',false);

 var emailvalue=$(this).val();

  if($(this).is(":checked")){
           if(emailvalue!=''){
           IDs.push($(this).val());}
      }
      else{
           if(emailvalue!=''){
           index = IDs.indexOf($(this).val());
           IDs.splice(index, 1);}
       }

if($('.checkdata:checked').length > 0){
$(".del_event").css('display','block');


}
else{
$(".del_event").css('display','none');

}
});

$('body').on('click','.submitallids',function(){
var alllids=IDs;
$('#deleteallids').val(alllids);
});


    

 $("body").on('click','.submitallids',function(e){
   

e.preventDefault();
  
   var thetext=$(this).attr('data-text');
 
   var newtext="";
   if(typeof thetext  === "undefined"){
      newtext = "You will not be able to recover this record";
   }
   else{
        newtext=thetext;
   }
swal({  
 title: "Are you sure?",  
 text: newtext,  
 type: "warning",  
 showCancelButton: true,  
 confirmButtonColor: "#DD6B55",  
 confirmButtonText: "Yes, delete it!", 
 closeOnConfirm: false }, function(){
   $('form').submit();
});
   
    });
   
//<!-- end check box delete all functionality --> 

//<!-- select store in add product page --> 
  
var theselected=$(".selstore option:selected").text();
 $('.storediv').children().children().children('span').html(theselected);
 
//<!-- end select store in add product page --> 


 
//<!-- delete multiple images add product page --> 
 
 
      $("body").on('click','.delmultimages',function(e) {
   var id=$(this).attr('data-image');
var baseurl=$('#thebaseurl').val();
e.preventDefault();
swal({  
 title: "Are you sure?",  
 text: "Do you really want to delete this image",  
 type: "warning",  
 showCancelButton: true,  
 confirmButtonColor: "#DD6B55",  
 confirmButtonText: "Yes, delete it!", 
 closeOnConfirm: false }, function(){
$.ajax({
          url: baseurl+"admin/products/deleteimages",
          type: "POST",
          data:{imgid:id},
          success: function(data)
        {$('#delmul'+id).remove();
         $('.delmultimages'+id).remove();
          $('.cancel').click();
        },
        error: function() {}           
     });

});
   
    });
   
//<!-- end delete multiple images add product page --> 


 
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;
            $(placeToInsertImagePreview).html('');
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery2');
    });




    // Single images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    $(placeToInsertImagePreview).html($($.parseHTML('<img>')).attr('src', event.target.result))
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#img-prev').on('change', function() {
        imagesPreview(this, 'div.imageprview');
    });
   $('#img-prev1').on('change', function() {
        imagesPreview(this, 'div.imageprview1');
    });
   $('#img-prev2').on('change', function() {
        imagesPreview(this, 'div.imageprview2');
    });


 //<!-- store category add product  -->
	
   
    $('#categ').change(function(){
   var id=$(this).val();
    $('#storeid').val(id);
    });
   


    
    $('body').on('change','#prodselopt',function(){
    var id=$(this).val();
    var baseurl=$('#thebaseurl').val();
     $.ajax({
          url: baseurl+"admin/products/getcategories",
          type: "POST",
          data:{id:id},
          success: function(data)
        { $('.ajaxcateg').html(data);
         $("#multi").val(["Jim", "Lucy"]).select2();
        },
        error: function() {}           
     });
     });
    
  
 //<!-- end store category add product  -->

 //<!-- category multi select edit product  -->

var pageid=$('#editpage').val();

if(pageid === undefined ){}
else{

    var str=$('#hiddencats').val();
    var res = str.split(",");
    $("#multi").val(res).select2();

}
   
 //<!-- end category multi select edit product  -->


 //<!--add brand on add product  -->

  $(".addbrandForm").on('submit',(function(e) {
    e.preventDefault();
    var baseurl=$('#thebaseurl').val();
    $.ajax({
          url: baseurl+"admin/brands/ajaxadd",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      success: function(data)
        {var newdata=data.split("sepratortext");
        $('.newbrandadddiv').html(newdata[0]);
        $('.newstoreadddiv').html(newdata[1]);
        $('.close').click();
        },
        error: function() 
        {
        }           
     });
  }));

 //<!--end add brand on add product  -->

 //<!--add store on add product  -->


  $(".addstoreForm").on('submit',(function(e) {
    e.preventDefault();
    var baseurl=$('#thebaseurl').val();
    $.ajax({
          url: baseurl+"admin/stores/ajaxadd",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      success: function(data)
        {var newdata=data.split("sepratortext");
        $('.newbrandadddiv').html(newdata[0]);
        $('.newstoreadddiv').html(newdata[1]);
        $('.close').click();
        },
        error: function() 
        {
        }           
     });
  }));
    //<!--end add store on add product  -->

 //<!--slider on view product  -->
 

            $(window).ready(function(){
                $('.flexslider').flexslider({
                    controlNav: "thumbnails",
                    start: function(slider){
                        $('body').removeClass('loading');
                    }
                });
            });
       
	    
 //<!--end slider on view product  -->
		

    $('[data-tooltip="true"]').tooltip(); 





 //<!--forget password -->

       $('#forget_submit').click(function(e){
e.preventDefault();
var useremail=$('#forgetemail').val().trim();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/;

if(!(regex.test(useremail)))
{$('#err-forgot').text("The email field must contain a vaild email.");
return false;
}

$('#err-forgot').text("");
var baseurl=$('#thebaseurl').val();

    $.ajax({
               url:baseurl+'admin/login/forgot',
               type:'POST',
                data:{email:useremail},
               success:function(data){
                   if(data=='not match'){$('#err-forgot').text("The email is not registered with us.");}
                   else{
                   $('#success_message').text("Please check your mail to reset password.");
                   $("#success_message").delay(4000).fadeOut();
                   }
               },
                error(error){console.log(error);}
           }); 
       });
   
    

 //<!--end forget password -->

 //<!--color picker -->
        var i=1;
        $('.add-color').click(function(){
          $('.color-field').append('<div class="form-group col-sm-6 no-padding addons removediv'+i+'"><div class="input-group colorpicker-element"><input type="color" class="form-control" name="color[]"><div class="deletecolor"><a href="javascript:void(0)" attrid="'+i+'" class="btn btn-white btn-xs btn-mini bold fs-14 delete-field">x</a></div></div></div>');  
         //$('.my-colorpicker2').colorpicker();
            i=i+1;
        });
        
        $('body').on('click','.delete-field',function(){
        
          var id =  $(this).attr('attrid');
        $('.removediv'+id).remove();
        });
      
 //<!--end color picker -->

$('#forgot-pass').click(function(){
$('#forgetemail').val('');
$('#err-forgot').text('');
$('#success_message').text('');
});
