$(document).ready(function(){
     
    $('body').on('click', '.updatestatus', function() {
        
        var id=$(this).attr('theid');
        var status=$(this).attr('thestatus');
        var URL = $('#baseUrl').val();
    
$.ajax({
        type: "POST",
        url: URL+'admin/register/updatestatus',
        data:{id:id,status:status},
       success: function(data){
          // alert(data);
           	var class_label='';
           	var sts='';
         
           if(data==0)
		{	 class_label = 'danger';
			sts='Deactive';}
			 if(data == 1)
			{ class_label = 'success';
			sts='Active';}
	   $(".statuschange"+id).html('<span class="label label-'+class_label+'">'+sts+'</span>');
	   $('#stats'+id).attr('thestatus',data);
     
},
failure: function(){}
    });
});
 // Brands active Deactive
 $('body').on('click', '.updatestatusbrand', function() {
        
        var id=$(this).attr('theid');
        var status=$(this).attr('thestatus');
        var URL = $('#baseUrl').val();
    
$.ajax({
        type: "POST",
        url: URL+'admin/brands/updatestatus',
        data:{id:id,status:status},
       success: function(data){
          // alert(data);
           	var class_label='';
           	var sts='';
         
           if(data==0)
		{	 class_label = 'danger';
			sts='Deactive';}
			 if(data == 1)
			{ class_label = 'success';
			sts='Active';}
	   $(".statuschangebrand"+id).html('<span class="label label-'+class_label+'">'+sts+'</span>');
	   $('#statsbrand'+id).attr('thestatus',data);
     
},
failure: function(){}
    });
});

 // Store active Deactive
$('body').on('click', '.updatestatusstore', function() {
        
        var id=$(this).attr('theid');
        var status=$(this).attr('thestatus');
        var URL = $('#baseUrl').val();
    
$.ajax({
        type: "POST",
        url: URL+'admin/stores/updatestatus',
        data:{id:id,status:status},
       success: function(data){
          // alert(data);
           	var class_label='';
           	var sts='';
         
           if(data==0)
		{	 class_label = 'danger';
			sts='Deactive';}
			 if(data == 1)
			{ class_label = 'success';
			sts='Active';}
	   $(".statuschangestore"+id).html('<span class="label label-'+class_label+'">'+sts+'</span>');
	   $('#statsstore'+id).attr('thestatus',data);
     
},
failure: function(){}
    });
});

// Products active Deactive
$('body').on('click', '.updatestatusproduct', function() {
        
        var id=$(this).attr('theid');
        var status=$(this).attr('thestatus');
        var URL = $('#baseUrl').val();
    
$.ajax({
        type: "POST",
        url: URL+'admin/products/updatestatus',
        data:{id:id,status:status},
       success: function(data){
          // alert(data);
           	var class_label='';
           	var sts='';
         
           if(data==0)
		{	 class_label = 'danger';
			sts='Deactive';}
			 if(data == 1)
			{ class_label = 'success';
			sts='Active';}
	   $(".statuschangeproduct"+id).html('<span class="label label-'+class_label+'">'+sts+'</span>');
	   $('#statsproduct'+id).attr('thestatus',data);
     
},
failure: function(){}
    });
});

// Offer active Deactive
$('body').on('click', '.updatestatusoffer', function() {
        
        var id=$(this).attr('theid');
        var status=$(this).attr('thestatus');
        var URL = $('#baseUrl').val();
    
$.ajax({
        type: "POST",
        url: URL+'admin/offers/updatestatus',
        data:{id:id,status:status},
       success: function(data){
          // alert(data);
           	var class_label='';
           	var sts='';
         
           if(data==0)
		{	 class_label = 'danger';
			sts='Deactive';}
			 if(data == 1)
			{ class_label = 'success';
			sts='Active';}
	   $(".statuschangeoffer"+id).html('<span class="label label-'+class_label+'">'+sts+'</span>');
	   $('#statsoffer'+id).attr('thestatus',data);
     
},
failure: function(){}
    });
});

// Categories active Deactive
$('body').on('click', '.updatestatuscat', function() {
        
        var id=$(this).attr('theid');
        var status=$(this).attr('thestatus');
        var URL = $('#baseUrl').val();
    
$.ajax({
        type: "POST",
        url: URL+'admin/stores/updatestatuscat',
        data:{id:id,status:status},
       success: function(data){
          // alert(data);
           	var class_label='';
           	var sts='';
         
           if(data==0)
		{	 class_label = 'danger';
			sts='Deactive';}
			 if(data == 1)
			{ class_label = 'success';
			sts='Active';}
	   $(".statuschangecat"+id).html('<span class="label label-'+class_label+'">'+sts+'</span>');
	   $('#statcat'+id).attr('thestatus',data);
     
},
failure: function(){}
    });
});

// user active Deactive
$('body').on('click', '.updatestatususer', function() {
        
        var id=$(this).attr('theid');
        var status=$(this).attr('thestatus');
        var URL = $('#baseUrl').val();
    
$.ajax({
        type: "POST",
        url: URL+'admin/users/updatestatususer',
        data:{id:id,status:status},
       success: function(data){
          // alert(data);
           	var class_label='';
           	var sts='';
         
           if(data==0)
		{	 class_label = 'danger';
			sts='Deactive';}
			 if(data == 1)
			{ class_label = 'success';
			sts='Active';}
	   $(".statuschangeuser"+id).html('<span class="label label-'+class_label+'">'+sts+'</span>');
	   $('#statuser'+id).attr('thestatus',data);
     
},
failure: function(){}
    });
});
    
});