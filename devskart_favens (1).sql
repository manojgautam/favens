-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2017 at 11:18 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devskart_favens`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(40) CHARACTER SET utf8 NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 NOT NULL,
  `image` varchar(100) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for admin and 0 for superadmin',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `image`, `status`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '26dc318942685872cf79c5eb96c9bb13', 'RTXkq628JHOViNKhzogC351spbYG9ZrPydMAeanU0DWctLEjmv.jpg', 1, 1, '2017-06-28 13:30:49', '2017-06-29 18:20:25'),
(4, 'prashant', 'prashant.techindustan@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', 1, 1, '2017-06-07 02:26:19', '2017-07-03 10:10:05'),
(6, 'Favens', 'favens@gmail.com', 'b8013862067dd746ee54d5ce4d7c18b7', 'Mg9i7KNdmeonfCxuBJbRUtZakLhW2vG0FrlP5p31.jpg', 1, 1, '2017-06-07 17:49:18', '0000-00-00 00:00:00'),
(13, 'favens', 'admin.favens@gmail.com', '95eb700d46278845481bab92fabc3af8', '', 1, 0, '2017-06-14 05:29:49', '2017-06-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `b_id` int(11) NOT NULL,
  `b_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `b_logo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `b_details` text CHARACTER SET utf8 NOT NULL,
  `b_p_count` smallint(5) UNSIGNED NOT NULL,
  `b_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `b_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `b_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archived 0 for not archive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`b_id`, `b_name`, `b_logo`, `b_details`, `b_p_count`, `b_status`, `b_added_by`, `b_archive`, `created_at`, `updated_at`) VALUES
(7, 'Nike', 'xH5Jh6pNGYQiBgDt9f7yOLzuZraU1XemW8qnFM0j.jpg', 'Nike, originally known as Blue Ribbon Sports (BRS), was founded by University of Oregon track athlete Phil Knight and his coach Bill Bowerman in January 1964. The company initially operated as a distributor for Japanese shoe maker Onitsuka Tiger (now ASICS), making most sales at track meets out of Knight\'s automobile.', 1, 1, 'Admin', 0, '2017-06-15 20:53:06', '0000-00-00 00:00:00'),
(8, 'Adidas', '1COU0uWgZKEsF6VbL4vkDAMie2wQtXlzThGSxYNp.jpg', 'Adidas AG is a German multinational corporation, headquartered in Herzogenaurach, Germany, that designs and manufactures shoes, clothing and accessories. It is the largest sportswear manufacturer in Europe, and the second largest in the world', 0, 1, 'Admin', 0, '2017-06-15 20:53:52', '0000-00-00 00:00:00'),
(9, 'Woodland', 'swKyOMZtpJ20QC6dv9eTxuiIDnYmcafbgXLA8G7B.jpg', 'Woodland\'s parent company, Aero Group, has been a well known name in the outdoor shoe industry since the early 50s. Founded in Quebec, Canada, it entered the Indian market in 1992. Before that, Aero Group was majorly exporting its leather shoes to Russia. ', 1, 1, 'Admin', 0, '2017-06-15 20:54:58', '0000-00-00 00:00:00'),
(10, 'Puma', 'dxhVc4DGzSgmHRjvXULCf7BiAPtYe9TEKkubF3pQ.jpg', 'PUMA SE, branded as PUMA, is a German multinational company that designs and manufactures athletic and casual footwear, apparel and accessories, headquartered in Herzogenaurach, Germany. The company was founded in 1948 by Rudolf Dassler', 1, 1, 'Admin', 0, '2017-06-15 20:55:25', '0000-00-00 00:00:00'),
(11, 'Tommy Hilfiger', 'obWRvFlD61IcsPjzXN8ryL7pGtZqwu5ng0kHefUChx2MiSmQAV.jpg', 'Chain featuring the designer\'s classic sportswear & accessories. Some focus on apparel for kids.', 2, 1, 'Admin', 0, '2017-06-15 20:55:57', '2017-06-29 17:30:36'),
(28, 'Jack & Jone', 'LgX3zYqsNQOj978lfERwVMBCx0tUcPvWAoDreTk4.jpg', ' It is the largest sportswear manufacturer in Europe, and the second largest in the world', 3, 1, 'Admin', 0, '2017-06-27 08:32:22', '2017-06-28 18:33:49');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `c_id` int(11) NOT NULL,
  `c_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `c_store` tinyint(1) NOT NULL COMMENT '0-default category',
  `c_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive	',
  `c_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`c_id`, `c_name`, `c_store`, `c_status`, `c_added_by`, `created_at`) VALUES
(18, 'shirt', 8, 1, 'Admin', '2017-06-21 01:51:07'),
(26, 'tees', 9, 1, 'Admin', '2017-06-21 18:44:17'),
(15, 'qwerty', 7, 1, 'Admin', '2017-06-21 01:50:22'),
(16, 'tees', 7, 1, 'Admin', '2017-06-21 01:50:32'),
(27, 'jeans', 10, 1, 'Admin', '2017-06-21 18:44:39'),
(28, 'Shoes', 0, 1, 'Admin', '2017-06-22 19:59:45'),
(29, 'zoomato', 6, 1, 'Admin', '2017-06-22 21:05:07'),
(34, 'moon', 8, 1, 'Admin', '2017-06-22 21:17:29'),
(35, 'Tshirts', 0, 1, 'Admin', '2017-06-22 22:03:01');

-- --------------------------------------------------------

--
-- Table structure for table `logos`
--

CREATE TABLE `logos` (
  `l_id` int(11) NOT NULL,
  `l_name` varchar(50) NOT NULL,
  `l_logo` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logos`
--

INSERT INTO `logos` (`l_id`, `l_name`, `l_logo`, `created_at`) VALUES
(15, 'Lotto', 'rkOcK6SD0J4twH93Q58VGUmWNynoExlqpFhBsYid.jpg', '2017-06-28 11:14:47'),
(16, 'Reebok', 'FR8nw7S3BCHYQtEvaPZJuyd1xem45GLrUpIhjVWX.jpg', '2017-06-28 11:15:05'),
(17, 'Puma', 'i0UfoZtHnG35B1bKPeRYdEcJuXDmzsyg4kNjVv2T.jpg', '2017-06-28 11:15:21'),
(18, 'Adidas', 'IM5zGTxlOrXcUK4hZ9jpmaiYNFw1DB7JH3SAgfb2.jpg', '2017-06-28 11:15:34'),
(19, 'Casio', 'KyEjbZ6S0cvm21DIWtFJwqzhCxRgalN7en5GdoiH.jpg', '2017-06-28 11:15:45'),
(20, 'Fila', 'm8PjGnhHFLUzsglioybavpBuqM76tWdQ9SAeOCIc.jpg', '2017-06-28 11:15:59'),
(21, 'gshock', 'MLGQs1pzOk42qVPiofeaUXctZjJIY5wbhTNR8uyx.jpg', '2017-06-28 11:16:18'),
(22, 'Lacoste', 'YTzN4PW8EqO7kabJC6ltXRUpVfI2sQnoeujgDGmv.jpg', '2017-06-28 11:16:35'),
(23, 'Nike', 'PUCel3uvsK21OTDn5N0MpAyJfqVRmBQ9xihgb7X4.jpg', '2017-06-28 11:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `n_id` int(11) NOT NULL,
  `n_type` tinyint(1) NOT NULL COMMENT '(1 for product,2 for brand, 3 for store)',
  `n_name` varchar(100) NOT NULL,
  `n_seen` tinyint(1) NOT NULL COMMENT '1 for read 0 for unread',
  `n_added_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `n_id`, `n_type`, `n_name`, `n_seen`, `n_added_by`, `created_at`) VALUES
(1, 39, 1, 'emo', 0, 'Admin', '2017-06-27 02:33:45'),
(2, 40, 1, 'Jacket Black Tight Fit ', 0, 'Admin', '2017-06-27 03:07:25'),
(3, 28, 2, 'Jack&Jone', 0, 'Admin', '2017-06-27 14:02:22'),
(4, 13, 3, 'American store', 0, 'Admin', '2017-06-27 14:14:32'),
(5, 29, 2, 'vector', 0, 'Admin', '2017-06-27 15:38:14'),
(6, 41, 1, 'assfd', 0, 'Admin', '2017-06-28 19:09:32'),
(7, 42, 1, 'test', 0, 'Admin', '2017-06-28 19:11:44'),
(8, 31, 2, 'mmm', 1, 'Admin', '2017-06-29 17:18:53'),
(9, 43, 1, 'abc', 1, 'Admin', '2017-06-29 18:31:54'),
(10, 44, 1, 'Tommy', 1, 'Admin', '2017-06-29 19:19:27'),
(11, 14, 3, 'my1', 1, 'parshant', '2017-06-30 12:09:17'),
(12, 15, 3, 'my2', 1, 'parshant', '2017-06-30 12:09:37'),
(13, 16, 3, 'sdsd', 1, 'parshant', '2017-06-30 12:09:48'),
(14, 17, 3, 'gfh', 1, 'parshant', '2017-06-30 12:10:05'),
(15, 18, 3, 'tyuytu', 1, 'parshant', '2017-06-30 12:10:20'),
(16, 19, 3, 'hj', 1, 'parshant', '2017-06-30 12:10:28'),
(17, 20, 3, 'gfgfr', 1, 'parshant', '2017-06-30 12:16:29'),
(18, 21, 3, 'sdfde', 1, 'parshant', '2017-06-30 12:16:50'),
(19, 22, 3, 'df', 1, 'parshant', '2017-06-30 12:17:06'),
(20, 23, 3, 'ghj', 1, 'parshant', '2017-06-30 12:29:09'),
(21, 24, 3, 'gthygfh', 1, 'parshant', '2017-06-30 12:29:19'),
(22, 25, 3, 'gfhgf', 1, 'parshant', '2017-06-30 12:29:32'),
(23, 26, 3, 'ghjghj', 1, 'parshant', '2017-06-30 12:29:50'),
(24, 27, 3, 'new1', 1, 'parshant', '2017-06-30 13:18:44'),
(25, 28, 3, 'new2', 1, 'parshant', '2017-06-30 13:19:02'),
(26, 45, 1, 'asd', 1, 'Admin', '2017-06-30 15:22:09'),
(27, 32, 2, 'fh', 1, 'parshant', '2017-06-30 16:09:48'),
(28, 33, 2, 'fdg', 1, 'parshant', '2017-06-30 16:09:59'),
(29, 34, 2, 'new', 1, 'parshant', '2017-06-30 16:19:40'),
(30, 35, 2, 'new2', 1, 'parshant', '2017-06-30 16:19:57'),
(31, 36, 2, 'err', 1, 'parshant', '2017-06-30 19:24:33'),
(32, 37, 2, 'xf', 1, 'parshant', '2017-06-30 19:24:53'),
(33, 38, 2, 'df', 1, 'parshant', '2017-06-30 19:25:10'),
(34, 39, 2, 'dfdsf', 1, 'parshant', '2017-06-30 19:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `o_id` int(11) NOT NULL,
  `o_title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `o_detail` text CHARACTER SET utf8 NOT NULL,
  `o_date` date NOT NULL,
  `o_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `o_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `o_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archive 0 for no archive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`o_id`, `o_title`, `o_detail`, `o_date`, `o_status`, `o_added_by`, `o_archive`, `created_at`, `updated_at`) VALUES
(14, 'Testing', 'lorem ipsum dolor sit', '2017-06-28', 1, 'Admin', 0, '2017-06-28 13:13:08', '2017-06-28 18:43:08'),
(12, 'Upto 60% off', 'Denims To love!', '2017-06-30', 1, 'Admin', 0, '2017-06-29 06:08:14', '0000-00-00 00:00:00'),
(13, '75% off', 'Only for Sports Shoe ', '2017-06-30', 1, 'Admin', 0, '2017-06-29 06:08:10', '0000-00-00 00:00:00'),
(11, '30% to 50% off', 'It\'s Now or Never', '2017-06-30', 1, 'Admin', 0, '2017-06-29 06:08:06', '0000-00-00 00:00:00'),
(10, 'Minimum 40% off', 'On season Latest Casual Style', '2017-06-29', 1, 'Admin', 0, '2017-06-29 06:09:57', '2017-06-29 11:39:57'),
(16, 'testing', 'lorem ipsum dolor sit', '2017-06-23', 1, 'Admin', 1, '2017-06-21 14:11:50', '0000-00-00 00:00:00'),
(17, 'dummy', 'lorem ipsum dolor sit', '2017-06-29', 1, 'Admin', 1, '2017-06-29 06:09:45', '2017-06-29 11:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(11) NOT NULL,
  `p_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `p_original_price` varchar(15) CHARACTER SET utf8 NOT NULL,
  `p_new_price` varchar(15) CHARACTER SET utf8 NOT NULL,
  `p_detail` text CHARACTER SET utf8 NOT NULL,
  `p_image` varchar(255) NOT NULL DEFAULT 'product_default.png',
  `p_brand` int(11) NOT NULL,
  `p_store` int(11) NOT NULL,
  `p_size` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_color` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `p_added_by` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archive 0 for no archive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_name`, `p_original_price`, `p_new_price`, `p_detail`, `p_image`, `p_brand`, `p_store`, `p_size`, `p_color`, `p_status`, `p_added_by`, `p_archive`, `created_at`, `updated_at`) VALUES
(32, 'Tommy Hilfiger Men Blue Slim Fit Mid-Rise Clean Look Jeans', '9999', '7777', 'Blue medium wash 5-pocket mid-rise jeans, clean look with light fade, has a button and zip closure, waistband with belt loops', 'Stg1RbOrw8BEC7ovcTFAVlhW4DPLKm.jpg', 11, 8, 'S,M,L,XL', 'Black|Blue', 1, 'Admin', 0, '2017-06-28 13:01:10', '2017-06-29 19:26:51'),
(30, 'Tommy Hilfiger Navy & Red Laptop Backpack', '1999', '1199', 'Navy blue, red and white laptop backpack One haul loop, has two padded and adjustable shoulder straps Two main zip compartments, one has a padded laptop sleeve secured with a tab and a Velcro closure A mesh stash pocket on ei', 'gfuJObjo2lLc1xkMr3V76GdqUZpKXW.jpg', 11, 10, 'M', 'Black|Blue|White', 1, 'Admin', 0, '2017-06-28 11:19:16', '2017-06-30 11:16:13'),
(33, 'Nike Shoe', '10,190', '8000', 'Designed to Move in Any Direction.\r\nA Revolution in Motion.', '4P0ZCon6mVrluLkfOth8NH1MpaeRzA.jpg', 7, 10, 'M,L,XL', 'Black|White', 1, 'Admin', 1, '2017-06-15 21:26:22', '2017-06-30 11:16:46'),
(34, 'PUMA Black Tight Fit Tights', '1499', '974', 'Black mid-rise three-fourth tights, has an elasticated waistband', 'tFyW6hiSHcAakCvgLXpMO591Ix2U0D.jpg', 10, 9, 'M,L', 'Black|Blue', 1, 'Admin', 0, '2017-06-28 13:16:19', '2017-06-30 11:17:47'),
(40, 'Jacket Black Tight Fit ', '6000', '5000', 'Blue medium wash 5-pocket mid-rise jeans, clean look with light fade, has a button and zip closure, waistband with belt loops', 'c29NTmOYkAesZtdL06QSfigHhrMuxw.jpg', 9, 6, 'XL,XXL', 'Black|Blue|White', 1, 'Admin', 0, '2017-06-28 13:16:40', '2017-06-30 11:15:22'),
(45, 'asd', '1234', '12', 'fgjhghj', 'HYTVDIyZsjMg4xkhSFquiWOQpU7R3l.jpg', 28, 10, 'S,M', '#4d1313|rgba(222,35,35,0.74)|rgba(41,31,105,0.29)', 1, 'Admin', 0, '2017-06-30 09:52:09', '2017-06-30 17:34:59'),
(43, 'abc', '12345', '123', 'fdgdfg', 'Sxuy5mROYBcPAdiUM3fQ7krb8gzoTn.jpg', 28, 9, 'S,M,L', 'Black|Blue', 1, 'Admin', 1, '2017-06-29 13:01:54', '2017-06-30 11:14:47'),
(44, 'Tommy', '600', '500', 'best product', 'zRJdunkoqCbSm6281XsD04w5WNM3jc.jpg', 28, 10, 'M,L', 'Black|Blue', 1, 'Admin', 0, '2017-06-29 13:49:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `image_id` int(11) NOT NULL,
  `p_image_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `p_id` int(11) NOT NULL,
  `p_image_type` tinyint(1) NOT NULL COMMENT '1 for Primary and 2 for others',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `p_image_name`, `p_id`, `p_image_type`, `created_at`, `updated_at`) VALUES
(140, 'HigoyPRd0GtNXhBqbpY5fzEV1TnmSK.jpg', 33, 2, '2017-06-30 05:46:46', '0000-00-00 00:00:00'),
(139, 'oOfVMl5gyCKFsHqGEm6Sk7jvpzQeUh.jpg', 33, 2, '2017-06-30 05:46:46', '0000-00-00 00:00:00'),
(138, 'ZuzpVcd8JUql7GeFfxEoBnCXY9hT0i.jpg', 30, 2, '2017-06-30 05:46:13', '0000-00-00 00:00:00'),
(137, 'skKENvanUTLCDXiJu5lAMoFeGBYfhO.jpg', 30, 2, '2017-06-30 05:46:13', '0000-00-00 00:00:00'),
(132, 'zML0rg5P9HyD6nVvWjx4kqaU2NFme7.jpg', 32, 2, '2017-06-29 13:56:51', '0000-00-00 00:00:00'),
(131, 'e2KRBsdYWZzuiF1DPvTk3EM69OrgN8.jpg', 32, 2, '2017-06-29 13:56:51', '0000-00-00 00:00:00'),
(130, 'O6XW9cgHfSAei5wzFuIlGvnjTMQRxs.jpg', 44, 2, '2017-06-29 13:49:27', '0000-00-00 00:00:00'),
(129, 'aJb1u5rgQv2TOjm0zFkxdpKRctwlos.jpg', 44, 2, '2017-06-29 13:49:27', '0000-00-00 00:00:00'),
(143, '8nQ3JAPwdhjROBDE0aioHLImMGcszV.jpg', 45, 2, '2017-06-30 09:52:09', '0000-00-00 00:00:00'),
(141, 'k1MRgsmXiO3EjTS5ZouYWedz2rxnHL.jpg', 34, 2, '2017-06-30 05:47:47', '0000-00-00 00:00:00'),
(135, 'bYTf53P7dk4umtoBhSWD1Q2FlnrREA.jpg', 40, 2, '2017-06-30 05:37:09', '0000-00-00 00:00:00'),
(142, 'gvFtcNbhIHTRpL9suy5e6D0UAlid1n.jpg', 34, 2, '2017-06-30 05:47:47', '0000-00-00 00:00:00'),
(136, 'JiTt4EFXLKZ2P8qbHQAGwyWS1uVod0.jpg', 40, 2, '2017-06-30 05:37:09', '0000-00-00 00:00:00'),
(123, 'tVEhRFTYD4gS6BIkcA2vwufjmMsy97.jpg', 43, 2, '2017-06-29 13:01:54', '0000-00-00 00:00:00'),
(124, 'xaUAw96DfOBlPmc0uNt5vdWRCipj2F.jpg', 43, 2, '2017-06-29 13:01:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `p_s_categories`
--

CREATE TABLE `p_s_categories` (
  `p_s_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_s_categories`
--

INSERT INTO `p_s_categories` (`p_s_id`, `p_id`, `s_id`, `cat_id`, `created_at`, `updated_at`) VALUES
(45, 36, 7, 16, '2017-06-28 05:20:26', '0000-00-00 00:00:00'),
(44, 36, 7, 15, '2017-06-28 05:20:26', '0000-00-00 00:00:00'),
(113, 30, 10, 27, '2017-06-30 05:46:13', '0000-00-00 00:00:00'),
(39, 39, 6, 28, '2017-06-27 07:33:45', '0000-00-00 00:00:00'),
(95, 32, 8, 18, '2017-06-29 13:56:51', '0000-00-00 00:00:00'),
(116, 33, 10, 35, '2017-06-30 05:46:46', '0000-00-00 00:00:00'),
(32, 37, 8, 28, '2017-06-23 08:31:53', '0000-00-00 00:00:00'),
(115, 33, 10, 28, '2017-06-30 05:46:46', '0000-00-00 00:00:00'),
(114, 33, 10, 27, '2017-06-30 05:46:46', '0000-00-00 00:00:00'),
(49, 38, 8, 34, '2017-06-28 06:30:58', '0000-00-00 00:00:00'),
(48, 38, 8, 28, '2017-06-28 06:30:58', '0000-00-00 00:00:00'),
(47, 38, 8, 18, '2017-06-28 06:30:58', '0000-00-00 00:00:00'),
(40, 39, 6, 29, '2017-06-27 07:33:45', '0000-00-00 00:00:00'),
(41, 39, 6, 35, '2017-06-27 07:33:45', '0000-00-00 00:00:00'),
(112, 40, 6, 35, '2017-06-30 05:45:22', '0000-00-00 00:00:00'),
(117, 34, 9, 0, '2017-06-30 05:47:47', '0000-00-00 00:00:00'),
(46, 36, 7, 35, '2017-06-28 05:20:26', '0000-00-00 00:00:00'),
(50, 38, 8, 35, '2017-06-28 06:30:58', '0000-00-00 00:00:00'),
(111, 40, 6, 29, '2017-06-30 05:45:22', '0000-00-00 00:00:00'),
(79, 41, 10, 35, '2017-06-28 13:39:32', '0000-00-00 00:00:00'),
(80, 42, 7, 28, '2017-06-28 13:41:44', '0000-00-00 00:00:00'),
(81, 42, 7, 35, '2017-06-28 13:41:44', '0000-00-00 00:00:00'),
(106, 43, 9, 28, '2017-06-30 05:44:47', '0000-00-00 00:00:00'),
(94, 44, 10, 35, '2017-06-29 13:49:27', '0000-00-00 00:00:00'),
(110, 40, 6, 28, '2017-06-30 05:45:22', '0000-00-00 00:00:00'),
(129, 45, 10, 35, '2017-06-30 12:04:59', '0000-00-00 00:00:00'),
(128, 45, 10, 28, '2017-06-30 12:04:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `s_id` int(11) NOT NULL,
  `s_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `s_location` varchar(100) CHARACTER SET utf8 NOT NULL,
  `s_image` varchar(100) CHARACTER SET utf8 NOT NULL,
  `s_p_count` int(11) NOT NULL,
  `s_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `s_added_by` varchar(50) CHARACTER SET utf8 NOT NULL,
  `s_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 for archived 0 for no archivee',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`s_id`, `s_name`, `s_location`, `s_image`, `s_p_count`, `s_status`, `s_added_by`, `s_archive`, `created_at`, `updated_at`) VALUES
(7, 'King of Prussia Mall', '160 N Gulph Rd, King of Prussia, PA 19406, USA', 'PHDTL6aS1mEfsNJCX3FqnWUjI4wpQO2xgVBetRGh.jpg', 0, 1, 'Admin', 0, '2017-06-15 20:31:14', '0000-00-00 00:00:00'),
(6, ' Mall of America', '60 E Broadway, Bloomington, MN 55425, USA', 'BuVka46y9qSY7Jg3jIR1UZxez8oAm5ELXnQ0swHW.jpg', 1, 1, 'Admin', 0, '2017-06-15 20:30:30', '0000-00-00 00:00:00'),
(8, 'Sawgrass Mills', '12801 W Sunrise Blvd, Sunrise, FL 33323, USA', 'el7Mx0qNprHW8KobFzCVwDnjhI4c3fQ9LSgskXu1.jpg', 1, 1, 'Admin', 0, '2017-06-15 20:35:06', '0000-00-00 00:00:00'),
(9, 'The Shops at Columbus Circle', 'With quirky boutiques and fashionable neighborhoods abound, it takes a lot to stand out on New York’', 'Pi4fo3MXUHJzrhOtAGdy0jZeLbSIp5RNg6KYQD97.jpg', 2, 1, 'Admin', 0, '2017-06-15 20:37:36', '0000-00-00 00:00:00'),
(10, ' The Galleria', '5085 Westheimer  Rd, Houston, TX 7056, USA', 'xya2uSgOICNimHrG0KXQ3L1tYsUvochPkFnBR7Dz.jpg', 4, 1, 'Admin', 0, '2017-06-15 20:38:08', '2017-06-28 18:37:07'),
(28, 'new2', 'mohali', 'EDHvgGn6WotAd3p7JKBSChqraXOMFjNf2Q9T1PRi.jpg', 0, 1, 'parshant', 1, '2017-06-30 07:49:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `u_email` varchar(50) NOT NULL,
  `u_access_token` varchar(255) NOT NULL,
  `u_device_token` varchar(255) NOT NULL,
  `u_device_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for android and 2 for iOS',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 for active 0 for deactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `u_email`, `u_access_token`, `u_device_token`, `u_device_type`, `is_active`, `created_at`) VALUES
(23, 'sh@g.com', '181f05fcf49199408ed221e1040d2151b7de5a88724bc213cdd89365f1e6ed84', 'eyumheOPX6s:APA91bG-jhXVugawOeThZ3mZrHSJDZpAuvgMh0CqQm5j9o35YOmQgBmQce-6G4lUgvgirMaRRVs1OfwbE8nj78oVHka8Who3-tMqtvXKZdbe3sCg4sNDBF3TLD9c2g7NK_3b41hScUC7', 1, 1, '2017-06-29 12:19:24'),
(22, 'satish@techindustan.com', '0b4eb6e88ed178e813c9a59e9054c7cc7202440b0a5ef2d694574bf76187bcfa', 'eyumheOPX6s:APA91bG-jhXVugawOeThZ3mZrHSJDZpAuvgMh0CqQm5j9o35YOmQgBmQce-6G4lUgvgirMaRRVs1OfwbE8nj78oVHka8Who3-tMqtvXKZdbe3sCg4sNDBF3TLD9c2g7NK_3b41hScUC7', 1, 1, '2017-06-29 11:16:58'),
(24, 'shruti@gmail.com', '17f1cf1be433f61e8d661e50fe0a98a8687fae3a0d5d5eb222915fdc8f1a6902', 'dOsMWeGkqDM:APA91bEwZlVBrkwwIv9UetODoCY37uCJ9OrhYsr8zVl2DJkuGoHwJnnl-2DRN6gzezwiXBbc9zhDy7Gnyjp5LNzdPqlLN_0ht6ZYn1KoTSYCI6VgpAMnI3CkXL7C6A-YIO21rTpbpal7', 1, 1, '2017-06-29 12:23:00'),
(25, 'sh@gma.xom', '209272a6a2ddbb82c659acbed752d613bdc61a0c1a5a5932508dadacfcd22b7e', 'cY2mMgeIW2E:APA91bHLvz0KpHJklCN7tJXnziWlQ4QCeJGg53KH66bA7nn4RDyKMF4Kz1n_KkVMO0x_0xm3feLGcX7u2yrF2EAIgdrVu7hYMHgDZjDUC5_jWO-H8gDca-bVDg9y_4o6wy5dBds4TB-e', 1, 1, '2017-06-29 12:34:42'),
(26, 'shru@gmail.com', 'c27c7a3083f6ed393505a8cf5baf56de2a8ea6b2b34d2f97031600124ea7d0cc', 'fWwmNA1lulI:APA91bFGkvjAB24rIl5X3vx301gMGARjj6hdH401oqFhnyuTiBrJsU8LkX0VJ42ne7SJ3wA_qUD_k5oKxoooDyzqCr8BfUtbYXkRYvPBeuhgsC1LE25Ri9gt0jXOSgFnuYNg8n_topXP', 1, 1, '2017-06-29 12:40:51'),
(27, 'miniwe@gmail.com', '60c49f01b00eced505c357cb08c25b6ff7da99824a713333d69b8e4879d517f2', 'fWwmNA1lulI:APA91bFGkvjAB24rIl5X3vx301gMGARjj6hdH401oqFhnyuTiBrJsU8LkX0VJ42ne7SJ3wA_qUD_k5oKxoooDyzqCr8BfUtbYXkRYvPBeuhgsC1LE25Ri9gt0jXOSgFnuYNg8n_topXP', 1, 1, '2017-06-29 12:45:55'),
(28, 'sh@gmail.c', '971c9f6a354259c5b0f3c320318ab13667b8c63f11f3e6a78d04320b4aedbcdb', 'eqln3XHkDO8:APA91bGlLqHf7oLuI8wg191qgtyG06xTjDLx1TDMgDW8oyzwa8RNpMeIhhsASGe9Er-OgQsTLx3S4QFVo97CGa2VLrVuI_eOlW-7Q60E9Z29ODRTcgawevj5DytRg8S6hEtXbd7kdX7I', 1, 1, '2017-06-30 07:46:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `logos`
--
ALTER TABLE `logos`
  ADD PRIMARY KEY (`l_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`o_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `p_s_categories`
--
ALTER TABLE `p_s_categories`
  ADD PRIMARY KEY (`p_s_id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `logos`
--
ALTER TABLE `logos`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `p_s_categories`
--
ALTER TABLE `p_s_categories`
  MODIFY `p_s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
